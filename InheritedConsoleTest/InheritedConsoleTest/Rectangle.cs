﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //for drawing!
using System.Windows.Forms;

namespace InheritedConsoleTest
{
    internal class Rectangle : Square, IMathShape
    {

        protected double b;
        public double B { get { return this.b; } set { this.b = value; } }
        public Rectangle(double a, double b)
        {
            this.a = a;
            this.b = b;
        }
        protected Rectangle() { }

        //перегрузка ToString для класса (для форматирванного вывода данных класса вместо вывода типа по умолчанию)
        public override string ToString()
        {
            return string.Format("Прямоугольник со сторонами {0} и {1}", this.a, this.b);
        }

        public override double calcS()//internal почти равно public, но видно всем, кто из моего exe; public можно вызвать и из другого exe
        {
            return this.a * this.b;

        }

        public override double calcP()
        {
            return (a + b) * 2;
        }

        public void draw(Graphics graphics)
        {
            Pen myPen = Pens.Blue;
            graphics.DrawRectangle(myPen, 240, 10, (float)a, (float)b);
        }
    }

}
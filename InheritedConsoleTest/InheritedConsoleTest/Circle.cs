﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //for drawing!
using System.Windows.Forms;


namespace InheritedConsoleTest
{
    class Circle : MyPoint, IMathShape, IGraphicShape
    {
        double r;

        public double R
        {
            get { return this.r; }
            set { r = value; }
        }

        private Circle() { }
        public Circle (double x, double y, double r)
        {
            this.x = x;
            this.y = y;
            this.R = r;
        }

        //перегрузка ToString для класса (для форматирванного вывода данных класса вместо вывода типа по умолчанию)
        public override string ToString()
        {
            return string.Format("Окружность с центром [{0} , {1}] и радиусом {2}", this.x, this.y, this.r);
        }

        public virtual double calcS()
        {
            return Math.Pow(Math.PI * r, 2.0);
        }

        public virtual double calcP()
        {
            return 2 * Math.PI * r;
        }

        public double calcL()//не получается создать static метод, т.к. нельзя вызвать не статический calcP из статического метода
        {
          return calcP();
        }

        public void draw(Graphics graphics)
        {
            Pen myPen = Pens.Red;
            graphics.DrawEllipse(myPen, 10, 10, (float)r, (float)r);
        }
    }
}

﻿namespace InheritedConsoleTest
{
    public interface IMathShape //интерфейс всегда public
    {
       double calcS();
       double calcP();
    }
}
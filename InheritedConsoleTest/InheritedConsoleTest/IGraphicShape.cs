﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //for drawing!
using System.Windows.Forms;


namespace InheritedConsoleTest
{
    public interface IGraphicShape
    {
        void draw(Graphics graphics);
    }
}

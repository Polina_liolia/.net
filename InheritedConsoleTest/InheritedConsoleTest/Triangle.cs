﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //for drawing!
using System.Windows.Forms;


namespace InheritedConsoleTest
{
    class Triangle : Rectangle, IMathShape, IGraphicShape
    {
        double c;

        public double C
        {
            get { return this.c; }
            set { this.c = value; }
        }

        private Triangle() { }
        public Triangle (double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.C = c;
        }

        //перегрузка ToString для класса (для форматирванного вывода данных класса вместо вывода типа по умолчанию)
        public override string ToString()
        {
            return string.Format("Треугольник со сторонами {0}, {1} и {2}", this.a, this.b, this.c);
        }

        public override double calcP()
        {
            return (a + b + c);
        }

        public override double calcS()
        {
            double half_p = this.calcP()/2;
            return Math.Sqrt(half_p * (half_p - a) * (half_p - b) * (half_p - c));
        }
         public void draw(Graphics graphics)
        {
            throw new NotImplementedException();//не удобно рисовать треугольник без координат его вершин
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //for drawing!
using System.Windows.Forms;

namespace InheritedConsoleTest
{
    class MyPoint : IGraphicShape
    {
        protected double x;
        protected double y;
        public MyPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        //перегрузка ToString для класса (для форматирванного вывода данных класса вместо вывода типа по умолчанию)
        public override string ToString()
        {
            return string.Format("[{0} ; {1}]", this.x, this.y);
        }

        public void draw(Graphics graphics)
        {
            Pen myPen = Pens.Red;
            Brush myBrush = Brushes.BlueViolet;
            int x = 10;//x-координата верхнего левого угла относительно окна формы
            int y = 10;//у-координата верхнего левого угла относительно окна формы
            graphics.DrawLine(myPen, x, y, 1, 1);
        }

        protected MyPoint() { }//запрет вызова конструктора без параметров

        //свойство для доступа к полю х: //Edit - Refactor - Encapsulate field
        public double X
        {
            get { return this.x; }

            set { this.x = value; }
        }
        //свойство для доступа к полю y:
        public double Y
        {
            get { return this.y; }

            set { this.y = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing; //for drawing!
using System.Windows.Forms;


namespace InheritedConsoleTest
{
    public class Square : IMathShape, IGraphicShape
    {
        #region fields&props
        protected double a;
        public double A
        {
            get{return this.a;}
            set { this.a = value; }
        }

        #endregion

        #region Constructors
        protected Square() { }
        public Square(double a)
        {
            this.a = a;
        }

        #endregion

        //перегрузка ToString для класса (для форматирванного вывода данных класса вместо вывода типа по умолчанию)
        public override string ToString()
        {
            return string.Format("Квадрат со стороной {0}", this.a);
        }

        //чтоб извлечь интерфейс из данного класса: Edit - Refactor - Extract interface
        public virtual double calcS()//internal почти равно public, но видно всем, кто из моего exe; public можно вызвать и из другого exe
        {
            return this.a * this.a;

        }

        public virtual double calcP()
        {
            return a * 4;
        }

        public void draw(Graphics graphics)
        {
            Pen myPen = Pens.Green;
            graphics.DrawRectangle(myPen, 140, 10, (float)a, (float)a);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InheritedConsoleTest
{
    public partial class FormShapes : Form
    {
        public FormShapes()
        {
            InitializeComponent();
        }

        private void btn_circle_Click(object sender, EventArgs e)
        {
            Circle circle = new Circle(50, 50, Int32.Parse(txt_a1.Text));
            Graphics graphics = Graphics.FromHwnd(this.pbox_for_shapes.Handle);
            circle.draw(graphics);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Square square = new Square(Int32.Parse(txt_a1.Text));
            Graphics graphics = Graphics.FromHwnd(this.pbox_for_shapes.Handle);
            square.draw(graphics);
        }

        private void btn_rectangle_Click(object sender, EventArgs e)
        {
            Rectangle rect = new Rectangle(Int32.Parse(txt_a2.Text), Int32.Parse(txt_b2.Text));
            Graphics graphics = Graphics.FromHwnd(this.pbox_for_shapes.Handle);
            rect.draw(graphics);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//реализовать всю иарархию классов квадрат, прямоугольник, треугольник, окружность(calcP - будет реализована и вызвана внутри calcL)
namespace InheritedConsoleTest
{
    class Program
    {
        public static void Main(string[] args)
        {
            //запускаем форму:
            Application.EnableVisualStyles();
            Application.Run(new FormShapes()); 
            //консольные тесты:
            MyPoint p1 = new MyPoint(0.0, 0.0);
            MyPoint p2 = new MyPoint(1.0, 1.0);
            double d = MyLine.calcDistance(p1, p2);
            Console.WriteLine("Точка 1{0}", p1); //неявно вызван ToString (был переопределен для класса)
            //по умолчанию был такой вызов: this.GetType().ToString();
            Square sq1 = new Square(3);
            double squareArea = sq1.calcS();
            Console.WriteLine("Площадь квадрата со стороной {0} = {1}", sq1.A, squareArea);
            Rectangle rec1 = new Rectangle(2.0, 3.0);
            double recArea = rec1.calcS();
            Console.WriteLine("Площадь прямоугольника со сторонами {0} и {1} = {2}", rec1.A, rec1.B, recArea);
            Console.WriteLine("Через интерфейс:");
            PrintAreas(sq1);
            PrintP(sq1);
            PrintAreas(rec1);
            PrintP(rec1);
            Triangle tri1 = new Triangle(3.0, 4.0, 5.0);
            PrintAreas(tri1);
            PrintP(tri1);
            Circle circle1 = new Circle(2.0, 3.0, 4.0);
            PrintAreas(circle1);
            PrintP(circle1);
            double circleLength = circle1.calcL();
            Console.WriteLine("Длина окружности с центром [{0} , {1}] и радиусом {2} равна {3}", circle1.X, circle1.Y, circle1.R, circleLength);
            Console.WriteLine("Press any key...");
            Console.ReadKey();

        }
        public static void PrintAreas(IMathShape shape)
        {
            double areas = shape.calcS();
            Console.WriteLine("Площадь фигуры ({0}) = {1}", shape, areas);
        }

        public static void PrintP(IMathShape shape)
        {
            double p = shape.calcP();
            Console.WriteLine("Периметр фигуры ({0}) = {1}", shape, p);
        }
    }
}

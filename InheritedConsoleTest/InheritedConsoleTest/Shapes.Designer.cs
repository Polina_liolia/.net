﻿namespace InheritedConsoleTest
{
    partial class FormShapes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_a = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.pbox_for_shapes = new System.Windows.Forms.PictureBox();
            this.txt_a1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_b = new System.Windows.Forms.Label();
            this.btn_rectangle = new System.Windows.Forms.Button();
            this.btn_circle = new System.Windows.Forms.Button();
            this.txt_b2 = new System.Windows.Forms.TextBox();
            this.txt_a2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbox_for_shapes)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_a
            // 
            this.lbl_a.AutoSize = true;
            this.lbl_a.Location = new System.Drawing.Point(13, 24);
            this.lbl_a.Name = "lbl_a";
            this.lbl_a.Size = new System.Drawing.Size(13, 13);
            this.lbl_a.TabIndex = 0;
            this.lbl_a.Text = "a";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Draw this square!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pbox_for_shapes
            // 
            this.pbox_for_shapes.Location = new System.Drawing.Point(16, 102);
            this.pbox_for_shapes.Name = "pbox_for_shapes";
            this.pbox_for_shapes.Size = new System.Drawing.Size(628, 167);
            this.pbox_for_shapes.TabIndex = 2;
            this.pbox_for_shapes.TabStop = false;
            // 
            // txt_a1
            // 
            this.txt_a1.Location = new System.Drawing.Point(33, 24);
            this.txt_a1.Name = "txt_a1";
            this.txt_a1.Size = new System.Drawing.Size(100, 20);
            this.txt_a1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(173, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "a";
            // 
            // lbl_b
            // 
            this.lbl_b.AutoSize = true;
            this.lbl_b.Location = new System.Drawing.Point(173, 47);
            this.lbl_b.Name = "lbl_b";
            this.lbl_b.Size = new System.Drawing.Size(13, 13);
            this.lbl_b.TabIndex = 5;
            this.lbl_b.Text = "b";
            // 
            // btn_rectangle
            // 
            this.btn_rectangle.Location = new System.Drawing.Point(176, 75);
            this.btn_rectangle.Name = "btn_rectangle";
            this.btn_rectangle.Size = new System.Drawing.Size(119, 23);
            this.btn_rectangle.TabIndex = 6;
            this.btn_rectangle.Text = "Draw this rectangle!";
            this.btn_rectangle.UseVisualStyleBackColor = true;
            this.btn_rectangle.Click += new System.EventHandler(this.btn_rectangle_Click);
            // 
            // btn_circle
            // 
            this.btn_circle.Location = new System.Drawing.Point(16, 47);
            this.btn_circle.Name = "btn_circle";
            this.btn_circle.Size = new System.Drawing.Size(119, 23);
            this.btn_circle.TabIndex = 7;
            this.btn_circle.Text = "Draw this circle!";
            this.btn_circle.UseVisualStyleBackColor = true;
            this.btn_circle.Click += new System.EventHandler(this.btn_circle_Click);
            // 
            // txt_b2
            // 
            this.txt_b2.Location = new System.Drawing.Point(192, 49);
            this.txt_b2.Name = "txt_b2";
            this.txt_b2.Size = new System.Drawing.Size(100, 20);
            this.txt_b2.TabIndex = 8;
            // 
            // txt_a2
            // 
            this.txt_a2.Location = new System.Drawing.Point(192, 24);
            this.txt_a2.Name = "txt_a2";
            this.txt_a2.Size = new System.Drawing.Size(100, 20);
            this.txt_a2.TabIndex = 9;
            // 
            // FormShapes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 281);
            this.Controls.Add(this.txt_a2);
            this.Controls.Add(this.txt_b2);
            this.Controls.Add(this.btn_circle);
            this.Controls.Add(this.btn_rectangle);
            this.Controls.Add(this.lbl_b);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_a1);
            this.Controls.Add(this.pbox_for_shapes);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_a);
            this.Name = "FormShapes";
            this.Text = "Shapes";
            ((System.ComponentModel.ISupportInitialize)(this.pbox_for_shapes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_a;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pbox_for_shapes;
        private System.Windows.Forms.TextBox txt_a1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_b;
        private System.Windows.Forms.Button btn_rectangle;
        private System.Windows.Forms.Button btn_circle;
        private System.Windows.Forms.TextBox txt_b2;
        private System.Windows.Forms.TextBox txt_a2;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExceptionConsoleTest
{
    class Program
    {
        #region Создание собственных классов исключений - простое наследование от Exception
        [Serializable]  //применить к классу сериализацию - сохранение информации о его структуре в памяти 
                        //и предоставить доступ к этой информации другим объектам системы
                        //каждый класс, наследуемый от исключительной ситуации, должен быть сериализуемым
        class MyException : Exception
        {   //у сериализуемого класса должно быть 4 конструктора: 
            public MyException() { } //"молчаливая" исключительная ситуация
            public MyException(string message) : base(message) { } //исключение с сообщением о происшествии
            public MyException(string message, Exception inner) : base(message, inner) { }//для повторной генерации исключительной ситуации
            protected MyException(System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext context) : base(info, context) { } //обеспечивает сериализацию
    }
        #endregion

        static void Main(string[] args)
        {
            //создаем свой класс исключительных ситуаций 
            //примеры использования унаследованных Ecxeptions
            DateTime d = new DateTime();
            Calendar_Entry ce = new Calendar_Entry();
            try
            {
                ce.Date = new DateTime(1880, 01, 31);
            }
            catch (MyCalendarEntryYearOutOfRangeException ex)
            {
                Console.WriteLine("MyCalendarEntryYearOutOfRangeException: " + ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("ArgumentOutOfRangeException: " + ex.Message);
            }

            try
            {
                ce.SetDate("20.05.1770");
            }
            catch (MyCalendarEntryYearOutOfRangeException ex)
            {
                Console.WriteLine("MyCalendarEntryYearOutOfRangeException: " + ex.Message);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("ArgumentOutOfRangeException: " + ex.Message);
            }

            Calendar_Entry first = new Calendar_Entry();
            first.SetDate("31.01.2000");
            Calendar_Entry second = new Calendar_Entry();
            first.SetDate("02.3.2001");
            Calendar_Entry third = new Calendar_Entry();
            third.SetDate("02.3.2001");

            Console.WriteLine(first);
            Console.WriteLine((first < second));
            Console.WriteLine((first > second));
            Console.WriteLine((third == second)); ???

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }


}

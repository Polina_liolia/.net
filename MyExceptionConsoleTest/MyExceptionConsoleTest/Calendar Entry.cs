﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExceptionConsoleTest
{
    //обертка над стандартным типом DateTime
    public partial class Calendar_Entry
    {
        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set
            {
                if(value.Year > 1900 && value.Year <= DateTime.Today.Year)
                {
                    date = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(); //Выход за пределы интервала
                }
            }
        }

        public void SetDate (string dateString)
        {
            DateTime dt = Convert.ToDateTime(dateString);
            if (dt.Year > 1900 && dt.Year <= DateTime.Today.Year)
            {
                date = dt;
            }
            else
                throw new MyCalendarEntryYearOutOfRangeException();
        }
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace MyExceptionConsoleTest
{
    [Serializable]
    internal class MyCalendarEntryYearOutOfRangeException : ArgumentOutOfRangeException
    {
        public MyCalendarEntryYearOutOfRangeException()
        {
        }

        public MyCalendarEntryYearOutOfRangeException(string message) : base(message)
        {
        }

        public MyCalendarEntryYearOutOfRangeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MyCalendarEntryYearOutOfRangeException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public MyCalendarEntryYearOutOfRangeException(string paramname, string message) : base(paramname, message)
        {
        }
    }
}
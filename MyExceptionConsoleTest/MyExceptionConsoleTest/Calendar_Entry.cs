﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyExceptionConsoleTest
{
    public partial class Calendar_Entry
    {
        #region Перегрузка операторов
        #region Операторы из System.Object
        public override string ToString()
        {
            string strValue = this.Date.ToLongDateString();
            return strValue;
        }

        public override bool Equals(object obj)
        {
            if (this == obj) //для сравнения с объектом передали этот же объект (ссылки равны) 
                return true;
            else if (obj == null || obj.GetType() != this.GetType())    //передан null или приведенный объект другого типа
                return false;
            else
            {
                //сравниваем значения, которые содержат сравниваемые объекты-ссылки
                Calendar_Entry other = obj as Calendar_Entry; //если obj не совместим с типом, будет создана ссылка null
                //аналогично: Calendar_Entry other = (Calendar_Entry)obj; //если obj не совместим с типом, произойдет исключительная ситуация

                return this.ToString() == other.ToString(); //сранение с помощью преобразования к строке - не оптимальный вариант
                //лучше - через реализацию компаратора
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Операторы сравнения
        public static bool operator==(Calendar_Entry lv, Calendar_Entry rv)
        {
            bool result = false;
            string v1 = lv.ToString();
            string v2 = rv.ToString();
            result = (v1 == v2);
            return result;
        }
        public static bool operator !=(Calendar_Entry lv, Calendar_Entry rv)
        {
            bool result = false;
            string v1 = lv.ToString();
            string v2 = rv.ToString();
            result = (v1 != v2);
            return result;
        }
        public static bool operator<(Calendar_Entry lv, Calendar_Entry rv)
        {
            bool result = false;
            string v1 = lv.ToString();
            string v2 = rv.ToString();
            result = String.CompareOrdinal(v1, v2) > 0; //сравнение строк с учетом регистра, но без учета региональных особенностей; сортировка символов
            return result;
        }
        public static bool operator >(Calendar_Entry lv, Calendar_Entry rv)
        {
            bool result = false;
            string v1 = lv.ToString();
            string v2 = rv.ToString();
            result = String.CompareOrdinal(v1, v2) < 0;
            return result;
        }
        public static bool operator <=(Calendar_Entry lv, Calendar_Entry rv)
        {
            bool result = false;
            string v1 = lv.ToString();
            string v2 = rv.ToString();
            result = String.CompareOrdinal(v1, v2) >= 0;
            return result;
        }
        public static bool operator >=(Calendar_Entry lv, Calendar_Entry rv)
        {
            bool result = false;
            string v1 = lv.ToString();
            string v2 = rv.ToString();
            result = String.CompareOrdinal(v1, v2) <= 0;
            return result;
        }
        public static Calendar_Entry operator +(Calendar_Entry lv, Calendar_Entry rv)
        {
            Calendar_Entry newEntry = new Calendar_Entry();
            newEntry.date = lv.Date.AddDays(rv.Date.Day);
            return newEntry;
        }
        #endregion 
        #endregion
    }
}

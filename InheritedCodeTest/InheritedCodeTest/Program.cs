﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace InheritedCodeTest
{
    class Program
    {
        //в user32.dll есть два варианта MessageBox:
        //MessageBoxA - ANSI
        //MessageBoxW - Unicode
        
            //Далее описаны три варианта импорта для MessageBoxW:
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int MessageBox(IntPtr hWnd, String text, String caption, uint type);
        [DllImport("user32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        public static extern int MessageBoxW(IntPtr hWnd, String text, String caption, uint type);
        [DllImport("user32.dll", CharSet = CharSet.Unicode, EntryPoint = "MessageBox")]
        public static extern int MyNewMessageBoxMethod(IntPtr hWnd, String text, String caption, uint type);

        public static void Main(string[] args)
        {
            MessageBox(new IntPtr(0), "Text from MessageBox", "One", 0);
            MessageBox(new IntPtr(0), "Text from MessageBoxW", "Two", 0);
            MessageBox(new IntPtr(0), "Text from MyNewMessageBoxMethod", "Three", 0);
        }
    }
}

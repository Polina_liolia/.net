﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; //to use writer
using System.Globalization;

namespace StreamReaderWriterTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = string.Format("{0:#.##}", 3.1456);//два знака после запятой (с округлением)
            Console.WriteLine(str1);
            using (StreamWriter writer = new StreamWriter(@"d:\Polina\.net\output.txt")) //для записи в указанный файл (по умолчанию - сохраняет в bin\debug)
            {
                writer.WriteLine(str1);
            }
            //writer.Close(); будет выполнен автоматически при использовании using

            using (StreamReader reader = new StreamReader(@"d:\Polina\.net\output.txt"))
            {
                String str3 = reader.ReadLine();
                System.Windows.Forms.MessageBox.Show("result = " + str3); /*чтобы подключить модуль Windows Forms: 
                                                                            Solutions Explorer - References - Add refference -
                                                                            Assemblies - Framework - System.Wondows.Forms*/
            }
            //reader.Close(); будет выполнен автоматически при использовании using

            using (StreamReader reader = new StreamReader(@"d:\Polina\.net\input.txt"))
            {
                string str3 = string.Empty;
                while ((str3 = reader.ReadLine()) != null)
                {
                    System.Windows.Forms.MessageBox.Show("result = " + str3);
                }
            }


            using (StreamReader reader = new StreamReader(@"d:\Polina\.net\input.txt"))
            {
                StringBuilder sb = new StringBuilder();
                string str3 = string.Empty;
                str3 = reader.ReadLine();
                int sz = -1;
                bool result = Int32.TryParse(str3, out sz);
                if (result && sz > 0)
                {
                    int[] arr = new int[sz];
                    for(int i = 0;  (str3 = reader.ReadLine()) != null; i++)
                    {
                        result = Int32.TryParse(str3, out arr[i]);
                        if (!result)
                            arr[i] = -1; 
                    }
                    System.Windows.Forms.MessageBox.Show("Прочитано из файла с разделителями перенос строки:");
                    for (int i = 0; i < sz; i++)
                        System.Windows.Forms.MessageBox.Show(arr[i].ToString());
                }
                else
                    System.Windows.Forms.MessageBox.Show("Empty file");
            }



            using (StreamReader reader = new StreamReader(@"d:\Polina\.net\input_space.txt"))
            {
                string str3 = string.Empty;
                str3 = reader.ReadLine();
                int sz = -1;
                bool result = Int32.TryParse(str3, out sz);
                if (result && sz > 0)
                {
                    int[] arr = new int[sz];
                    //string[] arrStr = new string[sz];
                    str3 = reader.ReadToEnd(); //читает все, до конца файла
                    string[] arrStr = str3.Split(' ');
                    for (int i = 0; i < sz; i++)
                    {
                        result = Int32.TryParse(arrStr[i], out arr[i]);
                        if (!result)
                            arr[i] = -1;
                    }
                    System.Windows.Forms.MessageBox.Show("Прочитано из файла с пробельными разделителями:");
                    foreach (int i in arr)
                        System.Windows.Forms.MessageBox.Show(i.ToString());
                }
                else
                    System.Windows.Forms.MessageBox.Show("Empty file");
            }


            DateTime dt = DateTime.Now;
            Console.WriteLine(dt);
            double d = 3.14;
            Console.WriteLine(d);
            //пример работы с культурой - для вывода в формате, принятом в указанной культуре
            CultureInfo ci = new CultureInfo("en-US"); //"uk-UA" - "en-US"

            string str2 = string.Format("{0:F05}", 3.1456);//пять знаков после запятой, отсутсвующие заполнятся нулем
            Console.WriteLine(str2);

            string str_float = string.Format("{0:#.##}", 3.1456f);//замена типа по умолчанию (double) на float
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}

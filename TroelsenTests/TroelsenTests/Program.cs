﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace TroelsenTests
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(); //cw + Tab + Tab
            Console.WriteLine("{0:c}, {1:d3}, {2:x}", 120, 2, 543 ); //c - currency, d - decimal, x - hex
            //reference to PresentationFramework required:
            System.Windows.MessageBox.Show(string.Format("100000 in hex is {0:x}", 100000));

            //base types static methods:
            Console.WriteLine(int.MaxValue);
            Console.WriteLine(int.MinValue);
            Console.WriteLine(double.MaxValue);
            Console.WriteLine(double.MinValue);
            Console.WriteLine(double.PositiveInfinity);
            Console.WriteLine(double.NegativeInfinity);
            Console.WriteLine(double.Epsilon);
            Console.WriteLine(char.IsDigit('a'));
            Console.WriteLine(char.IsLetter('2'));
            Console.WriteLine(char.IsLetterOrDigit("Dog23", 3));
            Console.WriteLine(char.IsPunctuation('?'));

            //TimeSpan takes hours, minutes and seconds:
            TimeSpan ts = new TimeSpan(4, 20, 33);
            Console.WriteLine(ts);
            Console.WriteLine(ts.Subtract(new TimeSpan(0, 15, 0)));//minus 15 min
            Console.WriteLine(ts.Add(new TimeSpan(0, 20, 0)));//plus 20 min

            //BigInteger creates big nubers without limitations;
            //BigIntegers are unmutable:
            BigInteger bi = new BigInteger(new byte[] { 34, 67, 89, 27, 250, 234, 129, 75, 34, 90 });
            BigInteger bi1 = BigInteger.Parse("999999999999999999999999999999999");
            BigInteger biMult = bi * bi1;
            Console.WriteLine(bi);
            Console.WriteLine(bi1);
            Console.WriteLine(biMult);

            //overflov/underflow control
            short a = 250;
            short b = 100;
            int res_int = a + b; //350 
            Console.WriteLine(res_int);
           
            try
            {
                checked
                {
                    byte res_byte_checked = (byte)(a + b); //overflow under check, exception generation
                }
            }
            catch(OverflowException ex)
            {
                Console.WriteLine(ex.Message);
            }

            unchecked //to not check code if overflow check was swotched on for the whole project
            {
                byte res_byte = (byte)(a + b); //94 - overflow
                Console.WriteLine(res_byte);
            }

            //key word params - method takes variable number of semi-type parametres
            Console.WriteLine(CalcAvg(2, 3.1, 555, 23, 8, 1));

            ShowNiceMsg();
            //named parameters (calling of method with all three default params values, except the second one):
            ShowNiceMsg(text_color: ConsoleColor.Red);

            //nullable
            int? nullable_number = getDataFromDB() ?? 100; //if method returned null, this var takes 100

            //static ctor test
            Account acc = new Account(345);
            Console.WriteLine(acc.sum); //345
            Console.WriteLine(Account.Interest); //0.04 - static prop
            
        }

        //key word params - method takes variable number of semi-type parametres
        public static double CalcAvg(params double[] numbers)
        {
            if (numbers.Length == 0)
                return 0;
            double sum = 0;
            for (int i = 0; i < numbers.Length; i++)
                sum += numbers[i];
            return sum / numbers.Length;
        } 

   
        public static void ShowNiceMsg(string message = "Some nice message", ConsoleColor text_color = ConsoleColor.Black, 
            ConsoleColor background = ConsoleColor.White)
        {
            Console.BackgroundColor = background;
            Console.ForegroundColor = text_color;
            Console.WriteLine(message);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
        }

        //nullable tests
        public static int? getDataFromDB ()
        {
            return null;
        }
    }
}

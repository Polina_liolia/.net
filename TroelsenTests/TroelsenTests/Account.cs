﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TroelsenTests
{
    public class Account
    {
        public double sum;
        static double interest;

         //static property
        public static double Interest
        {
            get  { return interest; }
            set  { interest = value; }
        }

        public Account(double sum)
        {
            this.sum = sum;
        }

        //static constructor - is called once only
        //нельзя перегружать
        //не имеет модификаторов доступа
        //для одного экземпляра программы вызывается только один раз - при создании первого экземпляра класса
        //или перед первым обращением к статическому члену класса
        //выполняется перед любым конструктором уровня экземпляра
       static Account()
        {
            Interest = 0.04;
        }
    }
}

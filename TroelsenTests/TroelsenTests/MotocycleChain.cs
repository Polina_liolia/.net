﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TroelsenTests
{
    class MotocycleChain
    {
        private string name;
        private int speed;

        public MotocycleChain() {  }
        public MotocycleChain(string name, int speed) //main constructor
        {
            this.speed = (speed > 60) ? 60 : speed;
            this.name = name;
        }
        public MotocycleChain(string name) : this(name, 0)   {  }
        public MotocycleChain(int speed) : this ("", speed)  {  }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TroelsenTests
{
    class MathClass
    {
        public const double pi = 3.14; //константное поле - присваивается только при инициализации
                                       //т.к. его значение должно быть известно уже на стадии компиляции
                                       //ЯВЛЯЕТСЯ НЕЯВНО СТАТИЧЕСКИМ

        public readonly double PI; //поле только для чтения - присваивается значение ТОЛЬКО В КОНСТРУКТОРЕ
        //его используют, если значение, которое необходимо присвоить, становится известно только 
        //ВО ВРЕМЯ ВЫПОЛНЕНИЯ 
        //не является неявно статическим
        public MathClass()
        {
            PI = 3.14;
        }

        public static readonly double Pi; //СТАТИЧЕСКОЕ поле для чтения, значение которого
        //становится известным только на этапе выполнения
        //значение устанавливается через статический конструктор:
        static MathClass()
        {
            Pi = 3.14;
        }
        
    }
}

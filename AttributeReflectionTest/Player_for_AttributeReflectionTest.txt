public class Player
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Player(string n, int a)
        {
            Name = n;
            Age = a;
        }
        public void Display()
        {
            Console.WriteLine("���: {0}  �������: {1}");
        }
        public int Payment(int hours, int perhour)
        {
            return hours * perhour;
        }
    }
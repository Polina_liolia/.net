﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AttributeReflectionTest
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Привет, как Вас зовут?");
        //    string userName = Console.ReadLine();
        //    Console.WriteLine("Привет " + userName + ", нажмите любую клавишу...");
        //    Console.ReadKey();
        //}

            /*
        static void Main(string[] args)
        {
            //анализ и вызов методов из другой сборки путем дизассамблирования:
            try
            {
                Assembly asm = Assembly.LoadFrom("MyAssembly.exe"); //загружаем сборку, с которой будем работать
                Type t = asm.GetType("MyAssembly.Program", true, true); //получаем класс Program из этой сборки
                //создаем экземпляр класса Programm
                object obj = Activator.CreateInstance(t);
                //получить метод GetResult:
                MethodInfo method = t.GetMethod("GetResult");
                //вызывем метод, передаем ему значения для параметров и получаем результат:
                object result = method.Invoke(obj, new object[] { 6, 108, 3 });
                Console.WriteLine(result);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        */

        static void Main (string[] args)
        {
            //standart type research:
            //using GetType to obtain type info:
            int A = 42;
            Type type = A.GetType();
            Console.WriteLine(type);
            //using reflection to get info about assembly
            System.Reflection.Assembly info = typeof(Int32).Assembly;
            Console.WriteLine(info);
            Type myType = typeof(Player);
            Console.WriteLine(myType.ToString());
            Console.WriteLine("Структура типа:");
            foreach (MemberInfo mi in myType.GetMembers())
            {
                Console.WriteLine(mi.DeclaringType + " " + mi.MemberType + " " + mi.Name);
            }
            Console.WriteLine("Методы:");
            foreach (MethodInfo method in myType.GetMethods())
            {
                string modificator = "";
                if (method.IsStatic)
                    modificator += "static ";
                if (method.IsVirtual)
                    modificator += "virtual ";
                Console.Write(modificator + method.ReturnType.Name + " " + method.Name + " (");
                //получаем все параметры
                ParameterInfo[] parameters = method.GetParameters();
                for (int i = 0; i < parameters.Length; i++)
                {
                    Console.Write(parameters[i].ParameterType.Name + " " + parameters[i].Name);
                    if (i + 1 < parameters.Length) Console.Write(", ");
                }
                Console.WriteLine(")");
            }
            Console.WriteLine("Конструкторы:");
            foreach (ConstructorInfo ctor in myType.GetConstructors())
            {
                Console.Write(myType.Name + " (");
                // получаем параметры конструктора
                ParameterInfo[] parameters = ctor.GetParameters();
                for (int i = 0; i < parameters.Length; i++)
                {
                    Console.Write(parameters[i].ParameterType.Name + " " + parameters[i].Name);
                    if (i + 1 < parameters.Length) Console.Write(", ");
                }
                Console.WriteLine(")");
            }
            Console.WriteLine("Press any key...");
            Console.ReadLine();
        }
     }
}

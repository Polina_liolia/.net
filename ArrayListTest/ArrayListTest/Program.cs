﻿using System;
using System.Collections;//для работы с коллекциями
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //примеры "языковых" массивов - часть языка С# и платформы .Net
            int[] array = new int[10];
            int[,] matrix = new int[3, 3];
            //коллекцияSysyem.Object - внутри данные хранятся как список, но доступ к ним осуществляется как к элементам массива (по индексатору - [2])
            //использование стандартного ArrayList:
            ArrayList arrayList = new ArrayList();
            int a = 13;
            double d = 3.14;
            char ch = '1';
            string str = "3.14";
            //базовый эл-т ArrayList - System.Object, поэтому он может смешивать данные:
            arrayList.Add(a);
            arrayList.Add(d);
            arrayList.Add(ch);
            arrayList.Add(str);
            int size = arrayList.Count; //Count характерен для списков, т.е. эл-ты идут не друг за другом, а, например, в виде дерева
            //с выводом в консоль нет никаких проблем:
            for (int i = 0; i < size; i++)
                Console.WriteLine(arrayList[i]);
            //Попытка подобного суммирования в данном случае приведет к исключительной ситуации (т.к. коллекция содержит эл-т, не приводимый к double)
            double sum = 0;
            for (int i = 0; i < size; i++)
            {
                double result;
                bool success = Double.TryParse(arrayList[i].ToString(), out result);
                if (!success)
                    Console.WriteLine(string.Format("Тип {0} невозможно преобразовать к double", arrayList[i].GetType().ToString()));
                sum += result;
            }
            Console.WriteLine(string.Format("Sum result: {0}", sum));

            //использование собственной обертки MyArrayList:
            MyArrayList myArrayList = new MyArrayList();
            //обертка реализована так, чтобы предотвратить смешивание данных:
            myArrayList.Add(a);
            myArrayList.Add(d);
            myArrayList.Add(ch);
           // myArrayList.Add(str); //синтаксическая ошибка - строки не преобразуемы к Double
            int my_size = myArrayList.Count; 
            //с выводом в консоль нет никаких проблем:
            for (int i = 0; i < my_size; i++)
                Console.WriteLine(myArrayList[i]);
            //Попытка подобного суммирования в данном случае не приведет к исключительной ситуации (т.к. коллекция более не содержит эл-т, не приводимый к double)
            double my_sum = 0;
            for (int i = 0; i < my_size; i++)
            {
                my_sum += Convert.ToDouble(myArrayList[i].ToString());
            }
            Console.WriteLine(string.Format("My sum result: {0}", my_sum));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;
using System.Diagnostics;

namespace InheritedCodeTest
{
    class Program
    {
        // Use DllImport to import the Win32 MessageBox function.
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int MessageBox(IntPtr hWnd, 
                                            String text, 
                                            String caption, 
                                            uint type);
        /*
         Заметки

         Если значение равно false, имя точки входа с буквой A на конце вызывается,
         когда поле DllImportAttribute.CharSet устанавливается равным CharSet.Ansi,
         а имя точки входа с буквой W на конце — когда поле DllImportAttribute.CharSet
         устанавливается равным CharSet.Unicode.Обычно это поле устанавливается управляемыми компиляторами.
         https://msdn.microsoft.com/ru-ru/library/system.runtime.interopservices.dllimportattribute.exactspelling%28v=vs.110%29.aspx
        */
        [DllImport("user32.dll", CharSet = CharSet.Unicode, 
            ExactSpelling = true)]
        public static extern int MessageBoxW(IntPtr hWnd,
                                          String text,
                                    String caption, 
                                     uint type);
        
        [DllImport("user32.dll", 
            CharSet = CharSet.Unicode, 
            EntryPoint = "MessageBox")]
        public static extern int MyNewMessageBoxMethod(
            IntPtr hWnd,
            String text, 
            String caption, 
            uint type);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        // Find window by Caption only. Note you must pass IntPtr.Zero as the first parameter.
        [DllImport("User32.dll")]
        public static extern int SetForegroundWindow(IntPtr point);

        static void Main(string[] args)
        {
            Process p = Process.Start("notepad++.exe");
            p.WaitForInputIdle();
            IntPtr h = p.MainWindowHandle;
            SetForegroundWindow(h);
            // SendKeys.SendWait("k");
            IntPtr processFoundWindow = p.MainWindowHandle;

            MessageBox(new IntPtr(0),
                       "Hello World!",
                        "Hello Dialog: Test 1",
                        0);
            MyNewMessageBoxMethod(new IntPtr(0),
                "Hello World!",
                "Hello Dialog: Test 2",
                0);
        }
    }
}

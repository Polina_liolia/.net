﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    public class Client : IEquatable<Client>, IComparable, ICloneable
    {
       public static string getAllClientSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" select ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_ID], ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_NAME] , ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_PHONE] , ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_MAIL] ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[CLIENTS] ");
            return sb.ToString();
        }
        private int clients_id;
        public int ID { get { return this.clients_id; } }
        private string clients_name;
        public string Name 
        { 
            set { clients_name = value; }
            get { return clients_name; }
        }
        private string clients_phone;
        public string Phone
        {
            get { return this.clients_phone; }
            set { this.clients_phone = value; }
        }
        private string clients_mail;
        public string Mail
        {
            set { this.clients_mail = value; }
            get { return this.clients_mail; }
        }
        protected Client() { }
        public Client(int clients_id,
            string clients_name, 
            string clients_phone,
            string clients_mail
            )
        {
            this.clients_id = clients_id;
            this.clients_name = clients_name;
            this.clients_phone = clients_phone;
            this.clients_mail = clients_mail;
        }
        #region Equals Implementation
        public bool Equals(Client other)//реализация интерфейса IEquatable
        {
            if (other == null)
                return false;
            if (this.clients_id == other.clients_id)
                return true;
            else
                return false;
        }
        public override bool Equals(Object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Client clientObj = obj as Client;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);//вызов реализации для интерфейса-строгий тип
        }
        public override int GetHashCode()//из System.Object для Dictionary
        {
            return this.clients_id.GetHashCode();
            //должа основываться на полях класса, которые задействованы в Equals
            //из System.Object
        }
        //операторы сравнения "==" и "!=" основываются на Equals из System.Object
        public static bool operator ==(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return Object.Equals(client1, client2);

            return client1.Equals(client2);
        }
        public static bool operator !=(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return !Object.Equals(client1, client2);

            return !(client1.Equals(client2));
        }
        #endregion
        #region реализация IComparable
        public int CompareTo(object obj)//аналог int res = String.Compare(str, str2);
        {
            if (obj == null) return 1;

            Client otherClient = obj as Client;
            if (otherClient != null)
                return this.clients_id.CompareTo(otherClient.clients_id);
            else
                throw new ArgumentException("Object is not a Client");
        }
        #endregion
        #region Clonable
        public Client ShallowCopy()//аналог String clone = (string)str.Clone();//удвоение ссылок:
        {
            return (Client)this.MemberwiseClone();//не зарагивает ссылочные типы
            //внутри нашего объекта - поддержка клонирования .Net-ом
        }
        public object Clone()
        {
            return ShallowCopy();
        }
        #endregion
    }
}

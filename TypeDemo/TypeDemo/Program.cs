﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//работа с основными типами данных .Net

namespace TypeDemo
{
    class Program
    {
        public static void Main(string[] args)
        {
            /*----------------Классы, наследование-----------------*/
            Man m = new Man();
            Console.WriteLine(m);//неявный вызов ToString
            Employee empl = new Employee();
            DateTime dt = DateTime.Now;
            Console.WriteLine(empl.ToString());

            doAction(m);
            doAction(empl);
            // doAction(dt);//ошибка - не удалось привести тип объекта!


            /*----------------Ссылочные и значимые типы----------------*/
            //неявные вызовы ToString, которые корректно реализованы в System.Object/System.ValueType
            MyValueType v1 = new MyValueType(); //не смотря на new, не под контролем сборщика мусора, т.к. это значимый тип(в стеке потока)
            MyRefferenceType r1 = new MyRefferenceType();
            //изменяем значение по умолчанию (по умолч. - 0):
            v1.x = 5;
            r1.x = 5;
            Console.WriteLine(v1.x);
            Console.WriteLine(r1.x);

            MyValueType v2 = v1;
            MyRefferenceType r2 = new MyRefferenceType();//под контролем у сборщика мусора
            r2 = r1;    //две ссылки равны (указывают на одно и то же)

            //изменение значения "первых" переменных:
            r1.x = 12;
            v1.x = 12;

            Console.WriteLine(v1.x);    //12
            Console.WriteLine(v2.x);    //5
            //доступ к одному и тому же из разных ссылок:
            Console.WriteLine(r1.x);    //12
            Console.WriteLine(r2.x);    //12


            /*----------------Массивы-----------------*/

            //создание, описание и инициализация одномерного массива:
            int[] array1 = new int[] { 1, 2, 3 }; //не смотря на то, что это массив примитивных типов, память выделена в куче под наблюдением сборщика мусора
            int[] array2 = array1;
            //неявно вызовется ToString:
            Console.WriteLine(array1);//System.Int32
            Console.WriteLine(array2);//System.Int32

            bool r = (array2 == array1) ? true : false;
            Console.WriteLine(r); //true


            /*----------------Цикл for----------------*/
            int N = array1.Length;//кол-во эл-тов в массиве
            for (int i = 0; i < N; i++)
            {
                Console.WriteLine(array1[i]);
                array1[i] = -1; //можно обратиться к нужному эл-ту
            }


            /*----------------Цикл foreach----------------*/

            foreach(int i in array1)    //работает до 15% быстрее, in - доступ к индексатору
                Console.WriteLine(i);   /*не во всех случаях можно обратиться к нужному эл-ту, со страндартными типами 
                                          происходить только сканирование значений из набора (доступ для чтения)
                                          при переборе массива экземпляров "своих" классов можно допустить редактирование при переборе*/

            /*----------------Функции----------------*/


            printArray(array1); //1, 2, 3 
            ChangeArray(array1, 1, -1);
            printArray(array1); //1, -1, 3

            /*----------------Строки----------------*/
            //тип String - unmutable (неизменяемая) строка
            //StringBuilder - изменяемая строка

            String s1 = "Hello";//не изменяемая строка
            Console.WriteLine(s1); //Hello
            changeStr(s1);//Произошло ли изменение? Нет. Т.к. s1 - не изменяемая строка
            //changeStr(ref s1); //так изменение произойдет (если ф-ция будет соответственно описана)
            Console.WriteLine(s1);//Hello

            String s2 = "Hello" + "," + " world" + "!"; /*4 неявных вызова new (для каждой из отдельных строк) 
                                                        + 3 неявных вызова new (при последовательном формировании итоговой строки)*/

            Console.WriteLine(s2);//Hello, world!  

            int a = 3,
                b = 7;
            swap1(a, b); //не изменит значение переменных
            swap2(ref a, ref b);//изменит значение переменных
            Console.WriteLine("a=" + a);//7
            Console.WriteLine("b=" + b);//3

            //функция, возвращающая нескользо параметров
            int A = 1;
            int B = 2;
            int C = -15;
            double x1, x2;
            calcSolutions(A, B, C, out x1, out x2);
            Console.WriteLine("Calc1:x1={0}, x2={1}", x1, x2); //конкатенатор строк во избежание избыточности при сложении строк
            //обеспечение контроля входных данных
            if (calcSolutions2(A, B, C, out x1, out x2))
                Console.WriteLine("Calc2: x1={0}, x2={1}", x1, x2);
            else
                Console.WriteLine("Действительных корней нет");

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        //не рекомендуется использовать: порождает неявные преобразования:
        public static void doAction(Object obj)
        {
            Man m = ((Man)obj);
        }

        public static void printArray(int[] array)
        {
            Console.WriteLine(array);
            foreach (int i in array)
                Console.WriteLine(i);
        }

        public static void ChangeArray(int[] array, int pos, int v)
        {
            array[pos] = v;
        }

       // public static void changeStr(ref string str) //получить возможность изменять строку
        public static void changeStr(string str)
        {
            str = "new value string";/*т.к. строки неизменяемы, создается локальная неявная неизменяемая строка, 
                                       кторая будет вырезана компилятором во избежание накладных расходов*/
        }

        public static void testString(string str)
        {
            str = "новая " + "строка " + "создается всегда";
        }

        //Функция для обмена значений двух переменных
        //вариант 1 - неверно
        public static void swap1 (int A, int B)
        {
            int tmp = A;
            A = B;
            B = tmp;
        }

        //вариант 2 - верно
        public static void swap2 (ref int A, ref int B)
        {
            int tmp = A;
            A = B;
            B = tmp;
        }

        //функция, возвращающая нескользо значений:
        public static void calcSolutions (int a, int b, int c, out double x1, out double x2)//параметры с ключ.словом out должны быть обязательно заполнены в ф-ции
                                                                                            //иначе - синтаксическая ошибка
        {
            double d = Math.Sqrt(Math.Pow(b, 2.0) - 4 * a * c);
            x1 = (-b - Math.Sqrt(d)) / (2 * a);
            x2 = (-b + Math.Sqrt(d)) / (2 * a);
        }
        //с проверкой корретности входных данных
        public static bool calcSolutions2(int a, int b, int c, out double x1, out double x2)
        {
            bool result = false;
            double d = Math.Sqrt(Math.Pow(b, 2.0) - 4 * a * c);
            if (d >= 0)
            {
                result = true;
                x1 = (-b - Math.Sqrt(d)) / (2 * a);
                x2 = (-b + Math.Sqrt(d)) / (2 * a);
            }
            else
            {
                result = false;
                x1 = 0;
                x2 = 0;
            }
            return result;
        }
    }

    // public sealed class Man //sealed - запрет наследования
    public class Man//[:System.Object] - неявное наследование (методы toString, getType...)
        
    {
        public string name; //поле открытое - field
        protected int age; //поле - защищенное - доступное для наследников
        public int Age { get { return this.age; } } //свойство property для чтения

    }

    public class Employee : Man
    {
        public int IdCode;
    }

    public struct MyValueType
    {
        public int x;
    }

    public class MyRefferenceType
    {
        public int x;
    }

    
}


﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullableDBNullTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = 10;
            int? y = 10;
            if (x.HasValue) //if (y!=null)
                Console.WriteLine(x.Value);
            else
                Console.WriteLine("undefined");
            #region Преобразование типов
            int? n = null;
            //int m1 = n; //ошибка на этапе компиляции - невозможно неявное приведение
            //int m2 = (int)n; //явное приведение (exception если null)
            //int m3 = n.Value; //exception, т.к. в переменной содержится null
            #endregion

            #region Операторы сравнения
            int? num1 = 10;
            int? num2 = null;
            if (num1 >= num2)//всегда false, т.к. null несравнимая величина
                Console.WriteLine("num1 >= num2 returns true");
            else //выполнится эта ветка, но значение num1 не меньше num2
                Console.WriteLine("num1 >= num2 returns false");

            if (num1 != num2)//true - работает
                Console.WriteLine("num1 != num2 returns true");
            else
                Console.WriteLine("num1 != num2 returns false");

            if (num1 == num2)//false - работает
                Console.WriteLine("num1 == num2 returns true");
            else
                Console.WriteLine("num1 == num2 returns false");

            num1 = null;
            //два значения null неразличимы
            Console.WriteLine("num1 = null; num2 = null");

            if (num1 >= num2)//всегда false, т.к. null несравнимая величина
                Console.WriteLine("num1 >= num2 returns true");
            else //выполнится эта ветка, но значение num1 не меньше num2
                Console.WriteLine("num1 >= num2 returns false");

            if (num1 != num2)//true - работает
                Console.WriteLine("num1 != num2 returns true");
            else
                Console.WriteLine("num1 != num2 returns false");

            if (num1 == num2)//false - работает
                Console.WriteLine("num1 == num2 returns true");
            else
                Console.WriteLine("num1 == num2 returns false");


            #endregion

            string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=db28pr10;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {

                con.Open();
                using (SqlCommand command = new SqlCommand(Customer.selectAll(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string nick_name = reader["NICKNAME"].ToString();
                        //если пришел null - вызовется его ToString
                        string phone = reader["PHONE"].ToString();
                        string email = reader["EMAIL"].ToString();
                        int ID_Men;
                        string id_men = string.Empty;
                        Int32.TryParse(reader["ID_MEN"].ToString(), out ID_Men); //если столбец null - явное приведение невозможгно
                        string name = reader["MEN_NAME"].ToString();
                        string lname = reader["LNAME"].ToString();
                        int year;
                        Int32.TryParse(reader["YEAR"].ToString(), out year);

                    }



                }
            }
        }
    }
    public static class MyExtensions
    {
        public static T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null || obj== DBNull.Value)
            //null - это пустая ссылка, а DBNull.Value - это null из БД
            {
                return default(T);//преобразовать в значение по умолчанию 
            }
            else
            {
                return (T)obj; //если не null, то явное приведение
                //аналог (int)obj или (double)obj
            }
        }

        public static T? GetValueOrNull<T>(this string valueAsString) //для вызова вида valueAsString.GetValueOrNull(...)
            where T : struct
        {
            if (string.IsNullOrEmpty(valueAsString))
                return null;
            return (T)Convert.ChangeType(valueAsString, typeof(T));
        }
    }
}


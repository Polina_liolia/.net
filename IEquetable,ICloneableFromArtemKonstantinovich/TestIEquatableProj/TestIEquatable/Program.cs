﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TestIEquatable
{
    class Program
    {
        static void Main(string[] args)
        {
            Person applicant1 = new Person("Jones", "099-29-4999");
            Person applicant2 = new Person("Jones", "199-29-3999");
            Person applicant3 = new Person("Jones", "299-49-6999");

            List<Person> applicants = new List<Person>();
            applicants.Add(applicant1);
            applicants.Add(applicant2);
            applicants.Add(applicant3);

            ArrayList list = new ArrayList();
            list.Add(applicant1);
            list.Contains(applicant1); //задействована equals из System.Object
            Person candidate = new Person("Jones", "199-29-3999");
            //задействована equals из интрфейса IEquatable
            if (applicants.Contains(candidate))
                Console.WriteLine("Found {0} (SSN {1}).",
                                   candidate.LastName, candidate.SSN);
            else
                Console.WriteLine("Applicant {0} not found.", candidate.SSN);

            // Call the shared inherited Equals(Object, Object) method.
            // It will in turn call the IEquatable(Of T).Equals implementation.
            Console.WriteLine("{0}({1}) already on file: {2}.",
                              applicant2.LastName,
                              applicant2.SSN,
                              Person.Equals(applicant2, candidate));
            #region Clonable Test
            // Create an instance of Person and assign values to its fields.
            Person p1 = new Person("Jones", "199-29-3999");
            p1.Age = 42;
            p1.Name = "Sam";
            p1.IdInfo = new IdInfo(6565);
            // Perform a shallow copy of p1 and assign it to p2.
            Person p2 = p1.ShallowCopy();
            // Display values of p1, p2
            Console.WriteLine("Original values of p1 and p2:");
            Console.WriteLine("   p1 instance values: ");
            DisplayValues(p1);
            Console.WriteLine("   p2 instance values:");
            DisplayValues(p2);

            // Change the value of p1 properties and display the values of p1 and p2.
            p1.Age = 32;
            p1.Name = "Frank";
            p1.IdInfo.IdNumber = 7878;
            Console.WriteLine("\nValues of p1 and p2 after changes to p1:");
            Console.WriteLine("   p1 instance values: ");
            DisplayValues(p1);
            Console.WriteLine("   p2 instance values:");
            DisplayValues(p2);

            // Make a deep copy of p1 and assign it to p3.
            Person p3 = p1.DeepCopy();
            // Change the members of the p1 class to new values to show the deep copy.
            p1.Name = "George";
            p1.Age = 39;
            p1.IdInfo.IdNumber = 8641;
            Console.WriteLine("\nValues of p1 and p3 after changes to p1:");
            Console.WriteLine("   p1 instance values: ");
            DisplayValues(p1);
            Console.WriteLine("   p3 instance values:");
            DisplayValues(p3);
         
            #endregion
        }
        public static void DisplayValues(Person p)
        {
            Console.WriteLine("      Name: {0:s}, Age: {1:d}", p.Name, p.Age);
            Console.WriteLine("      Value: {0:d}", p.IdInfo.IdNumber);
        }

    }
    //
    public class IdInfo
    {
        public int IdNumber;

        public IdInfo(int IdNumber)
        {
            this.IdNumber = IdNumber;
        }
    }
    public class Person : IEquatable<Person>, ICloneable
    {
        private string uniqueSsn;
        public string SSN
        {
            get { return this.uniqueSsn; }
            set { uniqueSsn = value; }
        }
        private string lName;
        public string LastName
        {
            get { return this.lName; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new ArgumentException("The last name cannot be null or empty.");
                else
                    this.lName = value;
            }
        }
        #region additional Fields
        public int Age;
        public string Name;
        public IdInfo IdInfo;
        #endregion
        public Person(string lastName, string ssn)
        {
            this.SSN = ssn;
            this.LastName = lastName;
        }
        protected Person() { }
        #region Equals Implementation
        public bool Equals(Person other)//реализация интерфейса IEquatable
        {
            if (other == null)
                return false;
            if (this.uniqueSsn == other.uniqueSsn)
                return true;
            else
                return false;
        }
        public override bool Equals(Object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Person personObj = obj as Person;
            if (personObj == null)
                return false;
            else
                return Equals(personObj);//вызов реализации для интерфейса-строгий тип
        }
        public override int GetHashCode()//из System.Object для Dictionary
        {
            return this.SSN.GetHashCode();
            //должа основываться на полях класса, которые задействованы в Equals
            //из System.Object
        }
        //операторы сравнения "==" и "!=" основываются на Equals из System.Object
        public static bool operator ==(Person person1, Person person2)
        {
            if (((object)person1) == null || ((object)person2) == null)
                return Object.Equals(person1, person2);

            return person1.Equals(person2);
        }
        public static bool operator !=(Person person1, Person person2)
        {
            if (((object)person1) == null || ((object)person2) == null)
                return !Object.Equals(person1, person2);

            return !(person1.Equals(person2));
        }
        #endregion
    //
        public Person ShallowCopy()//аналог String clone = (string)str.Clone();//удвоение ссылок:
        {
            return (Person)this.MemberwiseClone();//не зарагивает ссылочные типы
            //внутри нашего объекта - поддержка клонирования .Net-ом
        }
        public Person DeepCopy()//правильная реализация интерфейса ICloneable
        {
            Person other = (Person)this.MemberwiseClone();
            //все "свои" ссылочные типы заполняем сами
            other.IdInfo = new IdInfo(IdInfo.IdNumber);
            other.Name = String.Copy(Name);
            return other;
        }
    //
        public object Clone()
        {
            return DeepCopy();
        }
    }
    //Person herClone = (Person)emilyBronte.Clone();
    public class MyPerson : ICloneable
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Address PersonAddress { get; set; }

        public object Clone()
        {
            MyPerson newPerson = (MyPerson)this.MemberwiseClone();
            newPerson.PersonAddress = (Address)this.PersonAddress.Clone();
            return newPerson;
        }
    }
    public class Address : ICloneable
    {
        public int HouseNumber { get; set; }
        public string StreetName { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    //
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using itstep.Kharkov.Tester.MyObjects;

namespace GraphicsGUIWinFormTest
{
    public partial class mainFormCS : Form
    {
        public mainFormCS()
        {
            InitializeComponent();
        }
        //событие перерисовки
        private void mainFormCS_Paint(object sender, PaintEventArgs e)
        {
            Graphics gr = Graphics.FromHwnd(this.Handle);
            Pen mypen = new Pen(Color.Black, 1); //black pen, width 1
            gr.DrawLine(mypen, lbl_a.Left - 100, lbl_a.Top - 3, lbl_b.Left + lbl_b.Width + 100, lbl_a.Top - 3);
            //рисуем стрелки направления, длина стрелки 10, высота 5
            gr.DrawLine(mypen, lbl_b.Left + lbl_b.Width + 100,
                lbl_a.Top - 3, lbl_b.Left + lbl_b.Width + 100 - 10,
                lbl_a.Top - 3 + 5);//down
            gr.DrawLine(mypen, lbl_b.Left + lbl_b.Width + 100,
                lbl_a.Top - 3, lbl_b.Left + lbl_b.Width + 100 - 10,
                lbl_a.Top - 3 - 5);//down
            gr.DrawString("x", this.Font, Brushes.Black,    //Font прорисовывает контур символа, кисть Brushes.Black его закрашивает
                lbl_b.Left + lbl_b.Width + 100 - 10,
                lbl_a.Top - 3 + 5); //рисуем имя оси рядом со стрелкой
            //рисуем отметки точек а и b:
            gr.DrawLine(mypen, lbl_a.Left + lbl_a.Width/2,
                lbl_a.Top - 3 - 2, lbl_a.Left + lbl_a.Width / 2,
                lbl_a.Top - 3 + 2);//a
            gr.DrawLine(mypen, lbl_b.Left + lbl_b.Width / 2,
                lbl_a.Top - 3 - 2, lbl_b.Left + lbl_b.Width / 2,
                lbl_a.Top - 3 + 2);//b
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            string strValue = txtX.Text;
            #region Примеры опасных преобразований - конвертирования типов из строковых в целые
            int x;
            try
            {
                x = Convert.ToInt32(strValue);
            }
            catch(FormatException ex)//ожидаемое исключение
            {
                MessageBox.Show(String.Format("Введенное значение {0} невозможно преобразовать в целое число.Подробности: {1}", strValue, ex.Message), "Ошибка");
                return;
            }
            catch(Exception ex)//не ожидаемое исключение, которое не должно возникнуть
            {
                MessageBox.Show("Введено неверное значение.Подробности: " + ex.Message, "Ошибка");
                return;
            }
            #endregion
            if ((x >= 3) && (x <= 14))
            {
                MessageBox.Show(String.Format("x = {0} принадлежит интервалу", x), "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show(String.Format("x = {0} не принадлежит интервалу", x), "Результат", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /*  Чтобы подключить dll:
            references/контекстное меню/ - Add - Browse - находим dll в папке bin/debud */
        //dll должна быть создана в том же Framework, что и проект, к которому она подключается.
        //Посмотреть версию Framework: В Solution explorer - контекстное меню проекта - Properties
        //Требуемую версию Framework можно указать в ф-ле appconfig
        private void btn_testDll_Click(object sender, EventArgs e)
        {
            this.Text = itstep.Kharkov.Tester.MyObjects.MyMessager.formatMSG("Привет мир!"); //обращение по полному пути
            MessageBox.Show(MyMessager.formatMSG("Hello, world!"));//такое обращение возможно только с using itstep.Kharkov.Tester.MyObjects;
        }

        private void lbl_a_Click(object sender, EventArgs e)
        {

        }
    }
}

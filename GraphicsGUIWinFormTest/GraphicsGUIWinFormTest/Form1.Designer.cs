﻿namespace GraphicsGUIWinFormTest
{
    partial class mainFormCS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_a = new System.Windows.Forms.Label();
            this.lbl_b = new System.Windows.Forms.Label();
            this.lbl_inputX = new System.Windows.Forms.Label();
            this.txtX = new System.Windows.Forms.TextBox();
            this.btnCheck = new System.Windows.Forms.Button();
            this.btn_testDll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_a
            // 
            this.lbl_a.AutoSize = true;
            this.lbl_a.Location = new System.Drawing.Point(118, 176);
            this.lbl_a.Name = "lbl_a";
            this.lbl_a.Size = new System.Drawing.Size(25, 13);
            this.lbl_a.TabIndex = 0;
            this.lbl_a.Text = "a=3";
            this.lbl_a.Click += new System.EventHandler(this.lbl_a_Click);
            // 
            // lbl_b
            // 
            this.lbl_b.AutoSize = true;
            this.lbl_b.Location = new System.Drawing.Point(416, 176);
            this.lbl_b.Name = "lbl_b";
            this.lbl_b.Size = new System.Drawing.Size(31, 13);
            this.lbl_b.TabIndex = 1;
            this.lbl_b.Text = "b=14";
            // 
            // lbl_inputX
            // 
            this.lbl_inputX.AutoSize = true;
            this.lbl_inputX.Location = new System.Drawing.Point(27, 243);
            this.lbl_inputX.Name = "lbl_inputX";
            this.lbl_inputX.Size = new System.Drawing.Size(142, 26);
            this.lbl_inputX.TabIndex = 2;
            this.lbl_inputX.Text = "Введите целое значение Х\r\nиз интервала";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(193, 243);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(100, 20);
            this.txtX.TabIndex = 3;
            this.txtX.Text = "0";
            // 
            // btnCheck
            // 
            this.btnCheck.Location = new System.Drawing.Point(193, 278);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(95, 23);
            this.btnCheck.TabIndex = 4;
            this.btnCheck.Text = "Определить";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btn_testDll
            // 
            this.btn_testDll.Location = new System.Drawing.Point(328, 278);
            this.btn_testDll.Name = "btn_testDll";
            this.btn_testDll.Size = new System.Drawing.Size(75, 23);
            this.btn_testDll.TabIndex = 5;
            this.btn_testDll.Text = "Test dll";
            this.btn_testDll.UseVisualStyleBackColor = true;
            this.btn_testDll.Click += new System.EventHandler(this.btn_testDll_Click);
            // 
            // mainFormCS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 457);
            this.Controls.Add(this.btn_testDll);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.txtX);
            this.Controls.Add(this.lbl_inputX);
            this.Controls.Add(this.lbl_b);
            this.Controls.Add(this.lbl_a);
            this.Name = "mainFormCS";
            this.Text = "mainFormCS";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.mainFormCS_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_a;
        private System.Windows.Forms.Label lbl_b;
        private System.Windows.Forms.Label lbl_inputX;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.Button btn_testDll;
    }
}


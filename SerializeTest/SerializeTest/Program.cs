﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SerializeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var myClassCollection = new MyClassCollection
            {
                Collection = new List<MyClass>
                {
                    new MyClass {Name = "name1", Value = "val1", ServiceField = "bla-bla" },
                    new MyClass {Name = "name2", Value = "val2", ServiceField = "bla-bla" },
                    new MyClass {Name = "name3", Value = "val3", ServiceField = "bla-bla" },
                    new MyClass {Name = "name4", Value = "val4", ServiceField = "bla-bla" }
                }
            };

            //----------Действия на сервере-------------
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(MyClassCollection));
            StringWriter stringWriter = new StringWriter();
            xmlSerializer.Serialize(stringWriter, myClassCollection);
            
            
            //----------Действия на клиенте-------------
            //После этого получаем xml в виде строки из потока
            string xml = stringWriter.ToString();
            Console.WriteLine(xml);
            //и десереализуем:
            var serializedData = xml;
            var stringReader = new StringReader(serializedData);
            MyClassCollection collection = (MyClassCollection)xmlSerializer.Deserialize(stringReader);
            foreach(MyClass mc in collection.Collection)
                Console.WriteLine("Name: {0}, Value: {1}, Field: {2}", mc.Name, mc.Value, mc.ServiceField);


            #region Serialization.Formatters.Binary
            Console.WriteLine("Test Serialization.Formatters.Binary");
            UserInfo userData = new UserInfo();
            userData.UserName = "Bob";
            userData.UserPassword = 123;
            // BinaryFormatter сохраняет данные в двоичном формате. Чтобы получить доступ к BinaryFormatter, понадобится
            // импортировать System.Runtime.Serialization.Formatters.Binary
            BinaryFormatter binFormat = new BinaryFormatter();
            // Сохранить объект в локальном файле.
            using (Stream fStream = new FileStream("user.dat",
               FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, userData);
            }
            LoadUserInfoFromBinaryFile("user.dat");
            Console.WriteLine("Press any key");
            Console.ReadKey();
            #endregion
        }

        static void LoadUserInfoFromBinaryFile(string fileName)
        {
            BinaryFormatter binFormat = new BinaryFormatter();

            using (Stream fStream = File.OpenRead(fileName))
            {
                UserInfo userFromDisk =
                     (UserInfo)binFormat.Deserialize(fStream);
                Console.WriteLine("{0},pass {1}", userFromDisk.UserName, userFromDisk.UserPassword);
            }
        }
    }



    public class MyClass
    {
        [XmlElement("Name")]
        public string Name { get; set; } //это будет элементом
        [XmlAttribute("Value")]
        public string Value { get; set; } //это будет атрибутом
        [XmlIgnore]
        public string ServiceField { get; set; } //это не будет сереализироваться/десереализироваться
    }

    public class MyClassCollection
    {
        [XmlArray("Collection"), XmlArrayItem("Item")]
        public List<MyClass> Collection { get; set; }
    }



    public static class SerializeExtension
    {

    }
}

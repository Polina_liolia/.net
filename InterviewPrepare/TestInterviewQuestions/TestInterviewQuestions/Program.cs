﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInterviewQuestions
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee empl = new Employee("Petia", 32, "Marketing");
            Console.WriteLine(empl);

            int[] arr = new int[] { 5, 12, 3, 7, 9, 1, 10 };
            Array.Sort(arr, new Cmp());
            foreach(int x in arr)
                Console.WriteLine(x);
            Console.WriteLine();
            Hashtable ht = new Hashtable();
            ht.Add("seven", 7);
            ht.Add("esven", 77);
            Console.WriteLine(ht["seven"]);
        }
    }

    public class Cmp : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            return x > y ? -1 : x==y ? 0 : 1;
        }
    }

    public class Person
    {
        protected string Name { get; set; }
        protected int age;

        public Person(string name, int age)
        {
            Name = name;
            this.age = age;
        }
    }

    public class Employee : Person
    {
        public string Departmaent { get; set; }
        public Employee(string name, int age, string department):base(name, age)
        {
            this.Departmaent = department;
        }
        public override string ToString()
        {
            return $"{Name} : {this.age}, {Departmaent} dept.";
        }
    }
}

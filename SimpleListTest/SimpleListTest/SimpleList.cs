﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
    public class SimpleList : IList //для использования моего класса в списках
    {
        private object[] _contents = new object[8];
        //инициализация - до выполнение конструктора, но в момент его вызова через оператор new
        //private object[] _contents;
        private int _count;
        public SimpleList()
        {
            this._count = 0;
            //_contents = new object[this._count];
        }
        public void PrintContents()
        {
            Console.WriteLine("List has a capacity of {0} and currently has {1} elements.", _contents.Length, _count);
            Console.Write("List contents:");
            for (int i = 0; i < Count; i++)
            {
                Console.Write(" {0}", _contents[i]);
            }
            foreach (object _content in _contents)
            {
                Console.Write(" {0}", _content);
            }
            Console.WriteLine();
        }

        #region IList Members
        public int Add(object value)
        {
            if (_count < _contents.Length)
            {
                _contents[_count] = value;
                _count++;
                return (_count - 1);
            }
            else
            {
                return -1; //изменение размера и перезапись
                /*
             Если при добавлении элемента оказывается,
             что массив полностью заполнен,
             будет создан новый массив размером
             (n * 3) / 2 + 1,
             в него будут помещены все элементы
             из старого массива + новый, добавляемый элемент    
             */
            }
        }
        public void Remove(object value)
        {
            RemoveAt(IndexOf(value));
        }
        public void RemoveAt(int index)
        {
            if ((index >= 0) && (index < Count))
            {
                for (int i = 0; i < Count - 1; i++)
                {
                    _contents[i] = _contents[index + 1];
                }
            }
            _count--;
        }
        public void Clear()
        {
            _count = 0;
        }
        public bool Contains(object value)
        {
            bool inList = false;
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i] == value)
                {
                    inList = true;
                    break;
                }
            }
            return inList;
        }
        public int IndexOf(object value)
        {
            int itemIndex = -1;
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i] == value)
                {
                    itemIndex = i;
                    break;
                }
            }
            return itemIndex;
        }
        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }
        public object this[int index]
        {
            get
            {
                return this._contents[index];
            }
            set
            {
                this._contents[index] = value;
            }
        }
        public bool IsFixedSize
        {
            get
            {
                return true;
            }
        }
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        } //элементы не редактируемые, если false
        #endregion

        #region ICollection Members
        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }
        public int Count
        {
            get
            {
                return this._count;
            }
        }
        public bool IsSynchronized //коллекция не потокобезопасная | не синхронизируемая
        {
            get
            {
                return false;
            }
        }
        #endregion

        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}

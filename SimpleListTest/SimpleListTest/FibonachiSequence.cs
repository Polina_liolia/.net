﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
    /// <summary>
    /// Класс, генерирующий значения последовательности Фибоначчи
    /// </summary>
    public class FibonachiSequence : IEnumerator, IEnumerable
    {
        int[] fibonachi = new int[] { 1, 1, 2, 3, 5, 8, 13, 21 };
        int index = -1;
        
        #region IEnumerator
        public object Current
        {
            //set не реализуется, т.к. foreach - только для чтения
            get  { return fibonachi[index];  }
        }

        public bool MoveNext()
        {
            if (index == fibonachi.Length - 1)
            {
                Reset();//сбрасываем счетчик
                return false; //нет следующего эл-та
            }
            index++;//перемещаемся на след.эл-т
            return true;//перемещение осуществлено, индекс настроен на след.эл-т
        }

        public void Reset()
        {
            index = -1;
        }

        #endregion

        #region IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }
        #endregion
    }
}

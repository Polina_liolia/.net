﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDoubleNamespace
{
    public class MyDouble
    {
        double d;

        public MyDouble()
        {
            D = 0.0;
        }
        public MyDouble(double d)
        {
            this.D = d;
        }
        public MyDouble(float d)
        {
            this.D = (double)d;
        }

        public double D
        {
            get { return d; }
            set { d = value; }
        }

        public override string ToString()
        {
            return string.Format("{0}", this.D);
        }

        public static MyDouble operator +(MyDouble obj1, MyDouble obj2)
        {
            return new MyDouble (obj1.D + obj2.D);
        }

        public static bool operator ==(MyDouble obj1, MyDouble obj2)
        {
            double eps = 0.001;
            return Math.Abs(obj1.d - obj2.d) <= eps;
        }

         public static bool operator !=(MyDouble obj1, MyDouble obj2)
        {
            double eps = 0.001;
            return Math.Abs(obj1.d - obj2.d) > eps;
        }

        public override bool Equals(object obj)
        {
            if (this == obj) //для сравнения с объектом передали этот же объект (ссылки равны) 
                return true;
            else if (obj == null || obj.GetType() != this.GetType())    //передан null или приведенный объект другого типа
                return false;
            else
            {
                //сравниваем значения, которые содержат сравниваемые объекты-ссылки
               MyDouble other = obj as MyDouble; //если obj не совместим с типом, будет создана ссылка null
                 //если obj не совместим с типом, произойдет исключительная ситуация
                return this.ToString() == other.ToString(); //сранение с помощью преобразования к строке - не оптимальный вариант
                //лучше - через реализацию компаратора
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

       
        public static bool operator <(MyDouble obj1, MyDouble obj2)
        {
            return obj1.d < obj2.d;
        }
        public static bool operator >(MyDouble obj1, MyDouble obj2)
        {
            return obj1.d > obj2.d;
        }
    }
}

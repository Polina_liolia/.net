﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
    /// <summary>
    /// класс-обёртка над массивом из Book
    /// </summary>
    public class Library : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            return this.books.GetEnumerator();
            //вернули реализацию нашего массива
            //Enumerator - это интерфейс Итератора
        }
        private Book1[] books;
        public Library()
        {
            books = new Book1[]
            {
                new Book1{ Name = "Книга 1"},
                new Book1{ Name = "Вторая Книга"},
                new Book1 { Name = "Букварь"}
            };
        }
        public int Lenth { get { return books.Length; } }
        public Book1 this[int index]
        {
            get { return books[index]; }
            set { books[index] = value; }
        }

        //IEnumerable для первых max эл-тов коллекции
        public IEnumerable GetBooks(int max)
        {
            //на каждом проходе этого цикла создается копия эл-та в кеше (в оперативной памяти)
            for (int i = 0; i < max; i++)
            {
                if (i == books.Length)
                {
                    yield break; //кеширование
                }
                else
                {
                    yield return books[i]; //кеширование
                }
            }
            //кеш существует только в текущей области видимости, при выходе из нее сбрасывается
        }

    }
}

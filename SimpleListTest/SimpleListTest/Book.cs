﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
    public class Book
    {
        public string Name { set; get; }
        public override string ToString()
        {
            return string.Format("Book name: {0}", this.Name);
        }
    }
    /// <summary>
    /// Класс-обертка над массивом из Book
    /// </summary>
    public class Library : IEnumerable
    {
        private Book[] books;
        public Library()
        {
            books = new Book[]
            {
                new Book {Name="Book1" },
                new Book {Name="Book2" },
                new Book {Name="Book3" }
            };
        }

        public int Length
        {
            get { return books.Length; }
        }

        public Book this[int index]
        {
            get { return books[index]; }
            set { books[index] = value; }
        }

        public IEnumerator GetEnumerator()
        {
            return books.GetEnumerator();
            //вернули реализацию, существующую для массива
            //Enumerator - интерфейс итератора
        }
    }
}

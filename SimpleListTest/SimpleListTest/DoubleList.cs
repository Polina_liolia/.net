﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDoubleNamespace;

namespace DoubleListTest
{
    public class DoubleList : IList
    {
        //эта инициализация проходит до выполнения команд конструктора, но в момент его вызова оператором new
        private MyDouble[] _contents = new MyDouble[8];
        private int _count;
        //выделено памяти на 8 элементов, но пока используется 0
        public DoubleList()
        {
            _count = 0;
        }

        public void PrintContents()
        {
            Console.WriteLine("List has a capasity of {0} and currently has {1} element(s).", _contents.Length, _count);
            Console.WriteLine("List contains:");
            for (int i = 0; i < Count; i++)
            {
                Console.WriteLine("{0}", _contents[i]);
            }
        }

        public MyDouble this[int index]
        {
            get
            {
                return _contents[index];
            }

            set
            {
                _contents[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public int Add(MyDouble value)
        {
            if (_count >= _contents.Length)
            {
                //resize
                int new_size = (this._contents.Length * 3) / 2 + 1;
                MyDouble[] contents_new = new MyDouble[new_size];
                _contents.CopyTo(contents_new, 0);
                _contents = contents_new;
            }
            _contents[_count] = (MyDouble)value;
            _count++;
            return (_count - 1); //returns index of the added element 
        }
       
        public void RemoveAt(int index)
        {
            if (index >= 0 && index < Count)
                for (int i = index; i < Count - 1; i++)
                {
                    _contents[i] = _contents[i + 1];
                }
            _count--;
        }

        public int IndexOf(MyDouble value)
        {
            int itemIndex = -1;
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i] == value)
                {
                    itemIndex = i;
                    break;
                }
            }
            return itemIndex;
        }

        public void Remove(MyDouble value)
        {
            this.RemoveAt(this.IndexOf(value));
        }
        public void Clear()
        {
            _count = 0;
        }

        public bool Contains(MyDouble value)
        {
            bool inList = false;
            for (int i = 0; i < Count; i++)
            {
                if (_contents[i] == value)
                {
                    inList = true;
                    break;
                }
            }
            return inList;
        }



        public bool IsFixedSize //осуществляется ли resize
        {
            get
            {
                return false;
            }
        }

        public bool IsReadOnly //реализован ли метод set в индексаторе (возможно ли изменение элементов через индексатор)
        {
            get
            {
                return false;
            }
        }

        public bool IsSynchronized //потокобезопасная ли коллекция
        {
            get
            {
                return false;
            }
        }

        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        object IList.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        public int Add(object value)
        {
            throw new NotImplementedException();
        }

        public bool Contains(object value)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        public void Remove(object value)
        {
            throw new NotImplementedException();
        }
    }
}

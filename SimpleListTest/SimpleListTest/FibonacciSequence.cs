﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
    /// <summary>
    /// класс, генерирующий последовательность Фибоначи
    /// </summary>
    public class FibonacciSequence : IEnumerator, IEnumerable
    {
        //f1 = 1;
        //f2 = 1;
        //fi = (fi - 1) + (fi - 2);
        int[] fibonacci = new int[] { 1, 1, 2, 3, 5, 8, 13, 21 };
        int index = -1;
        #region Реализация IEnumerator
        //сеттера нет, foreach - только чтение
        public object Current
        {
            get { return fibonacci[index]; }
        }
        public bool MoveNext()
        {
            if (index == fibonacci.Length - 1)
            {
                Reset(); //нет следующего 
                //были уже звлечены все
                return false;
            }
            index++; //сдвинулись на одни
            return true; //следующий есть
            //и текущий на него настроен
        }
        public void Reset()
        {
            index = -1;
        }
        #endregion
        #region Реализация IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }

        
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DoubleListTest;
using MyDoubleNamespace;

namespace SimpleListTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DoubleList test1 = new DoubleList();
            MyDouble a = new MyDouble();
            MyDouble b = new MyDouble(3.14);
            MyDouble c = a + b;
            MyDouble d = new MyDouble(3.2f);
            Console.Write(a != b);
            Console.Write(b == c);
            Console.Write(b > c);
            Console.Write(a < b);
            Console.WriteLine("Populate the list:");
            test1.Add(a);
            test1.Add(b);
            test1.Add(c);
            test1.Add(d);
            test1.PrintContents();

            SimpleList test = new SimpleList();
            Console.WriteLine("Populate this List");
            test.Add("one");
            test.Add("two");
            test.Add("three");
            test.Add("four");
            test.Add("five");
            test.Add("six");
            test.Add("seven");
            test.Add("eight");
            test.PrintContents();
            Console.WriteLine();
            Library lib = new Library();
            Console.WriteLine("Книги из библиотеки без реализации IEnumerator через for:");
            for (int i = 0; i < lib.Lenth; i++)
            {
                Console.WriteLine(lib[i]);
            }
            Console.WriteLine();
            Console.WriteLine("Книги из библиотеки с реализованным IEnumerator через foreach:");
            //потокобезопасно и до 15% быстрее
            foreach (Book1 b1 in lib)
            {
                Console.WriteLine(b1);
            }
            Console.WriteLine();

            //кеширование для потокобезопасности
            //отбор первых двух книг в библиотеке. Используется механизм кеширования. 
            //Большая нагрузка на оперативную память и сборщик мусора
            //только для быстрого просмотра коллекций, находящихся в памяти (не подгружаемых)
            //при наличии ОЗУ более 8ГБ
            foreach (Book1 b1 in lib.GetBooks(2))
            {
                Console.WriteLine(b1);
            }
            Console.WriteLine();
            Console.WriteLine("Цифры фибоначчи:");
            FibonacciSequence fibonacci = new FibonacciSequence();
            foreach (int f in fibonacci)
            {
                Console.Write(f + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
    

}

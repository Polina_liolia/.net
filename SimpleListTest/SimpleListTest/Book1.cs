﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleListTest
{
   public class Book1
    {
        public string Name { get; set; }
        public override string ToString()
        {
            return String.Format("Книга - " + Name);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    class Logs : IList, IEnumerable
    {
        List<MyLog> logs;
        public Logs()
        {
            logs = new List<MyLog>();
        }

        public IEnumerator GetEnumerator()
        {
            return logs.GetEnumerator();
        }

        //predicate usage:
        public void printLogs(Predicate<MyLog> predicate)
        {
            foreach (MyLog my_log in logs)
            {
                if (predicate(my_log))
                    Console.WriteLine(my_log);
            }
        }

        #region IList methods for MyLog
        public MyLog this[int index]
        {
            //read only
            get
            {
                return logs[index];
            }
        }

        public int Add(MyLog value)
        {
            logs.Add(value);
            Added(this, new LogsEventArgs("Log item added", value));  //event generation:
            return logs.Count;  //returns index of the added element 
        }

        public int Count
        {
            get
            {
                return logs.Count;
            }
        }

        public bool Contains(MyLog value)
        {
            return logs.Contains(value);
        }

        public bool IsFixedSize
        {
            get
            {
                return false;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        public bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        public int IndexOf(MyLog value)
        {
            return logs.IndexOf(value);
        }


        public void Insert(int index, MyLog value)
        {
            logs.Insert(index, value);
        }


        public object SyncRoot
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        #region IList Methods for System.Object
        object IList.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }       

        public int Add(object value)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }


        public bool Contains(object value)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }
  
        public void Remove(object value)
        {
            throw new NotImplementedException();
        }


        public int IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Data Base queries
        public static string selectAll()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * ");
            sb.Append(" FROM [DBO].[MY_LOGS] ");
            return sb.ToString();
        }

        public static string selectOne(int my_logID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(selectAll());
            sb.Append(string.Format(" WHERE [MY_LOGS_ID] = {0};", my_logID));
            return sb.ToString();
        }

        public static string delete_from_DB(int my_logID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" DELETE FROM[dbo].[MY_LOGS] ");
            sb.Append(string.Format(" WHERE [MY_LOGS_ID] = {0};", my_logID));
            return sb.ToString();
        }

        public static string update_in_DB(int my_logID, MyLog updated_item)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" UPDATE [dbo].[MY_LOGS] ");
            sb.Append(string.Format(" SET LOG_INFO = {0}, ", updated_item.Log_info));
            sb.Append(string.Format(" SET ACTION = {0}, ", updated_item.Action));
            sb.Append(string.Format(" SET TBL_NAME = {0}, ", updated_item.Tbl_name));
            sb.Append(string.Format(" SET ID_ROW = {0}, ", updated_item.ID_row1));
            sb.Append(string.Format(" SET LOG_TIME = {0}, ", updated_item.Log_time));
            sb.Append(string.Format(" SET COLUMN_NAME = {0}, ", updated_item.Column_name));
            sb.Append(string.Format(" SET OLD_VALUE = {0}, ", updated_item.Old_value));
            sb.Append(string.Format(" SET NEW_VALUE = {0}, ", updated_item.New_value));
            sb.Append(string.Format(" WHERE [MY_LOGS_ID] = {0};", my_logID));
            return sb.ToString();
        }
        #endregion

        #region Events
        //delegate to inform user about logs state:
        public delegate void LogsStateHandler(object sender, LogsEventArgs e); 

        //log item added to logs list event
        public event LogsStateHandler Added;
        #endregion
    }
}

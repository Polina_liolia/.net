﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    public class Customer
    {
        protected string nick_name;
        protected string phone;
        protected string email;
        protected int ID;
        protected string name;
        protected string last_name;
        protected int year;

        protected Customer() { }
        
        public Customer (string nick_name, string phone, string email, int ID, string name, string last_name, int year)
        {
            this.nick_name = nick_name;
            this.phone = phone;
            this.email = email;
            this.ID = ID;
            this.name = name;
            this.last_name = last_name;
            this.year = year;
        }
        
        public static string selectOne(int id) {
            StringBuilder sb = new StringBuilder();
            sb.Append(Customer.selectAll());
            sb.Append(String.Format(" WHERE [dbo].[CUSTOMERS].CUSTOMERS_ID = {0}", id));
            return sb.ToString();
        }

        public static string update(int id)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Customer.selectAll());
            sb.Append(String.Format(" WHERE [dbo].[CUSTOMERS].CUSTOMERS_ID = {0}", id));
            return sb.ToString();
        }

        public static string insertItem(Customer customer) {
            StringBuilder sb = new StringBuilder();
            sb.Append("INSERT INTO [dbo].[MEN] ");
            sb.Append("(NAME, LNAME, YEAR)");
            //how to get ID????
            sb.Append("INSERT INTO [dbo].[CUSTOMERS]");
            sb.Append("(EMAIL, PHONE, NICKNAME, ID_MEN)");
            return sb.ToString();
        }

        public static string getUpdateQueryForCustomer(Customer customer)
        {
            StringBuilder sb = new StringBuilder();
     
            return sb.ToString();
        }

        public static string selectAll()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT [dbo].[MEN].NAME as MEN_NAME,");
            sb.Append("[dbo].[MEN].LNAME,");
            sb.Append("[dbo].[MEN].YEAR,");
            sb.Append("[dbo].[CUSTOMERS].NICKNAME,");
            sb.Append("[dbo].[CUSTOMERS].PHONE,");
            sb.Append("[dbo].[CUSTOMERS].EMAIL,");
            sb.Append("[dbo].[CUSTOMERS].ID_MEN ");
            sb.Append("FROM [dbo].[CUSTOMERS] ");
            sb.Append("LEFT JOIN[dbo].[MEN] ");
            sb.Append("ON[dbo].[CUSTOMERS].ID_MEN = [dbo].[MEN].MEN_ID");
            return sb.ToString();
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}", this.nick_name, this.phone, this.email, this.ID, this.name, this.last_name, this.year);
        }

        public string Nick_name
        {
            get {  return this.nick_name;  }
            set { this.nick_name = value;  }
        }

        public string Phone
        {
            get {  return this.phone; }
            set {  this.phone = value; }
        }

        public string Email
        {
            get  {  return this.email;  }
            set  {  this.email = value; }
        }

        public string Name
        {
            get  {  return this.name;  }
            set  {  this.name = value; }
        }

        public string Last_name
        {
            get  {  return this.last_name; }
            set  {  this.last_name = value; }
        }

        public int Year
        {
            get { return this.year; }
            set { this.year = value; }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDataBaseClasses;

namespace ExamTest
{
    class Customers : IEnumerable
    {
        Customer[] customers;
        public Customers()
        {
            customers = new Customer[]
            {
                new Customer("nick1", "0689654741", "mail@gmail.com", 1, "Vasia", "Vasilev", 1985),
                new Customer("nick2", "0685241639", "mail2@gmail.com", 1, "Petia", "Petrov", 1968),
                new Customer("nick3", "0689258411", "mail3@gmail.com", 1, "Sasha", "Aleksandrov", 1972)
            };
        }
        
        int Length
        {
            get { return this.customers.Length; }
        }

        public IEnumerator GetEnumerator()
        {
            return customers.GetEnumerator();
        }

        public void print(Predicate<Customer> pr)
        {
            foreach (Customer c in customers)
                if (pr(c))
                    Console.WriteLine(c);
        }
    }
}

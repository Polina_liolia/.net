﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDataBaseClasses;

namespace ExamTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Customers customers = new Customers();
            foreach (Customer c in customers)
                Console.WriteLine(c);
            customers.print(c => c.Name == "Vasia");

            MyLog l1 = new MyLog(1, "n", "customers", 5, "customer added", DateTime.Now, "all", "-", "-");
            MyLog l2 = new MyLog(2, "u", "customers", 6, "customer updated", DateTime.Now, "Surname", "Petrova", "Ivanova");

            Logs logs = new Logs();
            //bounding event handler:
            logs.Added += Show_message;

            logs.Add(l1);
            logs.Add(l2);

            logs.printLogs(x => x.My_logs_ID == 1);
        }
        private static void Show_message(object sender, LogsEventArgs e)
        {
            MyLog eventSender = sender as MyLog;
            if (eventSender != null)
            {
                Console.WriteLine("{0} Log item detales:\n{1}", e.Message.ToUpper(), e.Item);
            }
        }
    }
}

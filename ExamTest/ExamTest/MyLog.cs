﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    class MyLog : IComparable, IEquatable<MyLog>
    {
        int my_logs_ID;
        string action;
        string tbl_name;
        int ID_row;
        string log_info;
        DateTime log_time;
        string column_name;
        string old_value;
        string new_value;

        #region Class properties
        public int My_logs_ID
        {
            get
            {
                return my_logs_ID;
            }

            set
            {
                my_logs_ID = value;
            }
        }

        public string Action
        {
            get
            {
                return action;
            }

            set
            {
                action = value;
            }
        }

        public string Tbl_name
        {
            get
            {
                return tbl_name;
            }

            set
            {
                tbl_name = value;
            }
        }

        public int ID_row1
        {
            get
            {
                return ID_row;
            }

            set
            {
                ID_row = value;
            }
        }

        public string Log_info
        {
            get
            {
                return log_info;
            }

            set
            {
                log_info = value;
            }
        }

        public DateTime Log_time
        {
            get
            {
                return log_time;
            }

            set
            {
                log_time = value;
            }
        }

        public string Column_name
        {
            get
            {
                return column_name;
            }

            set
            {
                column_name = value;
            }
        }

        public string Old_value
        {
            get
            {
                return old_value;
            }

            set
            {
                old_value = value;
            }
        }

        public string New_value
        {
            get
            {
                return new_value;
            }

            set
            {
                new_value = value;
            }
        }
        #endregion

        public override string ToString()
        {
            return string.Format("logs ID: {0}\naction: {1}\ntable name: {2}\nID row: {3}\nlog info: {4}\nlog time: {5}\ncolumn name: {6}\nold value: {7}\nnew value:{8}",
                this.My_logs_ID, this.Action, this.Tbl_name, this.ID_row1, this.Log_info, this.Log_time, this.Column_name,
                this.Old_value, this.New_value);
        }

        #region Constructors
        private MyLog() { }
        public MyLog (int my_logs_ID, string action, string tbl_name, int ID_row, string log_info,  DateTime log_time,
            string column_name, string old_value, string new_value)
        {
            this.My_logs_ID = my_logs_ID;
            this.Action = action;
            this.Tbl_name = tbl_name;
            this.ID_row1 = ID_row;
            this.Log_info = log_info;
            this.Log_time = log_time;
            this.Column_name = column_name;
            this.Old_value = old_value;
            this.New_value = new_value;
        }
        #endregion
       
        #region ICompare, IEquatable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            MyLog other = obj as MyLog;
            if (other != null) 
                return this.My_logs_ID.CompareTo(other.My_logs_ID); 
            else 
                throw new ArgumentException("object is not MyLog");
        }
        //for IEquatable interface:
        public bool Equals(MyLog other)
        {
            if (other == null)
                return false;
            if (this.My_logs_ID == other.My_logs_ID)
                return true;
            else
                return false;
        }
        //to use with System.Object:
        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            MyLog logObj = other as MyLog;
            if (logObj == null)
                return false;
            else
                return Equals(logObj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region Compare operators
        public static bool operator == (MyLog current, MyLog other)
        {
            if ((object)current == null || (object)other == null)
                return Object.Equals(current, other);
            return current.Equals(other);
        }

        public static bool operator !=(MyLog current, MyLog other)
        {
            if ((object)current == null || (object)other == null)
                return Object.Equals(current, other);
            return !current.Equals(other);
        }

        public static bool operator >(MyLog current, MyLog other)
        {
            return current.My_logs_ID > other.My_logs_ID;
        }

        public static bool operator <(MyLog current, MyLog other)
        {
            return current.My_logs_ID > other.My_logs_ID;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyDataBaseClasses
{
    /// <summary>
    /// to use as an argument in delegate function
    /// </summary>
    class LogsEventArgs
    {
        private string message;
        private MyLog item;

        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }

        internal MyLog Item
        {
            get
            {
                return item;
            }

            set
            {
                item = value;
            }
        }

        private LogsEventArgs() { }
        public LogsEventArgs(string message, MyLog item)
        {
            this.Message = message;
            this.Item = item;
        }
    }
}

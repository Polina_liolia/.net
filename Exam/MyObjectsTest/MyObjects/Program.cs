﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            Product p1 = new Product("mobile_phone", 10240.0);
            Product p2 = new Product("laptop", 22620.0);
            Client c1 = new Client("Anton", 22, "Kh", "0663451234", "mail@gmail.com");

            Account a1 = new Account(c1, 356.9, "2620127654322");
            c1.Accounts.Add(a1);
            Order o1 = new Order("delivery time after 6pm", DateTime.Now, c1);
            Order_position op1 = new Order_position(p1, 2, o1);
            Order_position op2 = new Order_position(p2, 3, o1);
            o1.setOrderPosition(op1);
            o1.setOrderPosition(op2);

            Console.WriteLine(c1);
            Console.WriteLine(a1);
            Console.WriteLine(op1);
            Console.WriteLine(op2);
            Console.WriteLine(o1);
        }
    }  
}

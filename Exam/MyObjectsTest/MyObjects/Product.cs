﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
    public class Product : IEquatable<Product>, IComparable, IComparer<Product>, ICloneable
    {
        private string product_name;
        private double price;

        #region Properties
        public string Product_name
        {
            get
            {
                return product_name;
            }

            set
            {
                product_name = value;
            }
        }

        public double Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }
        #endregion

        #region Constructors
        private Product() { }
        public Product(string product_name, double price)
        {
            this.Product_name = product_name;
            this.Price = price;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("Product name: {1}, price: {2}", product_name, price);
        }
        public override int GetHashCode()
        {
            return product_name.GetHashCode();
        }
        #endregion

        #region Operators overloaded
        public static bool operator ==(Product product1, Product product2)
        {
            if (product1 == null || product2 == null)
                return object.Equals(product1, product2);
            return product1.Equals(product2);
        }

        public static bool operator !=(Product product1, Product product2)
        {
            if (product1 == null || product2 == null)
                return !object.Equals(product1, product2);
            return !product1.Equals(product2);
        }

        public static bool operator <(Product product1, Product product2)
        {
            if (product1 == null && product2 != null)
                return true;
            if (product1 != null && product2 == null)
                return false;
            int cmp_result =  product1.product_name.CompareTo(product2.product_name);
            return cmp_result < 0 ? true : false;
        }

        public static bool operator >(Product product1, Product product2)
        {
            if (product1 == null && product2 != null)
                return false;
            if (product1 != null && product2 == null)
                return true;
            int cmp_result = product1.product_name.CompareTo(product2.product_name);
            return cmp_result > 0 ? true : false;
        }
        #endregion

        #region IEquatable
        public bool Equals(Product other)
        {
            if ((object)other == null)
                return false;
            return (product_name.Equals(other.product_name));
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            Product other_product = other as Product;
            if (other_product != null)
                return (Equals(other_product));
            else
                throw new ArgumentException("Argument is not a Product");
        }
        #endregion

        #region IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Product other = obj as Product;
            if (other == null)
                throw new ArgumentException("Argument is not a Product");
            else
                return product_name.CompareTo(other.product_name);
               
        }
        #endregion

        #region IComparer
        public int Compare(Product x, Product y)
        {
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            return x.CompareTo(y);
        }

        #endregion

        #region ICloneable
        public object Clone()
        {
            Product other = (Product)this.MemberwiseClone(); //clones only ID and price
            other.Product_name = string.Copy(product_name); //deep copy
            return other;
        }
        #endregion
    }
}

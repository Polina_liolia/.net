﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyObjects
{
    public class Order_position : IEquatable<Order_position>, IComparable, IComparer<Order_position>, ICloneable
    {
        Product product;
        double price;
        int item_count;
        Order order;

        #region Properties
        public Product Product
        {
            get
            {
                return product;
            }

            set
            {
                product = value;
            }
        }

        public double Price //auto-calculated, so read only
        {
            get
            {
                return price;
            }
        }

        public int Item_count
        {
            get
            {
                return item_count;
            }

            set
            {
                item_count = value;
            }
        }

        public Order Order
        {
            get
            {
                return order;
            }

            set
            {
                order = value;
            }
        }



        #endregion

        #region Constructors
        private Order_position() { }
        public Order_position(Product product,
                                int item_count,
                                Order order)
        {
            this.Product = product;
            this.Item_count = item_count;
            this.price = product.Price * item_count;
            this.Order = order;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("Product: {0}, item price:{1}, item count: {2}, price: {3}",
               product.Product_name, product.Price, Item_count, Price);
        }
        public override int GetHashCode()
        {
            return price.GetHashCode();
        }
        #endregion

        #region Operators overloaded
        public static bool operator ==(Order_position x, Order_position y)
        {
            if (((object)x) == null || ((object)y) == null)
                return object.Equals(x, y);
            return x.Equals(y);
        }
        public static bool operator !=(Order_position x, Order_position y)
        {
            if (((object)x) == null || ((object)y) == null)
                return !object.Equals(x, y);
            return !x.Equals(y);
        }
        public static bool operator <(Order_position x, Order_position y)
        {
            if (((object)x) == null && ((object)y) != null)
                return true;
            if (((object)x) != null && ((object)y) == null ||
                ((object)x) == null && ((object)y) == null)
                return false;
            return x.price < y.price;
        }
        public static bool operator >(Order_position x, Order_position y)
        {
            if (((object)x) == null && ((object)y) != null ||
                ((object)x) == null && ((object)y) == null)
                return false;
            if (((object)x) != null && ((object)y) == null)
                return true;
            return x.price > y.price;
        }
        #endregion

        #region IEquatable
        public bool Equals(Order_position other)
        {
            if ((object)other == null)
                return false;
            return Equals(price, other.price);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            Order_position other_order_pos = other as Order_position;
            if (other_order_pos != null)
                return (Equals(other_order_pos));
            else
                throw new ArgumentException("Argument is not an Order Position");
        }
        #endregion

        #region IComparable
        public int CompareTo(Order_position other)
        {
            if (((object)other) == null)
                return 1;
            return price.CompareTo(other.price);
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Order_position other = obj as Order_position;
            if (other == null)
                throw new ArgumentException("Argument is not an Order Position");
            else
                return this.CompareTo(other);
        }
        #endregion

        #region IComparer
        public int Compare(Order_position x, Order_position y)
        {
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            return x.CompareTo(y);
        }

        #endregion

        #region ICloneable
        public object Clone()
        {
            Order_position clone = (Order_position)this.MemberwiseClone();
            clone.product = new Product(product.Product_name, product.Price);
            clone.order = new Order(order.Description, order.Order_date, 
                order.Client);
            for (int i = 0; i < order.countOrderPositions; i++)
                clone.order.setOrderPosition(order.getOrderPosition(i));
            return clone;
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerTestConsole
{
    public class LogRowXML
    {
        int number;
        DateTime dt;
        string action;
        string whoCalled;
        string description;
        #region Properties
        public int Number
        {
            get
            {
                return number;
            }

            set
            {
                number = value;
            }
        }

        public DateTime Dt
        {
            get
            {
                return dt;
            }

            set
            {
                dt = value;
            }
        }

        public string Action
        {
            get
            {
                return action;
            }

            set
            {
                action = value;
            }
        }

        public string WhoCalled
        {
            get
            {
                return whoCalled;
            }

            set
            {
                whoCalled = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }
        #endregion
        #region Constructors
        private LogRowXML() { }
        public LogRowXML (int number, DateTime dt, string action, string whoCalled, string description)
        {
            this.number = number;
            this.dt = dt;
            this.action = action;
            this.whoCalled = whoCalled;
            this.description = description;
        }
        #endregion
        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}, {4}", number, dt, action, whoCalled, description);
        }
    }
}

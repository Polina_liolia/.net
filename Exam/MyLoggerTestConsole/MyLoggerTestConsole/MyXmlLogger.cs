﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace MyLoggerTestConsole
{
    public class MyXmlLogger : IMyLogger, IDisposable
    {
        private string path;
        XmlReader reader;
        XmlWriter writer;
        #region Constructors
        private MyXmlLogger() { }
        private MyXmlLogger (string path)
        {
            this.path = path;
        }
        #endregion
        #region Create loggers
        public static MyXmlLogger CreateXmlLogger(string path)
        {
            return new MyXmlLogger(path);
        }
        #endregion
        #region IDisposable
        public void Dispose()//for manual calling
        {
            reader.Dispose();
            writer.Dispose();
        }
        void IDisposable.Dispose() //auto-called
        {
            reader.Dispose();
            writer.Dispose();
        }
        #endregion

        public void WriteProtocol(string action, string whoCalled, string description)
        {
            //creating list to save all log rows, that already exist:
            List<LogRowXML> logRows = new List<LogRowXML>();
            //if file exists, reading xml and saving all log rows to container:
            if (File.Exists(path))
            {
                reader = XmlReader.Create(path);
                while (!reader.EOF && reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "log_row") //if it is a log row element
                    {
                        string num_str = reader.GetAttribute("rownum");
                        int number;
                        Int32.TryParse(num_str, out number);
                        string dt_str = reader.GetAttribute("date_time");
                        DateTime dt;
                        DateTime.TryParse(dt_str, out dt);
                        string act = reader.GetAttribute("action");
                        string code_row = reader.GetAttribute("code_row");
                        string descr = reader.ReadElementContentAsString();
                        logRows.Add(new LogRowXML(number, dt, act, code_row, descr));
                    }
                }
                reader.Close(); 
            }
            //adding a new log row to container:
            logRows.Add(new LogRowXML((logRows.Count +1 ), DateTime.Now, action, whoCalled, description));
            //overriting an xml file:
            writer = XmlWriter.Create(path);
            writer.WriteStartDocument();
            writer.WriteRaw("\n");
            writer.WriteStartElement("log"); //корневой эл-т 
            writer.WriteRaw("\n");
            foreach (LogRowXML lr in logRows)
            {
                writer.WriteStartElement("log_row");
                writer.WriteAttributeString("rownum", lr.Number.ToString());
                writer.WriteAttributeString("date_time", lr.Dt.ToString());
                writer.WriteAttributeString("action", lr.Action);
                writer.WriteAttributeString("code_row", lr.WhoCalled);
                writer.WriteRaw(lr.Description);
                writer.WriteEndElement();
                writer.WriteRaw("\n");
            }                    
            writer.WriteEndDocument();
            writer.Close();   
        }
        
    }
}

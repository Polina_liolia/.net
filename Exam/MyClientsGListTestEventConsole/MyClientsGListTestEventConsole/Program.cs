﻿using MyClientsGListTestConsole;
using MyObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClientsGListTestEventConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ClientsGList clients = new ClientsGList();
            clients.Add(new Client("Nick", 32, "Kh", "0504563421", "email1@gmail.com"));
            clients.Add(new Client("Anna", 24, "Kiev", "0447634512", "email2@ukr.net"));
            clients.Add(new Client("Anatolii", 26, "Kh", "0678345621", "email3@gmail.com"));
            clients.Add(new Client("Igor", 35, "Vinnitsa", "0661276453", "email4@ukr.net"));
            clients.Add(new Client("Kirill", 38, "Zp", "0735643212", "email5@ukr.net"));
            clients.Add(new Client("Anna", 55, "Kh", "0764531276", "email7@ukr.net"));
            clients.Add(new Client("Petr", 21, "Kh", "0631287656", "email6@gmail.com"));
            clients.Add(new Client("Kirill", 21, "Odessa", "0689743423", "email8@ukr.net"));

            Product p1 = new Product("mobile_phone", 10240.0);
            Product p2 = new Product("laptop", 22620.0);
            Order o1 = new Order("delivery time after 6pm", DateTime.Now, clients[1]);
            
            //setting event handlers:
            o1.registerEventListener(o1.Client.getOrderInfo);
            
            Order_position op1 = new Order_position(p1, 2, o1);
            Order_position op2 = new Order_position(p2, 3, o1);
            Order_position op3 = new Order_position(p1, 5, o1);

            //this methods call generates events
            o1.setOrderPosition(op1);
            o1.setOrderPosition(op2);
            o1.removeOrderPosition(op1);

            //removing event handlers:
            o1.removeEventListener(o1.Client.getOrderInfo);

            //now no event handlers will work:
            o1.setOrderPosition(op3);
        }
    }
}

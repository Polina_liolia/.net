﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyObjects;

namespace MyClientsGListTestConsole
{
    public class ClientsGList : IEnumerable<Client>, IEnumerator<Client>,
        IList<Client>, ICollection<Client>, ICloneable
    {
        List<Client> clients;

        #region Constructor
        public ClientsGList()
        {
            clients = new List<Client>();
        }
        #endregion

        #region IEnumerable<Client>
        public IEnumerator<Client> GetEnumerator()
        {
            return clients.GetEnumerator();
        }

        public bool MoveNext()
        {
            return clients.GetEnumerator().MoveNext();
        }

        public void Reset()
        {
            throw new MethodAccessException("This enumerator can not be reset");
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return clients.GetEnumerator();
        }

        public IEnumerable GetClients(int number)
        {
            for (int i = 0; i < number; i++)
            {
                if (i == clients.Count)
                {
                    yield break;
                }
                else
                {
                    yield return clients[i];
                }
            }
        }
        #endregion

        #region IEnumerator<Client>
        public Client Current
        {
            get
            {
                return clients.GetEnumerator().Current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return clients.GetEnumerator().Current;
            }
        }

        public void Dispose() //no connections are going to be opened, no sense to Dispose
        {
            clients.GetEnumerator().Dispose();
        }
        #endregion

        #region IList<Client>, ICollection<Client>
        public int Count
        {
            get
            {
                return clients.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public Client this[int index]
        {
            get
            {
                if (index >= clients.Count)
                    throw new IndexOutOfRangeException();
                return clients[index];
            }

            set
            {
                if (index >= clients.Count)
                    throw new IndexOutOfRangeException();
                clients[index] = value;
            }
        }

        public int IndexOf(Client item)
        {
            return clients.IndexOf(item);
        }

        public void Insert(int index, Client item)
        {
            clients.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            clients.RemoveAt(index);
        }

        public void Add(Client item)
        {
            clients.Add(item);
        }

        public void Clear()
        {
            clients.Clear();
        }

        public bool Contains(Client item)
        {
            return clients.Contains(item);
        }

        public void CopyTo(Client[] array, int arrayIndex)
        {
            clients.CopyTo(array, arrayIndex);
        }

        public bool Remove(Client item)
        {
            return clients.Remove(item);
        }
        #endregion

        #region ICloneable
        public object Clone()
        {
            ClientsGList Clone = (ClientsGList)this.MemberwiseClone();
            Clone.Clear();
            foreach (Client cl in clients)
                Clone.Add(cl);
            return Clone;
        }
        #endregion

        #region Sort, Average, Sum
        public void SortByName()
        {
            clients.Sort((x, y) => x.Clients_name.CompareTo(y.Clients_name));
        }

        public void SortByNameAndAge()
        {
            clients.Sort(
                (x, y) => x.Clients_name.CompareTo(y.Clients_name) != 0 ? 
            x.Clients_name.CompareTo(y.Clients_name) :
            x.Age.CompareTo(y.Age));
        }

        public void SortByPhoneNumbers()
        {
            clients.Sort((x, y) => x.Clients_phone.CompareTo(y.Clients_phone));
        }

        public double GetMidAgeWithSum()
        {
            return ((double)clients.Sum<Client>(x => x.Age)) / clients.Count;
        }

        public double GetMidAgeWithAvg()
        {
            return clients.Average<Client>(x => x.Age);
        }

        public double GetMidAge(string region)
        {
            int count = clients.Count<Client>(x => x.Region_info == region);
            int sumAge = clients.Sum<Client>(x => x.Region_info == region ? x.Age : 0);
            return ((double)sumAge) / count;
        }
        #endregion
    }
}

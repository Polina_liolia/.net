﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyObjects;

namespace MyClientsGListTestConsole
{
    interface ISelectClients
    {
        bool Select(Client c);
    }

    public class IGetGmailDefRelease : ISelectClients
    {
        public bool Select(Client c)
        {
            return c.Clients_mail.EndsWith("gmail.com");
        }
    }

    public class IGetUkrNetDefRelease : ISelectClients
    {
        public bool Select(Client c)
        {
            return c.Clients_mail.EndsWith("ukr.net");
        }
    }
}

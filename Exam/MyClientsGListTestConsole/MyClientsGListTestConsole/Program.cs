﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyObjects;

namespace MyClientsGListTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            ClientsGList clients = new ClientsGList();
            clients.Add(new Client("Nick", 32, "Kh", "0504563421", "email1@gmail.com"));
            clients.Add(new Client("Anna", 24, "Kiev", "0447634512", "email2@ukr.net"));
            clients.Add(new Client("Anatolii", 26, "Kh", "0678345621", "email3@gmail.com"));
            clients.Add(new Client("Igor", 35, "Vinnitsa", "0661276453", "email4@ukr.net"));
            clients.Add(new Client("Kirill", 38, "Zp", "0735643212", "email5@ukr.net"));
            clients.Add(new Client("Anna", 55, "Kh", "0764531276", "email7@ukr.net"));
            clients.Add(new Client("Petr", 21, "Kh", "0631287656", "email6@gmail.com"));
            clients.Add(new Client("Kirill", 21, "Odessa", "0689743423", "email8@ukr.net"));

            Console.WriteLine("First three clients:");
            foreach (Client c in clients.GetClients(3))
                Console.WriteLine(c);
            Console.WriteLine();

            Console.WriteLine("Client, whose name is Anna (predicate test):");
            PrintClients(clients, x => x.Clients_name == "Anna");
            Console.WriteLine();

            Console.WriteLine("Clients, who have ukr.net emails (interface test):");
            PrintClients(clients, new IGetUkrNetDefRelease());
            Console.WriteLine();

            Console.WriteLine("Clients, who have gmail emails (interface test):");
            PrintClients(clients, new IGetGmailDefRelease());
            Console.WriteLine();

            Console.WriteLine("Clients, sorted by name:");
            clients.SortByName();
            foreach (Client c in clients)
                Console.WriteLine(c);
            Console.WriteLine();

            Console.WriteLine("Clients, sorted by name and age:");
            clients.SortByNameAndAge();
            foreach (Client c in clients)
                Console.WriteLine(c);
            Console.WriteLine();

            Console.WriteLine("Clients, sorted by phone numbers:");
            clients.SortByPhoneNumbers();
            foreach (Client c in clients)
                Console.WriteLine(c);
            Console.WriteLine();

            Console.WriteLine("Clients mid age (Sum used): {0}",
                clients.GetMidAgeWithSum());
            Console.WriteLine("Clients mid age (Average used): {0}",
                clients.GetMidAgeWithAvg());

            Console.WriteLine("Clients from Kharkov mid age: {0}",
                clients.GetMidAge("Kh"));
        }

        #region Clients selects
        public static void PrintClients(ClientsGList clients, Predicate<Client> pr)
        {
            foreach (Client c in clients)
                if (pr(c))
                    Console.WriteLine(c);
        }

        public static void PrintClients(ClientsGList clients, ISelectClients filter)
        {
            foreach (Client c in clients)
                if (filter.Select(c))
                    Console.WriteLine(c);
        }

        #endregion

    }

    #region Classes-helpers, inherited from IComparer<Client>
    public class CompareClientsNames : IComparer<Client>
    {
        public int Compare(Client x, Client y)
        {
            return String.Compare(x.Clients_name, y.Clients_name);
        }
    }

    public class CompareClientsNamesAndAges : IComparer<Client>
    {
        public int Compare(Client x, Client y)
        {
            int nameCmpResult = String.Compare(x.Clients_name, y.Clients_name);
            return nameCmpResult == 0 ? x.Age.CompareTo(y.Age) : nameCmpResult;
        }
    }

    public class CompareClientsPhones : IComparer<Client>
    {
        public int Compare(Client x, Client y)
        {
            return String.Compare(x.Clients_phone, y.Clients_phone);
        }
    }
    #endregion
}

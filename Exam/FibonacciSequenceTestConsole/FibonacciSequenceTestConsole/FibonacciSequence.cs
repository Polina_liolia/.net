﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequenceTestConsole
{
    class FibonacciSequence : IEnumerator, IEnumerable
    {
        int[] fibonacci;
        int index = -1;

        #region Constructors
        private FibonacciSequence() { }
        public FibonacciSequence(int n)
        {
            fibonacci = new int[n];
            fibonacci[0] = 1;
            fibonacci[1] = 1;
            for (int i = 2; i < n; i++)
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }
        #endregion

        #region IEnumerator
        public object Current
        {
            get { return fibonacci[index]; }
        }
        public bool MoveNext()
        {
            if (index == fibonacci.Length - 1)
            {
                Reset(); 
                return false;
            }
            index++;
            return true; 
        }
        public void Reset()
        {
            index = -1;
        }
        #endregion

        #region IEnumerable
        public IEnumerator GetEnumerator()
        {
            return this;
        }

        //IEnumerable for the first n collection elements:
        public IEnumerable GetFirst(int n)
        {
            for (int i = 0; i < n; i++)
            {
                if (i == fibonacci.Length)
                {
                    yield break;
                }
                else
                {
                    yield return fibonacci[i];
                }
            }
        }
        #endregion
        }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSequenceTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            FibonacciSequence fib = new FibonacciSequence(7);
            Console.WriteLine("Fibonacci sequence of 7 elements:");
            foreach (int f in fib)
                Console.WriteLine(f);

            Console.WriteLine("First three elements of the Fibonacci sequence:");
            foreach (int f in fib.GetFirst(3))
                Console.WriteLine(f);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TemperatureAnalizatorTestConsole
{
    public class TemperatureAnalizator
    {
        List<double> temperatures;
        double eps;
        #region Constructors
        private TemperatureAnalizator() { }
        public TemperatureAnalizator(double accuracy=0)
        {
            this.Accuracy = accuracy;
            temperatures = new List<double>();
        }
        #endregion
        #region Public properties
        public double Accuracy
        {
            get
            {
                return eps;
            }

            set
            {
                eps = value;
            }
        }

        public List<double> Temperatures
        {
            get
            {
                return temperatures;
            }

            set
            {
                temperatures = value;
            }
        }
        #endregion
        #region FindValue overloads
        public bool FindValue(int val)
        {
            double d = Convert.ToDouble(val);
            foreach (double t in temperatures)
                if (Math.Abs(t - d) <= eps)
                    return true;
            return false;
        }
        public bool FindValue(double val)
        {
            foreach (double t in temperatures)
                if (Math.Abs(t - val) <= eps)
                    return true;
            return false;
        }
        public bool FindValue(string val)
        {
            double d;
            bool parse_result = Double.TryParse(val, out d);
            if (!parse_result)
                throw new ArgumentException("String does not contain double");
            foreach (double t in temperatures)
                if (Math.Abs(t - d) <= eps)
                    return true;
            return false;
        }
        public bool FindValue(double val, double eps)
        {
            foreach (double t in temperatures)
                if (Math.Abs(t - val) <= eps)
                    return true;
            return false;
        }
        #endregion

        #region Find max, min, popular temperature, check stable interval
        public double findMaxTemperature()
        {
            if (temperatures.Count == 0) //container is empty
                throw new FormatException("No temperatures found");
            double max = 0.0;
            foreach (double t in temperatures)
                if (t - max > eps)
                    max = t;
            return max;
        }
        public double findMinTemperature()
        {
            if (temperatures.Count == 0) //container is empty
                throw new FormatException("No temperatures found");
            double min = temperatures[0];
            foreach (double t in temperatures)
                if (min - t > eps)
                    min = t;
            return min;
        }
        public double findPopularTemperature()
        {
            if (temperatures.Count == 0) //container is empty
                throw new FormatException("No temperatures found");
            int maxCount = 0;
            int maxCountIndex = -1;
            for (int i = 0; i < temperatures.Count; i++)
            {
                int count = 0;
                for (int j = i + 1; j < temperatures.Count; j++)
                {
                    if (Math.Abs(temperatures[i] - temperatures[j]) <= eps)
                        count++;
                }
                if (count >= maxCount)
                {
                    maxCount = count;
                    maxCountIndex = i;
                }
            }
            return temperatures[maxCountIndex];
        }
        /*If stable interval was successfully found, returns an index of its fits element.
        Othervise returns -1
        */
        public int findStableIntervalFirstElementIndex()
        {
            int index = -1;
            for (int i = 0; i < temperatures.Count - 3; i++)
                if (Math.Abs(temperatures[i] - temperatures[i + 1]) <= eps &&
                    Math.Abs(temperatures[i] - temperatures[i + 2]) <= eps)
                    return i;
            return index;
        }
        #endregion

        public bool readTemperatureFromFile(string path)
        {
            temperatures = new List<double>();
            string temps;
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                temps = sr.ReadToEnd();
            }
            //getting a string array of temperatures: 
            string[] temperatures_str = temps.Split(new Char[] { ' ', '\t', '\n' });
            //converting string temperatures to double:
            for (int i = 0; i < temperatures_str.Length; i++)
            {
                double result = 0.0;
                Double.TryParse(temperatures_str[i], out result);
                temperatures.Add(result);
            }
            if (Temperatures.Count > 0)//some temperatures were read from file
                return true;
            return false; //no data was read from file
        }

        public void PrintTemperatures()
        {
            if (temperatures.Count == 0)
                Console.WriteLine("No temperatures were found");
            else
            {
                Console.WriteLine("Temperatures list:");
                foreach (double t in temperatures)
                    Console.WriteLine(t);
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureAnalizatorTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            TemperatureAnalizator TA = new TemperatureAnalizator(0.1);
            TA.PrintTemperatures();
            TA.readTemperatureFromFile(@"D:\MyData\temperatures.dat");
            TA.PrintTemperatures();
            
            #region WriteLine overloads tests
            Console.WriteLine(TA.FindValue(11.001)); //true
            Console.WriteLine(TA.FindValue(2587.25)); //false
            Console.WriteLine(TA.FindValue(11.007, 0.01)); //true
            Console.WriteLine(TA.FindValue("11,001")); //true
            try
            {
                Console.WriteLine(TA.FindValue("somecilytext")); //could not be parsed
            }
            catch(ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine(TA.FindValue(15)); //true
            #endregion

            Console.WriteLine(TA.findMaxTemperature());//20.1
            Console.WriteLine(TA.findMinTemperature());//11.001
            Console.WriteLine(TA.findPopularTemperature());//17,369
            Console.WriteLine(TA.findStableIntervalFirstElementIndex());//9
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CesarEncoderDecoderTest
{
    class Program
    {
        static void Main(string[] args)
        {
            CesarEncoder encoder = new CesarEncoder(3);
            encoder.Encode(@"D:\MyData\cesar.txt");
            CesarDecoder decoder = new CesarDecoder(3);
            decoder.Decode(@"D:\MyData\cesar.txt");
        }
    }
}

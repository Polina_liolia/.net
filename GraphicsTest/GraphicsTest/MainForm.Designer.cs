﻿namespace GraphicsTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDrawRec = new System.Windows.Forms.Button();
            this.btnDrawEllipse = new System.Windows.Forms.Button();
            this.btnDrawCircle = new System.Windows.Forms.Button();
            this.btn_testDll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDrawRec
            // 
            this.btnDrawRec.Location = new System.Drawing.Point(25, 517);
            this.btnDrawRec.Name = "btnDrawRec";
            this.btnDrawRec.Size = new System.Drawing.Size(129, 36);
            this.btnDrawRec.TabIndex = 0;
            this.btnDrawRec.Text = "Прямоугольник";
            this.btnDrawRec.UseVisualStyleBackColor = true;
            this.btnDrawRec.Click += new System.EventHandler(this.btnDrawRec_Click);
            // 
            // btnDrawEllipse
            // 
            this.btnDrawEllipse.Location = new System.Drawing.Point(175, 516);
            this.btnDrawEllipse.Name = "btnDrawEllipse";
            this.btnDrawEllipse.Size = new System.Drawing.Size(88, 35);
            this.btnDrawEllipse.TabIndex = 1;
            this.btnDrawEllipse.Text = "Эллипс";
            this.btnDrawEllipse.UseVisualStyleBackColor = true;
            this.btnDrawEllipse.Click += new System.EventHandler(this.btnDrawEllipse_Click);
            // 
            // btnDrawCircle
            // 
            this.btnDrawCircle.Location = new System.Drawing.Point(288, 516);
            this.btnDrawCircle.Name = "btnDrawCircle";
            this.btnDrawCircle.Size = new System.Drawing.Size(106, 36);
            this.btnDrawCircle.TabIndex = 2;
            this.btnDrawCircle.Text = "Круг";
            this.btnDrawCircle.UseVisualStyleBackColor = true;
            this.btnDrawCircle.Click += new System.EventHandler(this.btnDrawCircle_Click);
            // 
            // btn_testDll
            // 
            this.btn_testDll.Location = new System.Drawing.Point(541, 522);
            this.btn_testDll.Name = "btn_testDll";
            this.btn_testDll.Size = new System.Drawing.Size(75, 23);
            this.btn_testDll.TabIndex = 3;
            this.btn_testDll.Text = "Test dll";
            this.btn_testDll.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btn_testDll.UseVisualStyleBackColor = true;
            this.btn_testDll.Click += new System.EventHandler(this.btn_testDll_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 583);
            this.Controls.Add(this.btn_testDll);
            this.Controls.Add(this.btnDrawCircle);
            this.Controls.Add(this.btnDrawEllipse);
            this.Controls.Add(this.btnDrawRec);
            this.Name = "MainForm";
            this.Text = "Тест графических возможностей UI";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDrawRec;
        private System.Windows.Forms.Button btnDrawEllipse;
        private System.Windows.Forms.Button btnDrawCircle;
        private System.Windows.Forms.Button btn_testDll;
    }
}


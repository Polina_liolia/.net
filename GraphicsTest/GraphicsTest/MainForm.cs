﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using itstep.Kharkov.Tester.MyObjects;

namespace GraphicsTest
{
    public partial class MainForm : Form//наследование структуры
        //partial - частично определенный, т.е. реализация класса разделена на разные файлы
    {
        public MainForm()
        {
            InitializeComponent();
            btnDrawRec.Text = "ПРЯМОУГОЛЬНИК";//можно задать текст компонента
            btnDrawEllipse.Text = "ЭЛЛИПС";
            btnDrawCircle.Text = "КРУГ";
        }

        private void btnDrawRec_Click(object sender, EventArgs e)
        {
            //получаем графический контекст = холст, на котором рисовать
            //определиться с контуром - Pen
            //определиться с кистью Brush
            //вызываем из контекста нужную функцию
            Graphics graphics = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Red;
            Brush myBrush = Brushes.BlueViolet;
            int x = 100;//x-координата верхнего левого угла относительно окна формы
            int y = 150;//у-координата верхнего левого угла относительно окна формы
            int width = 50;
            int height = 25;
            graphics.DrawRectangle(myPen, x, y, width, height); 

        }

        private void btnDrawCircle_Click(object sender, EventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Green;
            Brush myBrush = Brushes.Coral;
            int x = 100;//x-координата верхнего левого угла относительно окна формы
            int y = 150;//у-координата верхнего левого угла относительно окна формы
            int width = 50;
            int height = 25;
            graphics.FillEllipse(myBrush, x, y, height, height);
        }

        private void btnDrawEllipse_Click(object sender, EventArgs e)
        {
            Graphics graphics = Graphics.FromHwnd(this.Handle);
            Pen myPen = Pens.Green;
            Brush myBrush = Brushes.Coral;
            int x = 100;//x-координата верхнего левого угла относительно окна формы
            int y = 150;//у-координата верхнего левого угла относительно окна формы
            int width = 50;
            int height = 25;
            graphics.DrawEllipse(myPen, x, y, width, height);
        }

        private void btn_testDll_Click(object sender, EventArgs e)
        {
            MessageBox.Show(MyMessager.formatMSG("Hello world!"));
        }
    }
}

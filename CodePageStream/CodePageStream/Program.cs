﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CodePageStream
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime timeAction = DateTime.Now;
            StreamWriter stream = new StreamWriter("D:\\MyData\\text.txt", true, System.Text.Encoding.UTF8);
                                                     //путь к ф-лу //разрешаем дозапись //кодировка
            //пишется в оперативную память
            stream.Write(timeAction.Day.ToString() + ".");
            stream.Write(timeAction.Month.ToString() + ".");
            stream.Write(timeAction.Year.ToString() + ".");
            stream.Write(timeAction.Hour.ToString() + ":");
            stream.Write(timeAction.Minute.ToString() + ":");
            stream.Write(timeAction.Second.ToString() + " ");
            //stream.Write(timeAction.ToString());
            stream.Close(); //ф-л физически создан на диске

            string readPath = @"D:\MyData\text.txt"; //без экранирования
            string writePath = @"D:\MyData\newtext.txt";
            string text = String.Empty;
            try
            {
                //кодировка - та, которая установлена в текстовом редакторе или ОС
                using (StreamReader sr = new StreamReader(readPath, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                }
                //содержимое ф-ла удалится, запишутся только новые данные:
                using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default)) 
                {
                    sw.WriteLine(text);
                }
                //дозапись в ф-л:
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("Дозапись");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }


                                                             //КОДИРОВКА
        
            text = "Hello world"; //кодировка 1251s VS
            //запись в ф-л:
            using (FileStream fstream = new FileStream(@"D:\MyData\note.dat", FileMode.OpenOrCreate))
            //ф-л для побайтовой записи
            {
                //преобразуем строку в байты
                //ASCIIEncoding encoding = new ASCIIEncoding();
                //UTF8Encoding encoding = new UTF8Encoding();
                //Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                byte[] input = Encoding.Default.GetBytes(text);
                //запись массива байтов в ф-л:
                fstream.Write(input, 0, input.Length);
                Console.WriteLine("Текст записан в ф-л");
                //перемещаем указатель в конец ф-ла, до конца файла 5 байт
                fstream.Seek(-5, SeekOrigin.End); //минус 5 символов с конца потока
                //считываем 4 символа с текущей позиции
                byte[] output = new byte[4];
                fstream.Read(output, 0, output.Length); //fstream.Read(output, 0, 4);
                //декодируем байты в строку с использованием нужной кодировки:
                string textFromFile = Encoding.Default.GetString(output);
                Console.WriteLine("Текст из файла: {0}", textFromFile);//worl
                //заменим в ф-ле слово world на слово house
                string replaceText = "house";
                fstream.Seek(-5, SeekOrigin.End);  //минус 5 символов с конца потока
                input = Encoding.Default.GetBytes(replaceText);//преобразуем содержимое replaceText в байты
                fstream.Write(input, 0, input.Length);//записал в поток fstream( тот, в котором был сдвиг)
                //считываем весь ф-л, возвращаем указатель в начало ф-ла:
                fstream.Seek(0, SeekOrigin.Begin);
                output = new byte[fstream.Length];
                fstream.Read(output, 0, output.Length);
                //декодируем байты в строку:
                textFromFile = Encoding.Default.GetString(output);
                Console.WriteLine("Текст из файла: {0}", textFromFile);
            }

            //Получение информации о файле
            string path = @"D:\MyData\test.txt";
            string newPath = @"D:\MyData\newtest.txt";
            FileInfo fileInf = new FileInfo(path);
            if (fileInf.Exists)
            {
                Console.WriteLine("Имя файла: {0}", fileInf.Name);
                Console.WriteLine("Время создания: {0}", fileInf.CreationTime);
                Console.WriteLine("Размер: {0}", fileInf.Length);
                fileInf.CopyTo(newPath, true);      //копирование
                // альтернатива с помощью класса File
                // File.Copy(path, newPath, true);
                fileInf.MoveTo(newPath);            //перемещение
                // альтернатива с помощью класса File
                // File.Move(path, newPath);
                //Удаление файла
                fileInf.Delete();                   //удаление
                // альтернатива с помощью класса File
                // File.D
            }




        }
        }
}

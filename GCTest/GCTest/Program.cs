﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.Dispose();
            Person[] persons = new Person[3];
            persons[0] = new Person();
            persons[1] = new Person();
            persons[2] = new Person();
            foreach (Person per in persons)
                per.Dispose(); //public void Dispose(), может быть перегружен у наследника

            /*если используются эти команды, "деструктор" должен проверять, не была ли память уже ранее очищена*/
            GC.Collect(2); //2 - второе поколение, большие объекты
            GC.WaitForPendingFinalizers(); //дождаться очищения памяти
            GC.Collect(2); //повторная команда
            GC.GetTotalMemory(true); //вернуть всю память

            using (Person pers = new Person())
            {
                //автоматически вызовется void IDisposable.Dispose()
            }
            //using (List<Person>[] ps = new List<Person>[] { new Person(), new Person() })
            //{
            //    foreach (Person P in ps)
            //        P.Dispose(); 
            //}

            #region Версия с использованием GC.WaitForPendingFinalizers()

            #endregion
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCTest
{
    public class Person : IDisposable
    {
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //здесь выполнение метода Dispose
                }
                //освобождаем управляемые ресурсы
                //вызываем Dispose() других объектов
                //освобождаем неуправляемые объекты-методы из dll-оберток
                Dispose();
                disposed = true;
            }
           
        }


        //ВАРИАНТ ДЛЯ РУЧНОГО ВЫЗОВА:
        public void Dispose()   //будет задействован при явном вызове
        {
            //уведомление GC о том, что выделенную память можно очистить 
            //(но не гарантия того, что она будет в этот момент очищена)
            Console.Beep();
            Print(ConsoleColor.Cyan);
        }
        //ВАРИАНТ ДЛЯ АВТОМАТИЧЕСКОГО ВЫЗОВА:
        void IDisposable.Dispose() //будет задействован при автоматическом вызове (using)
        {
            //закрытие соединений 
            //соединения, созданные при помощи обращения к неуправляемому коду
            Console.Beep();
            Print(ConsoleColor.Red);
        }
        //"ДЕСТРУКТОР":
        ~Person() //финализация переделывается компилятором в спец.метод Finalize 
            //(это нужно для использования с кодом на других языках)
            //"деструктор" вызывается GC в момент очищения памяти
        {
            Print(ConsoleColor.Blue);
        }
        protected void Print(ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine("Dispose");
            Console.ResetColor();
        }

        
    }
}

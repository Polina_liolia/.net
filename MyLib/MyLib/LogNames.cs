﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace MyLib
{
    public class LogNames
    {
        private string[] logNames = new string[] { "mylog.txt", @"c:\mylog.txt", @"d:\mylog.txt", @"e:\mylog.txt", @"f:\mylog.txt" };
        [IndexerName("VALUE")] //Для языков, не поддерживающих [] как индексатор навигации по массивам
        public string this[int index]
        {
            get { return logNames[index]; }
        }

    }
}

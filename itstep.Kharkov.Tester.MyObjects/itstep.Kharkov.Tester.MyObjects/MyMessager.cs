﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//dll должна быть создана в том же Framework, что и проект, к которому она подключается.
//Посмотреть версию Framework: В Solution explorer - контекстное меню проекта - Properties

namespace itstep.Kharkov.Tester.MyObjects
{
    public class MyMessager
    {
        public static string formatMSG(string msg)
        {
            return String.Format("msg:{0}", msg);
        }
    }
}

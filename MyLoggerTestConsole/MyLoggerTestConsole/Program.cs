﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            MyTxtLogger loger = MyTxtLogger.CreateTxtLogger();
            loger.Use_D = true;//вопрос: нужно ли вводить понятие «текущий лог»?
            loger.WriteProtocol("ТЕСТ ЛОГА", "MyLoggerTestConsole: Program.cs : Main", "Запись в лог на диске d");
            loger.WriteProtocol(loger["D"], "ТЕСТ ЛОГА", "MyLoggerTestConsole: Program.cs : Main", "Запись в лог на диске d");
            loger.WriteProtocol(loger[2], "ТЕСТ ЛОГА", "MyLoggerTestConsole: Program.cs : Main", "Запись в лог на диске d");

            IMyLogger logger1 = MyXmlLogger.CreateXmlLogger(@"d:\myData\Logs.xml");
            logger1.WriteProtocol("ТЕСТ ЛОГА", "MyLoggerTestConsole: Program.cs : Main", "Запись в лог на диске d");

           
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace MyLoggerTestConsole
{
    public class MyXmlLogger : IMyLogger
    {
        private string path;
        #region Constructors
        private MyXmlLogger() { }
        private MyXmlLogger (string path)
        {
            this.path = path;
        }
        #endregion
        #region Create loggers
        public static MyXmlLogger CreateXmlLogger(string path)
        {
            return new MyXmlLogger(path);
        }
        #endregion

        public void WriteProtocol(string action, string whoCalled, string description)
        {
            List<LogRowXML> logRows = new List<LogRowXML>();
            if (File.Exists(path))
            {
                using (XmlReader reader = XmlReader.Create(path))
                {
                    while (!reader.EOF && reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "log_row") //if it is a log row element
                        {
                            string num_str = reader.GetAttribute("rownum");
                            int number;
                            Int32.TryParse(num_str, out number);
                            string dt_str = reader.GetAttribute("date_time");
                            DateTime dt;
                            DateTime.TryParse(dt_str, out dt);
                            string act = reader.GetAttribute("action");
                            string code_row = reader.GetAttribute("code_row");
                            string descr = reader.ReadElementContentAsString();
                            logRows.Add(new LogRowXML(number, dt, act, code_row, descr));
                        }
                    }
                }
            }
            logRows.Add(new LogRowXML((logRows.Count +1 ), DateTime.Now, action, whoCalled, description));
            using (XmlWriter writer = XmlWriter.Create(path))
            {
                writer.WriteStartDocument();
                writer.WriteRaw("\n");
                writer.WriteStartElement("log"); //корневой эл-т 
                writer.WriteRaw("\n");
                foreach (LogRowXML lr in logRows)
                {
                    writer.WriteStartElement("log_row");
                    writer.WriteAttributeString("rownum", lr.Number.ToString());
                    writer.WriteAttributeString("date_time", lr.Dt.ToString());
                    writer.WriteAttributeString("action", lr.Action);
                    writer.WriteAttributeString("code_row", lr.WhoCalled);
                    writer.WriteRaw(lr.Description);
                    writer.WriteEndElement();
                    writer.WriteRaw("\n");
                }                    
                writer.WriteEndDocument();
            }
        }
        
    }
}

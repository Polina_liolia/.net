﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLoggerTestConsole
{
    public interface IMyLogger
    {
        void WriteProtocol(string action, string whoCalled, string description);
    }
}

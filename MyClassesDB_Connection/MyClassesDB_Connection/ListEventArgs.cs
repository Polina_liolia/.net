﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesDB_Connection
{
    /// <summary>
    /// to use as an argument in delegate function
    /// </summary>
    public class ListEventArgs<T>
    {
          private string message;
          private T item;

          public string Message
          {
              get
              {
                  return message;
              }

              set
              {
                  message = value;
              }
          }

          internal T Item
          {
              get
              {
                  return item;
              }

              set
              {
                  item = value;
              }
          }

          private ListEventArgs() { }
          public ListEventArgs(string message, T item)
          {
              this.Message = message;
              this.Item = item;
          }
    }
}

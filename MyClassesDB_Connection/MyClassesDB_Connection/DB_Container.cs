﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClassesDB_Connection
{
    public class DB_Container<T> : IEnumerable<T>, IEnumerator<T>, IList<T>, ICollection<T>
    {
        private List<T> list;

        #region Constructor
        public DB_Container()
        {
            list = new List<T>();
        } 
        #endregion

        #region Print method overloads
        public void print()
        {
            foreach(T item in list)
            {
                Console.WriteLine(item);
            }
        }

        public void print(Func<T, bool> f)
        {
            foreach (T item in list)
            {
                if(f(item))
                    Console.WriteLine(item);
            }
        }

        #endregion

        #region IEnumerable
        public IEnumerator<T> GetEnumerator()  
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() 
        {
            return list.GetEnumerator();
        }
        #endregion

        #region IEnumerator
        public T Current  
        {
            get
            {
                return list.GetEnumerator().Current;
            }
        }

        object IEnumerator.Current  
        {
            get
            {
                return list.GetEnumerator().Current;
            }
        }

        public void Dispose()  
        {
           list.GetEnumerator().Dispose();
        }

        public bool MoveNext()  
        {
            return list.GetEnumerator().MoveNext();
        }

        public void Reset()  
        {
            throw new NotImplementedException();
        }

        /*
         public void Reset()  
        {
            Reset(ref list.GetEnumerator());
        }

        static void Reset<T>(ref T enumerator) where T : IEnumerator
        {
            enumerator.Reset();
        }
         */
        #endregion

        #region ICollection
        public int Count  
        {
            get
            {
               return list.Count;
            }
        }

        public bool IsReadOnly  
        {
            get
            {
                return false;
            }
        }

        public void Add(T item)  
        {
            list.Add(item);
            Added(this, new ListEventArgs<T>("List item added", item));  //event generation
        }

        public void Clear()  
        {
            list.Clear();
        }

        public bool Contains(T item)  
        {
            return list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)  
        {
            CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)  
        {
            return list.Remove(item);
        }
        #endregion

        #region IList
        public T this[int index]
        {
            get
            {
                return list[index];
            }

            set
            {
                list[index] = value;
                Added(this, new ListEventArgs<T>("List item added", value));  //event generation
            }
        }

        public int IndexOf(T item)
        {
            return IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            RemoveAt(index);
        }
        #endregion

        #region Events
        //delegate to inform user about logs state:
        public delegate void ListStateHandler(object sender, ListEventArgs<T> e);

        //log item added to logs list event
        public event ListStateHandler Added;
        #endregion
    }
}

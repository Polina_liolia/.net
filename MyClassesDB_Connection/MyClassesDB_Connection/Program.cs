﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;//для работы с ф-лом App.Config
using System.Data.SqlClient; //из драйвера MS SQL Server
using System.Data.SqlTypes;//System.Data.dll
using MyDataBaseClasses;

namespace MyClassesDB_Connection
{
    class Program
    {
        static void Main(string[] args)
        {

            //string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=db28pr10;Integrated Security=True";
            string connectionString = @"Data Source=POLINA11-30\SQLEXPRESS;Initial Catalog=db28pr10;Integrated Security=True";
            DB_Container<Client> clients = new DB_Container<Client>();
            clients.Added += item_added;

            //getting all clients from DB:
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string sqlQuery = Client.getAllClientSSelect();
                using (SqlCommand command = new SqlCommand(sqlQuery, con))//calling of Client's method 
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int ID;
                            Int32.TryParse(reader["CLIENTS_ID"].ToString(), out ID);
                            string name = reader["CLIENTS_NAME"].ToString();
                            string phone = reader["CLIENTS_PHONE"].ToString();
                            string mail = reader["CLIENTS_MAIL"].ToString();
                            clients.Add(new Client(ID, name, phone, mail));
                        }
                    }
                }
                con.Close();
            }
            
            //filling in all unrequired table fields, that were not filled before:
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string sqlUpdateUnrequiredFields = Order.updateUnrequiredFields();
                using (SqlCommand command = new SqlCommand(sqlUpdateUnrequiredFields, con))
                {
                    command.ExecuteNonQuery();
                }
                con.Close();
            }

            DB_Container<Order> orders = new DB_Container<Order>();
            orders.Added += item_added;
            //getting all orders from DB:
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string sqlGetAllOrders = Order.getAllOrdersSelect();
                using (SqlCommand command = new SqlCommand(sqlGetAllOrders, con))//calling of Client's method 
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int ID;
                            Int32.TryParse(reader["ORDERS_ID"].ToString(), out ID);
                            string description = /*"???"; */reader["ORDER_DESCRIPTION"].ToString();
                            DateTime order_date;
                            DateTime.TryParse(reader["ORDERS_DATE"].ToString(), out order_date);
                            double total_costs;
                            Double.TryParse(reader["TOTAL_COSTS"].ToString(), out total_costs);
                            int client_ID;
                            Int32.TryParse(reader["CLIENTS_ID"].ToString(), out client_ID);
                            orders.Add(new Order(ID, description, order_date, total_costs, client_ID));
                        }
                    }
                }
                con.Close();
            }
            

            DB_Container<Product> products = new DB_Container<Product>();
            products.Added += item_added;
            //getting all products from DB:
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string sqlGetAllProducts = Product.getAllProductsSelect();
                using (SqlCommand command = new SqlCommand(sqlGetAllProducts, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int ID;
                            Int32.TryParse(reader["PRODUCTS_ID"].ToString(), out ID);
                            string products_name = reader["PRODUCTS_NAME"].ToString();
                            double price;
                            Double.TryParse(reader["PRICE"].ToString(), out price);
                            products.Add(new Product(ID, products_name, price));
                        }
                    }
                }
                con.Close();
            }
            

            //getting all orders of client with id=2
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string sqlGetAllClientsOrders = Order_position.selectByClientID(2);
                using (SqlCommand command = new SqlCommand(sqlGetAllClientsOrders, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int clientID;
                            Int32.TryParse(reader["CLIENTS_ID"].ToString(), out clientID);
                            string client_name = reader["CLIENTS_NAME"].ToString();
                            string client_phone = reader["CLIENTS_PHONE"].ToString();
                            string client_mail = reader["CLIENTS_MAIL"].ToString();
                            int orderID;
                            Int32.TryParse(reader["ORDERS_ID"].ToString(), out orderID);
                            DateTime date;
                            DateTime.TryParse(reader["ORDERS_DATE"].ToString(), out date);
                            string description = reader["DESCRIPTION"].ToString();
                            int item_count;
                            Int32.TryParse(reader["ITEM_COUNT"].ToString(), out item_count);
                            double price;
                            Double.TryParse(reader["PRICE"].ToString(), out price);

                            Console.WriteLine(string.Format("ClientID: {0}\nClient name: {1}\n Client phone: {2}\nClient mail: {3} ",
                                clientID, client_name, client_phone, client_mail));
                            Console.WriteLine(string.Format("OrderID: {0}\nOrder date: {1}\n Description: {2}\nItem count: {3}\nPrice: {4} ",
                                orderID, date, description, item_count, price));
                        }
                    }
                }
                con.Close();
            }

            //getting all orders with id=3
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                string sqlGetOrdersByID = Order_position.selectByOrderID(3);
                using (SqlCommand command = new SqlCommand(sqlGetOrdersByID, con))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            int clientID;
                            Int32.TryParse(reader["CLIENTS_ID"].ToString(), out clientID);
                            string client_name = reader["CLIENTS_NAME"].ToString();
                            string client_phone = reader["CLIENTS_PHONE"].ToString();
                            string client_mail = reader["CLIENTS_MAIL"].ToString();
                            int orderID;
                            Int32.TryParse(reader["ORDERS_ID"].ToString(), out orderID);
                            DateTime date;
                            DateTime.TryParse(reader["ORDERS_DATE"].ToString(), out date);
                            string description = reader["DESCRIPTION"].ToString();
                            int item_count;
                            Int32.TryParse(reader["ITEM_COUNT"].ToString(), out item_count);
                            double price;
                            Double.TryParse(reader["PRICE"].ToString(), out price);
                            
                            Console.WriteLine(string.Format("OrderID: {0}\nOrder date: {1}\n Description: {2}\nItem count: {3}\nPrice: {4} ",
                                orderID, date, description, item_count, price));
                            Console.WriteLine(string.Format("ClientID: {0}\nClient name: {1}\n Client phone: {2}\nClient mail: {3} ",
                                clientID, client_name, client_phone, client_mail));
                        }
                    }
                }
                con.Close();
            }


            //Client A = new Client(1, "ff", "566", "korkgo@kof");
            //Client B = new Client(2, "ff", "566", "korkgo@kof");
            //Console.WriteLine(object.Equals(A, B));

            //Product C = new Product(6, "p1", 55.3);
            //Console.WriteLine(C);
            //Console.WriteLine(C.GetHashCode());

            //Product D = (Product)C.Clone();
            //Console.WriteLine(D);
            //Console.WriteLine(D.GetHashCode());

            //Product E = new Product(8, "p2", 5114.3);
            //Console.WriteLine(E);
            //Console.WriteLine(E.GetHashCode());


        }
        public static void item_added<T>(object sender, ListEventArgs<T> e)
        {
            Console.WriteLine("{0} Item added detales:\n{1}", e.Message.ToUpper(), e.Item);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleShufflingAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            //input:
            List<MyIDObject> myIDObjectsList = new List<MyIDObject>()
            {
                new MyIDObject(1, 1),
                new MyIDObject(2, 2),
                new MyIDObject(3, 3),
                new MyIDObject(4, 4),
                new MyIDObject(5, 5)
            };

            //output:
            List<int> shuffledList = listShuffler(myIDObjectsList);
            foreach(int ID in shuffledList)
                Console.WriteLine(ID);

        }
        public static List<int> listShuffler(List<MyIDObject> myList)
        {
            List<int> resultList = new List<int>();
            //creating initial list (not shuffled yet):
            foreach(MyIDObject obj in myList)
            {
                for(int i = 0; i < obj.Count; i++)
                {
                    resultList.Add(obj.ID);
                }
            }
            Random rand = new Random();
            //shuffling initial array:
            for (int i = 0; i < resultList.Count; i++)
            {
                //getting index of a random element from resultList:
                int new_index = rand.Next(resultList.Count);
                //swapping current and random elements of list:
                int tmp = resultList[i];
                resultList[i] = resultList[new_index];
                resultList[new_index] = tmp;
            }
            return resultList;
        }
        
    }
    public class MyIDObject
    {
        public int ID { get; set; }
        public int Count { get; set; }
        public MyIDObject(int id, int count)
        {
            ID = id;
            Count = count;
        }
    }
}

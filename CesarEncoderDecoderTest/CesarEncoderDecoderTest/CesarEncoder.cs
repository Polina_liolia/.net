﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CesarEncoderDecoderTest
{
    public class CesarEncoder
    {
        private int step;
        private CesarEncoder() { }
        public CesarEncoder(int step)
        {
            this.step = step;
        }

        public void Encode(string path)
        {
            using (FileStream fstream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite))
            {
                byte[] output = new byte[fstream.Length];
                fstream.Read(output, 0, output.Length);
                //шифрование:
                for (int i = 0; i < output.Length; i++)
                    output[i] += (byte)step;
                //декодируем байты в строку:
                string encodedText = Encoding.Default.GetString(output);
                Console.WriteLine(encodedText);
                //перемещаемся в начало файла
                fstream.Seek(0, SeekOrigin.Begin);
                //замещаем существующий текст шифром:
                fstream.Write(output, 0, output.Length);
            }
        }
    }
}

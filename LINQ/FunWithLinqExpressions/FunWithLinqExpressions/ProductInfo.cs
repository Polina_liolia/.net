﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithLinqExpressions
{
    public class ProductInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int NumberInStock { get; set; }
        public ProductInfo(string name, string description, int numberInStock)
        {
            Name = name;
            Description = description;
            NumberInStock = numberInStock;
        }
        public override string ToString()
        {
            return string.Format("Name: {0}, Description: {1}, Number in stock: {2}",
                Name, Description, NumberInStock);
        }
    }
}

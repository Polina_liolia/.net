﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithLinqExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductInfo[] productsInStock = new ProductInfo[]
            {
                new ProductInfo("Milk", "cow's best", 120),
                new ProductInfo("Bread", "soft as cotton", 70),
                new ProductInfo("Tea", "black", 320),
                new ProductInfo("Soap", "smells like wathermelon", 30),
                new ProductInfo("Butter", "80%", 245),
                new ProductInfo("Sosage", "real meat only", 80),
                new ProductInfo("Turkey", "fat and tasty", 20)
            };
            Array anonimusTypes = getProjectedAnonimusTypesArray(productsInStock, 50);
            Console.WriteLine("Anonimus types converted to array:");
            foreach (var p in anonimusTypes)
                Console.WriteLine(p);

            int number = countProductsMoreThenNumberInStock(productsInStock, 50);
            Console.WriteLine("Count products more then 50 in stock: {0}", number);

            displayIntersection();

        }

        private static Array getProjectedAnonimusTypesArray(ProductInfo[] productsInStock, int number)
        {
            //получить объекты, содержащие информацию только о названии и описании продуктов,
            //которых в продаже больше 50:
            var productsOverStock = from p in productsInStock
                                    where p.NumberInStock > number
                                    select new { p.Name, p.Description }; //projection - anonimus type dynamic generation 
            Console.WriteLine("Anonimus types generated (reversed):");
            foreach (var p in productsOverStock.Reverse())
                Console.WriteLine(string.Format("{0}: {1}", p.Name, p.Description));
            return productsOverStock.ToArray(); //because var type can't be returned
        }

        private static int countProductsMoreThenNumberInStock(ProductInfo[] productsInStock, int number)
        {
            return (from p in productsInStock
                          where p.NumberInStock > number
                          select p).Count();
        }

        private static void displayIntersection()
        {
            string[] arr1 = new string[] { "BMW", "Audi", "Lexus" };
            string[] arr2 = new string[] { "Dewoo", "Shkoda", "BMW", "VW", "Lexus" };
            //common elements:
            var intersection = (from car in arr1 select car).Intersect(from car in arr2 select car);
            Console.WriteLine("Intersection result:");
            foreach (var car in intersection)
                Console.WriteLine(car);
        }
    }
}

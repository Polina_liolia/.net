﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToObjectTest
{
    class Program
    {
        static void QueryOverString()
        {
            //page 432
            string[] currentGames = { "Morrowind", "Unncharted 2", "Fallout 3",
                 "Daxter", "System Shock 2" };
            //найти элементы, включающие пробелы:
            IEnumerable<string> subset = from g in currentGames
                                         where g.Contains(" ")
                                         orderby g select g;
            foreach(string s in subset)
                Console.WriteLine(s);

            //неявная типизапция:
            var subset1 = from game in currentGames
                          where game.Contains("a")
                          orderby game
                          select game;
            foreach (var s in subset1)
                Console.WriteLine(s);
        }
        static void Main(string[] args)
        {
            QueryOverString();
        }
    }
}

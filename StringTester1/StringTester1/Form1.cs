﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringTester1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void CompareBtn_MouseClick(object sender, MouseEventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            int result = string.Compare(s1, s2);
            MessageBox.Show(result.ToString());
            result = string.Compare(s1, s2, true);
            MessageBox.Show(result.ToString());
            int res = string.Compare(s1, s2);
            switch (res)
            {
                case -1:
                    MessageBox.Show("s1 < s2");
                    break;
                case 1:
                    MessageBox.Show("s1 > s2");
                    break;
                default:
                    MessageBox.Show("s1 == s2");
                    break;
            }
        }

        private void EqualsBtn_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            if (string.Equals(s1, s2))
            {
                MessageBox.Show("Строки равны");
            }
            else
            {
                MessageBox.Show("Строки различны");
            }
        }

        private void SplitBtn_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            string[] words = str.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                MessageBox.Show(words[i]);
            }
        }

        private void IsNullOrEmptyBtn_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            //s1 = string.Empty; //обнуление строки (или инициализация пустотой)
            /*Плохой вариант:
            if (s1 == "")//сравниваются ссылки, а не значения строк
                         //использование кавычек - всегда выделение памяти
            */
            //if (s1.Length == 0) //стиль .Net 1.1
            //if(String.IsNullOrEmpty(s1))//проверка без учета пробелов (пробел - не пустота)
            if (string.IsNullOrWhiteSpace(s1))//проверка c учетом пробелов (пробел, null, Empty - тоже пустота)
            {
                MessageBox.Show("В S1 - пустая строка");
            }
            else
            {
                MessageBox.Show("В S1 - не пустая строка");
            }
        }

        private void btnCmpOrdinal_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            int result = string.Compare(s1, s2);
            MessageBox.Show(result.ToString());
            result = string.Compare(s1, s2, true);
            MessageBox.Show(result.ToString());
            int res = string.Compare(s1, s2);
            switch (res)
            {
                case -1:
                    MessageBox.Show("s1 < s2");
                    break;
                case 1:
                    MessageBox.Show("s1 > s2");
                    break;
                default:
                    MessageBox.Show("s1 == s2");
                    break;
            }
        }

        private void btnContains_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            if (s1.Contains(s2))
                MessageBox.Show(string.Format("Строка '{0}' содержит строку '{1}'", s1, s2));
            else
                MessageBox.Show(string.Format("Строка '{0}' НЕ содержит строку '{1}'", s1, s2));
        }

        private void btnClone_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            string clone = (string)str.Clone(); //удвоение ссылки
            textBox2.Text = clone;
            MessageBox.Show(str + clone);
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            string str = textBox1.Text;
            string copy = (string)str.Clone(); //аналог копирующего конструктора (создается новая ссылка и копируются данные)
            textBox2.Text = copy;
            MessageBox.Show(str + copy);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            string res = s1.Insert(5, s2); //нумерация от нуля, вставка второй строки на 5 позицию первой строки; не меняет ни s1, ни s2 - создается новая строка
            MessageBox.Show(res);
            //String.Builder - изменяемая строка
            StringBuilder sb = new StringBuilder(s1);
            sb.Insert(5, s2);
            MessageBox.Show(sb.ToString());


        }

        private void btnIndexOf_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            int result = s1.IndexOf(s2);
            if (result >= 0)
                MessageBox.Show(string.Format("Строка '{0}' содержит строку '{1}, индекс символа вхождения: {2}'", s1, s2, result));
            else
                MessageBox.Show(string.Format("Строка '{0}' НЕ содержит строку '{1}'", s1, s2));
        }

        private void btnEndsWith_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            if (s1.EndsWith(s2))
                MessageBox.Show(string.Format("Строка '{0}' заканчивается строкой '{1}'", s1, s2));
            else
                MessageBox.Show(string.Format("Строка '{0}' НЕ заканчивается строкой '{1}'", s1, s2));
        }

        private void btnConcat_Click(object sender, EventArgs e)
        {
            string s1 = textBox1.Text;
            string s2 = textBox2.Text;
            string concat = string.Concat(s1, s2);
            MessageBox.Show(concat);
        }
    }
}

﻿namespace StringTester1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnClone = new System.Windows.Forms.Button();
            this.btnContains = new System.Windows.Forms.Button();
            this.btnCmpOrdinal = new System.Windows.Forms.Button();
            this.IsNullOrEmptyBtn = new System.Windows.Forms.Button();
            this.SplitBtn = new System.Windows.Forms.Button();
            this.EqualsBtn = new System.Windows.Forms.Button();
            this.CompareBtn = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Lbl2 = new System.Windows.Forms.Label();
            this.Lbl1 = new System.Windows.Forms.Label();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnIndexOf = new System.Windows.Forms.Button();
            this.btnEndsWith = new System.Windows.Forms.Button();
            this.btnConcat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCopy
            // 
            this.btnCopy.Location = new System.Drawing.Point(358, 190);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(75, 23);
            this.btnCopy.TabIndex = 23;
            this.btnCopy.Text = "Copy";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnClone
            // 
            this.btnClone.Location = new System.Drawing.Point(277, 190);
            this.btnClone.Name = "btnClone";
            this.btnClone.Size = new System.Drawing.Size(75, 23);
            this.btnClone.TabIndex = 22;
            this.btnClone.Text = "Clone";
            this.btnClone.UseVisualStyleBackColor = true;
            this.btnClone.Click += new System.EventHandler(this.btnClone_Click);
            // 
            // btnContains
            // 
            this.btnContains.Location = new System.Drawing.Point(195, 190);
            this.btnContains.Name = "btnContains";
            this.btnContains.Size = new System.Drawing.Size(75, 23);
            this.btnContains.TabIndex = 21;
            this.btnContains.Text = "Contains";
            this.btnContains.UseVisualStyleBackColor = true;
            this.btnContains.Click += new System.EventHandler(this.btnContains_Click);
            // 
            // btnCmpOrdinal
            // 
            this.btnCmpOrdinal.Location = new System.Drawing.Point(113, 190);
            this.btnCmpOrdinal.Name = "btnCmpOrdinal";
            this.btnCmpOrdinal.Size = new System.Drawing.Size(75, 23);
            this.btnCmpOrdinal.TabIndex = 20;
            this.btnCmpOrdinal.Text = "CompareOrdinal";
            this.btnCmpOrdinal.UseVisualStyleBackColor = true;
            this.btnCmpOrdinal.Click += new System.EventHandler(this.btnCmpOrdinal_Click);
            // 
            // IsNullOrEmptyBtn
            // 
            this.IsNullOrEmptyBtn.Location = new System.Drawing.Point(356, 148);
            this.IsNullOrEmptyBtn.Name = "IsNullOrEmptyBtn";
            this.IsNullOrEmptyBtn.Size = new System.Drawing.Size(95, 23);
            this.IsNullOrEmptyBtn.TabIndex = 19;
            this.IsNullOrEmptyBtn.Text = "IsNullOrEmpty";
            this.IsNullOrEmptyBtn.UseVisualStyleBackColor = true;
            this.IsNullOrEmptyBtn.Click += new System.EventHandler(this.IsNullOrEmptyBtn_Click);
            // 
            // SplitBtn
            // 
            this.SplitBtn.Location = new System.Drawing.Point(275, 148);
            this.SplitBtn.Name = "SplitBtn";
            this.SplitBtn.Size = new System.Drawing.Size(75, 23);
            this.SplitBtn.TabIndex = 18;
            this.SplitBtn.Text = "Split";
            this.SplitBtn.UseVisualStyleBackColor = true;
            this.SplitBtn.Click += new System.EventHandler(this.SplitBtn_Click);
            // 
            // EqualsBtn
            // 
            this.EqualsBtn.Location = new System.Drawing.Point(194, 148);
            this.EqualsBtn.Name = "EqualsBtn";
            this.EqualsBtn.Size = new System.Drawing.Size(75, 23);
            this.EqualsBtn.TabIndex = 17;
            this.EqualsBtn.Text = "Equals";
            this.EqualsBtn.UseVisualStyleBackColor = true;
            this.EqualsBtn.Click += new System.EventHandler(this.EqualsBtn_Click);
            // 
            // CompareBtn
            // 
            this.CompareBtn.Location = new System.Drawing.Point(113, 148);
            this.CompareBtn.Name = "CompareBtn";
            this.CompareBtn.Size = new System.Drawing.Size(75, 23);
            this.CompareBtn.TabIndex = 16;
            this.CompareBtn.Text = "Compare";
            this.CompareBtn.UseVisualStyleBackColor = true;
            this.CompareBtn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CompareBtn_MouseClick);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(113, 107);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(363, 20);
            this.textBox2.TabIndex = 15;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(113, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(363, 20);
            this.textBox1.TabIndex = 14;
            // 
            // Lbl2
            // 
            this.Lbl2.AutoSize = true;
            this.Lbl2.Location = new System.Drawing.Point(70, 114);
            this.Lbl2.Name = "Lbl2";
            this.Lbl2.Size = new System.Drawing.Size(26, 13);
            this.Lbl2.TabIndex = 13;
            this.Lbl2.Text = "S2=";
            // 
            // Lbl1
            // 
            this.Lbl1.AutoSize = true;
            this.Lbl1.Location = new System.Drawing.Point(70, 73);
            this.Lbl1.Name = "Lbl1";
            this.Lbl1.Size = new System.Drawing.Size(26, 13);
            this.Lbl1.TabIndex = 12;
            this.Lbl1.Text = "S1=";
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(113, 231);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(75, 22);
            this.btnInsert.TabIndex = 24;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnIndexOf
            // 
            this.btnIndexOf.Location = new System.Drawing.Point(195, 230);
            this.btnIndexOf.Name = "btnIndexOf";
            this.btnIndexOf.Size = new System.Drawing.Size(75, 23);
            this.btnIndexOf.TabIndex = 25;
            this.btnIndexOf.Text = "IndexOf";
            this.btnIndexOf.UseVisualStyleBackColor = true;
            this.btnIndexOf.Click += new System.EventHandler(this.btnIndexOf_Click);
            // 
            // btnEndsWith
            // 
            this.btnEndsWith.Location = new System.Drawing.Point(275, 230);
            this.btnEndsWith.Name = "btnEndsWith";
            this.btnEndsWith.Size = new System.Drawing.Size(75, 23);
            this.btnEndsWith.TabIndex = 26;
            this.btnEndsWith.Text = "EndsWith";
            this.btnEndsWith.UseVisualStyleBackColor = true;
            this.btnEndsWith.Click += new System.EventHandler(this.btnEndsWith_Click);
            // 
            // btnConcat
            // 
            this.btnConcat.Location = new System.Drawing.Point(358, 231);
            this.btnConcat.Name = "btnConcat";
            this.btnConcat.Size = new System.Drawing.Size(75, 23);
            this.btnConcat.TabIndex = 27;
            this.btnConcat.Text = "Concat";
            this.btnConcat.UseVisualStyleBackColor = true;
            this.btnConcat.Click += new System.EventHandler(this.btnConcat_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 513);
            this.Controls.Add(this.btnConcat);
            this.Controls.Add(this.btnEndsWith);
            this.Controls.Add(this.btnIndexOf);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnClone);
            this.Controls.Add(this.btnContains);
            this.Controls.Add(this.btnCmpOrdinal);
            this.Controls.Add(this.IsNullOrEmptyBtn);
            this.Controls.Add(this.SplitBtn);
            this.Controls.Add(this.EqualsBtn);
            this.Controls.Add(this.CompareBtn);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Lbl2);
            this.Controls.Add(this.Lbl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnClone;
        private System.Windows.Forms.Button btnContains;
        private System.Windows.Forms.Button btnCmpOrdinal;
        private System.Windows.Forms.Button IsNullOrEmptyBtn;
        private System.Windows.Forms.Button SplitBtn;
        private System.Windows.Forms.Button EqualsBtn;
        private System.Windows.Forms.Button CompareBtn;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label Lbl2;
        private System.Windows.Forms.Label Lbl1;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnIndexOf;
        private System.Windows.Forms.Button btnEndsWith;
        private System.Windows.Forms.Button btnConcat;
    }
}


﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;//для работы с ф-лом App.Config
using System.Data.SqlClient; //из драйвера MS SQL Server
using System.Data.SqlTypes;//System.Data.dll




namespace MSServerTestConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=db28pr10;Integrated Security=True";
            List<Customer> all_customers = new List<Customer>();

            CustomersArrayList customers_list = new CustomersArrayList();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                
                con.Open();
                using (SqlCommand command = new SqlCommand(Customer.selectAll(), con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string nick_name = reader["NICKNAME"].ToString();
                        string phone = reader["PHONE"].ToString();
                        string email = reader["EMAIL"].ToString();
                        int ID_Men;
                        Int32.TryParse(reader["ID_MEN"].ToString(), out ID_Men);
                        string name = reader["MEN_NAME"].ToString();
                        string lname = reader["LNAME"].ToString();
                        int year;
                        Int32.TryParse(reader["YEAR"].ToString(), out year);
                        Customer New = new Customer(nick_name, phone, email, ID_Men, name, lname, year);
                        all_customers.Add(New);
                        customers_list.Add(New);
                    }
                    foreach (Customer i in all_customers)
                    {
                        Console.WriteLine(i);
                    }
                    Console.WriteLine("Customers array list:");
                    for (int i = 0; i < customers_list.Count; i++)
                        Console.WriteLine(customers_list[i]);

                }
            }
        }
    }
}

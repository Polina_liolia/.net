﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSServerTestConnection
{
    class CustomersArrayList
    {
        private ArrayList ar;
        public CustomersArrayList()
        {
            this.ar = new ArrayList();
        }
        public void Add(Customer v)
        {
            ar.Add(v);
        }
        public Customer this[int index]
        {
            get
            {
                return (Customer)ar[index];
            }
            set
            {
                ar[index] = value;
            }
        }
        public int Count
        {
            get
            {
                return ar.Count;
            }
        }
    }
}

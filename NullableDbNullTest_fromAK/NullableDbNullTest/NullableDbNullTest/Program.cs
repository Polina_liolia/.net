﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;// из драйвера MS SQL Server-а
using System.Data.SqlTypes;//System.Data.dll

namespace NullableDbNullTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int? x = 10;
            int? y = 10;
            if (x.HasValue)// if (y != null)
            {
                System.Console.WriteLine(x.Value);
            }
            else
            {
                System.Console.WriteLine("Undefined");
            }
            #region иная запись условия
            if (y != null)
            {
                System.Console.WriteLine(y.Value);
            }
            else
            {
                System.Console.WriteLine("Undefined");
            }
#endregion
            #region Преобразования типов
            /*
            int? n = null;
            //int m1 = n;// Даже не откомпилируется
            //неявное приведение типов не работает
            int m2 = (int)n;//Явное приведение типа работает
            //Получим исключение System.InvalidOperationException
            //во время выполнения программы 
            int m3 = n.Value;  // 
            */
            #endregion
            #region сравнение типов?
            int? num1 = 10;
            int? num2 = null;
            if (num1 >= num2)//обратное условие num1 < num2
            {
                Console.WriteLine("num1 is greater than"+ 
                "or equal to num2");
            }
            else
            {
                // выполнится эта ветка, но значение в num1 
                //не меньше значения num2
                Console.WriteLine("num1 >= num2 returned"+
                    "false (but num1 < num2 also is false)");
            }
            if (num1 < num2)
            {
                Console.WriteLine("num1 is less than num2");
            }
            else//в обратном условие тоже ветка else
                //null - несравнимое значение
            {
                Console.WriteLine("num1 < num2 returned false (but num1 >= num2 also is false)");
            }
            if (num1 != num2)
            {
                // This comparison is true, num1 and num2 are not equal.
                Console.WriteLine("Finally, num1 != num2 returns true!");
            }
            num1 = null;
            if (num1 == num2)
            {
                // два значения null неразличимы - равны друг другу
                Console.WriteLine("num1 == num2 returns true when the value of each is null");
            }
            #endregion
            #region упаковка
            int? c = null;
            // d = c, если значение c  null, 
            //то в переменную значимого типа d = -1.
            int d = c ?? -1;
            //"упаковка" в ссылочный тип
            //упаковка - процесс преобразования 
            //в тип, совместимый с object
            bool? b = null;
            object o = b; // Now o is null.
            b = false;
            int? i = 44;
            object bBoxed = b; // bBoxed contains a boxed bool.
            object iBoxed = i; // iBoxed contains a boxed int.
            //
            int Xx = 10;//int - это 4 байта
            object xBoxed = Xx;//object - 10 байт
            #endregion
            string connectionString = @"Data Source=DESKTOP-9B09E8L\SQLEXPRESS;Initial Catalog=db28pr5;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM [db28pr5].[dbo].[CUSTOMERS]", con))
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string nick = reader["NICK_NAME"].ToString();
                        //если пришло NULL, в .Net это тип System.DBNull
                        //и вызвался его ToString()
                        string phone = reader["PHONE"].ToString();
                        string email = reader["EMAIL"].ToString();
                        object id_men2 = (object)reader["ID_MEN"];//если столбец содержит NULL
                        //в .Net это тип System.DBNull явное приведение не System.Object НЕ возможно
                        string id_men = MyExtensions.ConvertFromDBVal<string>(id_men2);
                        int? idMen = (id_men).GetValueOrNull<int>();
                        //int? idMen = (MyExtensions.ConvertFromDBVal<string>(id_men2)).GetValueOrNull<int>();
                    }
                }
            }
        }//main
    }
    public static class MyExtensions
    {
        /// <summary>
        /// преобразовывает System.Object в любой тип T
        /// </summary>
        public static T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            //null - это пустая ссылка, а 
            //DBNull.Value - это NULL из базы данных
            {
                return default(T);//преобразовать в
                //значение по умолчанию для типа T
            }
            else
            {
                return (T)obj;//если не null, то явное приведение
                //аналог (int)obj Или (double)obj
            }
        }
        /// <summary>
        /// Из символьной записи значения любого значимого типа. Формирую тип?
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ValueAsString"></param>
        /// <returns></returns>
        public static T? GetValueOrNull<T>(this string ValueAsString)
            where T : struct
        {
            if (string.IsNullOrEmpty(ValueAsString))
                return null;
            return (T)Convert.ChangeType(ValueAsString, typeof(T));
        }
    }
    #region DBnull
    // For example purposes only. Use the built-in nullable bool 
    // type (bool?) whenever possible.
    public struct DBBool
    {
        // The three possible DBBool values.
        public static readonly DBBool Null = new DBBool(0);
        public static readonly DBBool False = new DBBool(-1);
        public static readonly DBBool True = new DBBool(1);
        // Private field that stores –1, 0, 1 for False, Null, True.
        sbyte value;
        // Private instance constructor. The value parameter must be –1, 0, or 1.
        DBBool(int value)
        {
            this.value = (sbyte)value;
        }
        // Properties to examine the value of a DBBool. Return true if this
        // DBBool has the given value, false otherwise.
        public bool IsNull { get { return value == 0; } }
        public bool IsFalse { get { return value < 0; } }
        public bool IsTrue { get { return value > 0; } }
        // Implicit conversion from bool to DBBool. Maps true to DBBool.True and
        // false to DBBool.False.
        public static implicit operator DBBool(bool x)
        {
            return x ? True : False;
        }
        // Explicit conversion from DBBool to bool. Throws an exception if the
        // given DBBool is Null; otherwise returns true or false.
        public static explicit operator bool(DBBool x)
        {
            if (x.value == 0) throw new InvalidOperationException();
            return x.value > 0;
        }
        // Equality operator. Returns Null if either operand is Null; otherwise
        // returns True or False.
        public static DBBool operator ==(DBBool x, DBBool y)
        {
            if (x.value == 0 || y.value == 0) return Null;
            return x.value == y.value ? True : False;
        }
        // Inequality operator. Returns Null if either operand is Null; otherwise
        // returns True or False.
        public static DBBool operator !=(DBBool x, DBBool y)
        {
            if (x.value == 0 || y.value == 0) return Null;
            return x.value != y.value ? True : False;
        }
        // Logical negation operator. Returns True if the operand is False, Null
        // if the operand is Null, or False if the operand is True.
        public static DBBool operator !(DBBool x)
        {
            return new DBBool(-x.value);
        }
        // Logical AND operator. Returns False if either operand is False,
        // Null if either operand is Null, otherwise True.
        public static DBBool operator &(DBBool x, DBBool y)
        {
            return new DBBool(x.value < y.value ? x.value : y.value);
        }
        // Logical OR operator. Returns True if either operand is True, 
        // Null if either operand is Null, otherwise False.
        public static DBBool operator |(DBBool x, DBBool y)
        {
            return new DBBool(x.value > y.value ? x.value : y.value);
        }
        // Definitely true operator. Returns true if the operand is True, false
        // otherwise.
        public static bool operator true(DBBool x)
        {
            return x.value > 0;
        }
        // Definitely false operator. Returns true if the operand is False, false
        // otherwise.
        public static bool operator false(DBBool x)
        {
            return x.value < 0;
        }
        public override bool Equals(object obj)
        {
            if (!(obj is DBBool)) return false;
            return value == ((DBBool)obj).value;
        }
        public override int GetHashCode()
        {
            return value;
        }
        public override string ToString()
        {
            if (value > 0) return "DBBool.True";
            if (value < 0) return "DBBool.False";
            return "DBBool.Null";
        }
    }

    #endregion
}

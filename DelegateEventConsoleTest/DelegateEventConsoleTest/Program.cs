﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateEventConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account = new Account(52.2, 10);
            account.Added += Show_message; //вешаем свой обработчик на событие
            account.Withdrowed += Show_message; //вешаем свой обработчик на событие
            account.Put(10.2);
            account.Withdraw(12);
            account.Withdraw(1112);
            account.Added -= Show_message; //снимаем обработчик с события
            account.Withdrowed -= Show_message; //снимаем обработчик с события

            AccountEx accountEx = new AccountEx(125.2, 15);
            accountEx.Added += Show_message;
            accountEx.Withdrowed += Show_message;
            accountEx.Withdraw(20);
            accountEx.Put(50.2);
            accountEx.Withdraw(1000);


        }

        private static void Show_message(object sender, AccountEventArgs e)
        {
            //colored message:
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            AccountEx eventSender = sender as AccountEx;
            if (eventSender != null)
            {
                Console.WriteLine(string.Format("{0} Сумма операции: {1} Сумма счета: {2}", e.Message, e.Sum, eventSender.Sum));
            }
            else
            {
                Console.WriteLine(e.Message);
            }
            //reset colored console text:
            Console.ResetColor();

        }

        private static void Show_message(string message)//обработчик событий Added и Withdrowed
        {
            Console.WriteLine(message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateEventConsoleTest
{
    /*
    [ACCOUNTS_ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [nvarchar](50) NULL,
	[BANKS_ID] [int] NOT NULL,
	[ACCOUNTS_SUM] [float] NOT NULL,
	[ACCOUNT] [int] NOT NULL
         */

      
    class Account
    {
        double _sum; //переменная для хранения суммы
        int _percentage; //переменная для хранения процента

        public int Percentage
        {
            get
            {
                return _percentage;
            }

        }

        public double Sum
        {
            get
            {
                return _sum;
            }
        }

        private Account() { }
        public Account(double sum, int percentage)
        {
            _sum = sum;
            _percentage = percentage;
        }

        //для уведомления пользователя о событии, касающемся состояния его счета:
        public delegate void AccountStateHandler(string message); //указатель на ф-цию

        //событие, возникающее при снятии денег со счета:
        public event AccountStateHandler Withdrowed;

        //событие, возникающее при зачислении денег на счет:
        public event AccountStateHandler Added;

        public void Put (double sum)
        {
            _sum += sum;
            if (Added != null)
                Added(string.Format("На счет поступило {0}, баланс: {1}", sum, _sum)); //генерация события
        }

        public void Withdraw (double sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                if (Withdrowed != null)
                    Withdrowed(string.Format("Со счета списано {0}, баланс: {1}", sum, _sum)); //генерация события
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed("На счету недостаточно средств для списания"); //генерация события
            }
        }

       
    }

    //расширенная версия Счета
    /// <summary>
    /// для параметра-аргумента функции-делегата (по аналогии с сархитектурой WinForms .Net)
    /// </summary>
   class AccountEventArgs
    {
        private string message;
        private double sum;
        private AccountEventArgs() { }
        public AccountEventArgs (string message, double sum)
        {
            this.Message = message;
            this.Sum = sum;
        }

        public string Message
        {
            get  { return this.message; }
            set  { message = value; }
        }

        public double Sum
        {
            get { return this.sum; }
            set { sum = value; }
        }
    }

    class AccountEx
    {
        double _sum; //переменная для хранения суммы
        int _percentage; //переменная для хранения процента

        public int Percentage
        {
            get
            {
                return _percentage;
            }

        }

        public double Sum
        {
            get
            {
                return _sum;
            }
        }

        private AccountEx() { }
        public AccountEx(double sum, int percentage)
        {
            _sum = sum;
            _percentage = percentage;
        }

        //для уведомления пользователя о событии, касающемся состояния его счета:
        public delegate void AccountStateHandler(object sender, AccountEventArgs e); //указатель на ф-цию

        //событие, возникающее при снятии денег со счета:
        public event AccountStateHandler Withdrowed;

        //событие, возникающее при зачислении денег на счет:
        public event AccountStateHandler Added;

        public void Put(double sum)
        {
            _sum += sum;
            if (Added != null)
                Added(this, new AccountEventArgs("Поступление на счет", sum)); //генерация события
        }

        public void Withdraw(double sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                if (Withdrowed != null)
                    Withdrowed(this, new AccountEventArgs("Списание со счета", sum)); //генерация события
            }
            else
            {
                if (Withdrowed != null)
                    Withdrowed(this, new AccountEventArgs("На счету недостаточно средств для списания", sum)); //генерация события
            }
        }
    }
}

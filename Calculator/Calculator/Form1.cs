﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMult_decimal_Click(object sender, EventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnMult_decimal.BackColor = (btnMult_decimal.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnSum_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDif_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnMod_decimal.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnSum_decimal_Click(object sender, EventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnSum_decimal.BackColor = (btnSum_decimal.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDif_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnMod_decimal.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnDif_decimal_Click(object sender, EventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnDif_decimal.BackColor = (btnDif_decimal.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnSum_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnMod_decimal.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnDiv_decimal_Click(object sender, EventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnDiv_decimal.BackColor = (btnDiv_decimal.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDif_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnSum_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnMod_decimal.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnMod_decimal_Click(object sender, EventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnMod_decimal.BackColor = (btnMod_decimal.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnSum_decimal.BackColor = System.Drawing.SystemColors.Control;
            btnDif_decimal.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnResult_decimal_Click(object sender, EventArgs e)
        {
            //getting strings with numbers to operate:
            string a_text = txtA_decimal.Text;
            string b_text = txtB_decimal.Text;
            int a, b;   //to save decimals, inputed by user
            try
            {
                a = Convert.ToInt32(a_text);
            }
            catch (FormatException ex)//exception expected
            {
                MessageBox.Show(String.Format("Введенное значение {0} невозможно преобразовать в целое число.Подробности: {1}", a_text, ex.Message), "Ошибка");
                return;
            }
            catch (Exception ex)//unexpected exception
            {
                MessageBox.Show(String.Format("Введено неверное значение: {0}. Подробности: {1}.", a_text, ex.Message), "Ошибка");
                return;
            }
            try
            {
                b = Convert.ToInt32(b_text);
            }
            catch (FormatException ex)//exception expected
            {
                MessageBox.Show(String.Format("Введенное значение {0} невозможно преобразовать в целое число.Подробности: {1}", b_text, ex.Message), "Ошибка");
                return;
            }
            catch (Exception ex)//unexpected exception
            {
                MessageBox.Show(String.Format("Введено неверное значение: {0}. Подробности: {1}.", b_text, ex.Message), "Ошибка");
                return;
            }
            txtA_decimal.Clear();
            txtA_decimal.AppendText("0");
            txtB_decimal.Clear();
            txtB_decimal.AppendText("0");

            //checking an operation type, selected by user:
            if (btnDif_decimal.BackColor == System.Drawing.SystemColors.ActiveCaption)  //difference operation selected
            {
                int result_dif = a - b;
                lblResult_decimal.Text = Convert.ToString(result_dif);
                btnDif_decimal.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnSum_decimal.BackColor == System.Drawing.SystemColors.ActiveCaption)//sum operation selected
            {
                int result_sum = a + b;
                lblResult_decimal.Text = Convert.ToString(result_sum);
                btnSum_decimal.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnMult_decimal.BackColor == System.Drawing.SystemColors.ActiveCaption)//multiplication operation selected
            {
                int result_mult = a * b;
                lblResult_decimal.Text = Convert.ToString(result_mult);
                btnMult_decimal.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnDiv_decimal.BackColor == System.Drawing.SystemColors.ActiveCaption)//divide operation selected
            {
                if (b == 0)
                {
                    MessageBox.Show("Деление на ноль невозможно!", "Ошибка");
                    return;
                }
                double result_div = (double)a / b;
                lblResult_decimal.Text = string.Format("{0:#.###}", result_div);
                btnDiv_decimal.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnMod_decimal.BackColor == System.Drawing.SystemColors.ActiveCaption)//divide operation selected)
            {
                int result_mod;
                try
                {
                    result_mod = a % b;
                }
                catch (DivideByZeroException ex)//exception expected
                {
                    MessageBox.Show(String.Format("Невозможно найти остаток от деления на ноль.Подробности: {0}", ex.Message), "Ошибка");
                    return;
                }
                catch (Exception ex)//unexpected exception
                {
                    MessageBox.Show(String.Format("Подробности: {0}.", ex.Message), "Ошибка");
                    return;
                }

                lblResult_decimal.Text = Convert.ToString(result_mod);
                btnMod_decimal.BackColor = System.Drawing.SystemColors.Control;
            }
            else//no operation selected
            {
                MessageBox.Show("No operations selected!", "Error");
            }
        }

       
        private void txtA_double_KeyPress(object sender, KeyPressEventArgs e)
        {
            char charInputed = e.KeyChar;
            string text;
            if (charInputed != '\b')//if not backspace was pressed
            {
                if (charInputed != '\r') //if not Enter was pressed
                    text = string.Concat(txtA_double.Text, charInputed);//updating a text field value
                else
                    text = txtA_double.Text;
            }
            else
                text = (txtA_double.Text.Length > 0) ?
                    txtA_double.Text.Substring(0, txtA_double.Text.Length - 1) : txtA_double.Text;//getting text from A input except one last char
            Regex regex = new Regex("^[+-]?\\d+(?:,\\d+)?$");
            bool isDouble = regex.IsMatch(text);
            //int input_result = nonNumberInputed(sender, e);
            txtA_double.ForeColor = isDouble ? Color.Blue : Color.Red;//indicating valid or invalid value with text color
        }

        private void txtB_double_KeyPress(object sender, KeyPressEventArgs e)
        {
            char charInputed = e.KeyChar;
            string text;
            if (charInputed != '\b')//if not backspace was pressed
            {
                if (charInputed != '\r') //if not Enter was pressed
                    text = string.Concat(txtB_double.Text, charInputed);//updating a text field value
                else
                    text = txtB_double.Text;
            }
            else //getting text from A input except one last char:
                text = (txtB_double.Text.Length > 0) ?
                    txtB_double.Text.Substring(0, txtB_double.Text.Length - 1) : txtB_double.Text;
            //if div or mod operation selected, checking if text in txtB_double is not a zero
            Regex regex = (btnDiv_double.BackColor == System.Drawing.SystemColors.ActiveCaption ||
                btnMod_double.BackColor == System.Drawing.SystemColors.ActiveCaption) ? 
                new Regex("^[+-]?(?:(?:[1-9]+(?:,\\d*)?)|(?:\\d+,\\d+))$") : new Regex("^[+-]?\\d+(?:,\\d+)?$");
            bool isDouble = regex.IsMatch(text);
            txtB_double.ForeColor = isDouble ? Color.Blue : Color.Red;//indicating valid or invalid value with text color
        }

        private void btnMult_double_MouseClick(object sender, MouseEventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnMult_double.BackColor = (btnMult_double.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnSum_double.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_double.BackColor = System.Drawing.SystemColors.Control;
            btnDif_double.BackColor = System.Drawing.SystemColors.Control;
            btnMod_double.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnSum_double_MouseClick(object sender, MouseEventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnSum_double.BackColor = (btnSum_double.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_double.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_double.BackColor = System.Drawing.SystemColors.Control;
            btnDif_double.BackColor = System.Drawing.SystemColors.Control;
            btnMod_double.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnDif_double_MouseClick(object sender, MouseEventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnDif_double.BackColor = (btnDif_double.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_double.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_double.BackColor = System.Drawing.SystemColors.Control;
            btnSum_double.BackColor = System.Drawing.SystemColors.Control;
            btnMod_double.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnDiv_double_MouseClick(object sender, MouseEventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnDiv_double.BackColor = (btnDiv_double.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_double.BackColor = System.Drawing.SystemColors.Control;
            btnDif_double.BackColor = System.Drawing.SystemColors.Control;
            btnSum_double.BackColor = System.Drawing.SystemColors.Control;
            btnMod_double.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnMod_double_MouseClick(object sender, MouseEventArgs e)
        {
            //toggle between checked and unchecked status: 
            btnMod_double.BackColor = (btnMod_double.BackColor == System.Drawing.SystemColors.Control) ?
                System.Drawing.SystemColors.ActiveCaption : System.Drawing.SystemColors.Control;
            //setting other buttons as unchecked:
            btnMult_double.BackColor = System.Drawing.SystemColors.Control;
            btnDiv_double.BackColor = System.Drawing.SystemColors.Control;
            btnSum_double.BackColor = System.Drawing.SystemColors.Control;
            btnDif_double.BackColor = System.Drawing.SystemColors.Control;
        }

        private void btnResult_double_MouseClick(object sender, MouseEventArgs e)
        {
            //getting strings with numbers to operate:
            string a_text = txtA_double.Text;
            string b_text = txtB_double.Text;

            Regex regexA = new Regex("^[+-]?\\d+(?:,\\d+)?$");

            //using another regex for B value if dividing operetion is going to be used:
            Regex regexB = (btnDiv_double.BackColor == System.Drawing.SystemColors.ActiveCaption ||
                btnMod_double.BackColor == System.Drawing.SystemColors.ActiveCaption) ?
               new Regex("^[+-]?(?:(?:[1-9]+(?:,\\d*)?)|(?:\\d+,\\d+))$") : new Regex("^[+-]?\\d+(?:,\\d+)?$");
            bool isADouble = regexA.IsMatch(a_text);
            bool isBDouble = regexB.IsMatch(b_text);
            //indicating valid or invalid value with text color
            txtA_double.ForeColor = isADouble ? Color.Blue : Color.Red;
            txtB_double.ForeColor = isBDouble ? Color.Blue : Color.Red;
            if (!isADouble || !isBDouble )
            {
                MessageBox.Show("Wrong input!", "Error");
                return;
            }
            //converting strings, inputed by user, to doubles:
            double a = Convert.ToDouble(a_text);
            double b = Convert.ToDouble(b_text); 
            
            txtA_double.Clear();
            txtA_double.AppendText("0,0");
            txtB_double.Clear();
            txtB_double.AppendText("0,0");

            //checking an operation type, selected by user:
            if (btnDif_double.BackColor == System.Drawing.SystemColors.ActiveCaption)  //difference operation selected
            {
                double result_dif = a - b;
                lblResult_double.Text = string.Format("{0:#.###}", result_dif);
                btnDif_double.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnSum_double.BackColor == System.Drawing.SystemColors.ActiveCaption)//sum operation selected
            {
                double result_sum = a + b;
                lblResult_double.Text = string.Format("{0:#.###}", result_sum);
                btnSum_double.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnMult_double.BackColor == System.Drawing.SystemColors.ActiveCaption)//multiplication operation selected
            {
                double result_mult = a * b;
                lblResult_double.Text = string.Format("{0:#.###}", result_mult);
                btnMult_double.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnDiv_double.BackColor == System.Drawing.SystemColors.ActiveCaption)//divide operation selected
            {
                double result_div = a / b;
                lblResult_double.Text = string.Format("{0:#.###}", result_div);
                btnDiv_double.BackColor = System.Drawing.SystemColors.Control;
            }
            else if (btnMod_double.BackColor == System.Drawing.SystemColors.ActiveCaption)//divide operation selected)
            {
                double result_mod = a % b;
                lblResult_double.Text = string.Format("{0:#.###}", result_mod);
                btnMod_double.BackColor = System.Drawing.SystemColors.Control;
            }
            else//no operation selected
            {
                MessageBox.Show("No operations selected!", "Error");
            }
        }
    }
}

﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDecimal = new System.Windows.Forms.GroupBox();
            this.lblResult_decimal = new System.Windows.Forms.Label();
            this.btnResult_decimal = new System.Windows.Forms.Button();
            this.txtB_decimal = new System.Windows.Forms.TextBox();
            this.lblB_decimal = new System.Windows.Forms.Label();
            this.btnMod_decimal = new System.Windows.Forms.Button();
            this.btnDiv_decimal = new System.Windows.Forms.Button();
            this.btnDif_decimal = new System.Windows.Forms.Button();
            this.btnSum_decimal = new System.Windows.Forms.Button();
            this.btnMult_decimal = new System.Windows.Forms.Button();
            this.txtA_decimal = new System.Windows.Forms.TextBox();
            this.lblA_decimal = new System.Windows.Forms.Label();
            this.gbDouble = new System.Windows.Forms.GroupBox();
            this.lblResult_double = new System.Windows.Forms.Label();
            this.btnSum_double = new System.Windows.Forms.Button();
            this.btnResult_double = new System.Windows.Forms.Button();
            this.lblA_double = new System.Windows.Forms.Label();
            this.txtB_double = new System.Windows.Forms.TextBox();
            this.txtA_double = new System.Windows.Forms.TextBox();
            this.lblB_double = new System.Windows.Forms.Label();
            this.btnMult_double = new System.Windows.Forms.Button();
            this.btnMod_double = new System.Windows.Forms.Button();
            this.btnDif_double = new System.Windows.Forms.Button();
            this.btnDiv_double = new System.Windows.Forms.Button();
            this.gbDecimal.SuspendLayout();
            this.gbDouble.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDecimal
            // 
            this.gbDecimal.Controls.Add(this.lblResult_decimal);
            this.gbDecimal.Controls.Add(this.btnResult_decimal);
            this.gbDecimal.Controls.Add(this.txtB_decimal);
            this.gbDecimal.Controls.Add(this.lblB_decimal);
            this.gbDecimal.Controls.Add(this.btnMod_decimal);
            this.gbDecimal.Controls.Add(this.btnDiv_decimal);
            this.gbDecimal.Controls.Add(this.btnDif_decimal);
            this.gbDecimal.Controls.Add(this.btnSum_decimal);
            this.gbDecimal.Controls.Add(this.btnMult_decimal);
            this.gbDecimal.Controls.Add(this.txtA_decimal);
            this.gbDecimal.Controls.Add(this.lblA_decimal);
            this.gbDecimal.Location = new System.Drawing.Point(21, 45);
            this.gbDecimal.Name = "gbDecimal";
            this.gbDecimal.Size = new System.Drawing.Size(428, 229);
            this.gbDecimal.TabIndex = 0;
            this.gbDecimal.TabStop = false;
            this.gbDecimal.Text = "Действия над ЦЕЛЫМИ числами";
            // 
            // lblResult_decimal
            // 
            this.lblResult_decimal.AutoSize = true;
            this.lblResult_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.lblResult_decimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblResult_decimal.ForeColor = System.Drawing.Color.Red;
            this.lblResult_decimal.Location = new System.Drawing.Point(370, 93);
            this.lblResult_decimal.Name = "lblResult_decimal";
            this.lblResult_decimal.Size = new System.Drawing.Size(23, 25);
            this.lblResult_decimal.TabIndex = 10;
            this.lblResult_decimal.Text = "0";
            // 
            // btnResult_decimal
            // 
            this.btnResult_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.btnResult_decimal.Location = new System.Drawing.Point(289, 95);
            this.btnResult_decimal.Name = "btnResult_decimal";
            this.btnResult_decimal.Size = new System.Drawing.Size(75, 23);
            this.btnResult_decimal.TabIndex = 9;
            this.btnResult_decimal.Text = "=";
            this.btnResult_decimal.UseVisualStyleBackColor = false;
            this.btnResult_decimal.Click += new System.EventHandler(this.btnResult_decimal_Click);
            // 
            // txtB_decimal
            // 
            this.txtB_decimal.Location = new System.Drawing.Point(207, 96);
            this.txtB_decimal.Name = "txtB_decimal";
            this.txtB_decimal.Size = new System.Drawing.Size(76, 20);
            this.txtB_decimal.TabIndex = 8;
            this.txtB_decimal.Text = "0";
            // 
            // lblB_decimal
            // 
            this.lblB_decimal.AutoSize = true;
            this.lblB_decimal.Location = new System.Drawing.Point(207, 79);
            this.lblB_decimal.Name = "lblB_decimal";
            this.lblB_decimal.Size = new System.Drawing.Size(25, 13);
            this.lblB_decimal.TabIndex = 7;
            this.lblB_decimal.Text = "b = ";
            this.lblB_decimal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnMod_decimal
            // 
            this.btnMod_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.btnMod_decimal.Location = new System.Drawing.Point(117, 153);
            this.btnMod_decimal.Name = "btnMod_decimal";
            this.btnMod_decimal.Size = new System.Drawing.Size(75, 23);
            this.btnMod_decimal.TabIndex = 6;
            this.btnMod_decimal.Text = "mod";
            this.btnMod_decimal.UseVisualStyleBackColor = false;
            this.btnMod_decimal.Click += new System.EventHandler(this.btnMod_decimal_Click);
            // 
            // btnDiv_decimal
            // 
            this.btnDiv_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.btnDiv_decimal.Location = new System.Drawing.Point(117, 123);
            this.btnDiv_decimal.Name = "btnDiv_decimal";
            this.btnDiv_decimal.Size = new System.Drawing.Size(75, 23);
            this.btnDiv_decimal.TabIndex = 5;
            this.btnDiv_decimal.Text = "/";
            this.btnDiv_decimal.UseVisualStyleBackColor = false;
            this.btnDiv_decimal.Click += new System.EventHandler(this.btnDiv_decimal_Click);
            // 
            // btnDif_decimal
            // 
            this.btnDif_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.btnDif_decimal.Location = new System.Drawing.Point(117, 93);
            this.btnDif_decimal.Name = "btnDif_decimal";
            this.btnDif_decimal.Size = new System.Drawing.Size(75, 23);
            this.btnDif_decimal.TabIndex = 4;
            this.btnDif_decimal.Text = "-";
            this.btnDif_decimal.UseVisualStyleBackColor = false;
            this.btnDif_decimal.Click += new System.EventHandler(this.btnDif_decimal_Click);
            // 
            // btnSum_decimal
            // 
            this.btnSum_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.btnSum_decimal.Location = new System.Drawing.Point(117, 64);
            this.btnSum_decimal.Name = "btnSum_decimal";
            this.btnSum_decimal.Size = new System.Drawing.Size(75, 23);
            this.btnSum_decimal.TabIndex = 3;
            this.btnSum_decimal.Text = "+";
            this.btnSum_decimal.UseVisualStyleBackColor = false;
            this.btnSum_decimal.Click += new System.EventHandler(this.btnSum_decimal_Click);
            // 
            // btnMult_decimal
            // 
            this.btnMult_decimal.BackColor = System.Drawing.SystemColors.Control;
            this.btnMult_decimal.Location = new System.Drawing.Point(117, 35);
            this.btnMult_decimal.Name = "btnMult_decimal";
            this.btnMult_decimal.Size = new System.Drawing.Size(75, 23);
            this.btnMult_decimal.TabIndex = 2;
            this.btnMult_decimal.Text = "*";
            this.btnMult_decimal.UseVisualStyleBackColor = false;
            this.btnMult_decimal.Click += new System.EventHandler(this.btnMult_decimal_Click);
            // 
            // txtA_decimal
            // 
            this.txtA_decimal.Location = new System.Drawing.Point(22, 96);
            this.txtA_decimal.Name = "txtA_decimal";
            this.txtA_decimal.Size = new System.Drawing.Size(77, 20);
            this.txtA_decimal.TabIndex = 1;
            this.txtA_decimal.Text = "0";
            // 
            // lblA_decimal
            // 
            this.lblA_decimal.AutoSize = true;
            this.lblA_decimal.Location = new System.Drawing.Point(19, 79);
            this.lblA_decimal.Name = "lblA_decimal";
            this.lblA_decimal.Size = new System.Drawing.Size(25, 13);
            this.lblA_decimal.TabIndex = 0;
            this.lblA_decimal.Text = "a = ";
            this.lblA_decimal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // gbDouble
            // 
            this.gbDouble.Controls.Add(this.lblResult_double);
            this.gbDouble.Controls.Add(this.btnSum_double);
            this.gbDouble.Controls.Add(this.btnResult_double);
            this.gbDouble.Controls.Add(this.lblA_double);
            this.gbDouble.Controls.Add(this.txtB_double);
            this.gbDouble.Controls.Add(this.txtA_double);
            this.gbDouble.Controls.Add(this.lblB_double);
            this.gbDouble.Controls.Add(this.btnMult_double);
            this.gbDouble.Controls.Add(this.btnMod_double);
            this.gbDouble.Controls.Add(this.btnDif_double);
            this.gbDouble.Controls.Add(this.btnDiv_double);
            this.gbDouble.Location = new System.Drawing.Point(469, 45);
            this.gbDouble.Name = "gbDouble";
            this.gbDouble.Size = new System.Drawing.Size(421, 229);
            this.gbDouble.TabIndex = 1;
            this.gbDouble.TabStop = false;
            this.gbDouble.Text = "Действия над ДРОБНЫМИ числами";
            // 
            // lblResult_double
            // 
            this.lblResult_double.AutoSize = true;
            this.lblResult_double.BackColor = System.Drawing.SystemColors.Control;
            this.lblResult_double.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblResult_double.ForeColor = System.Drawing.Color.Red;
            this.lblResult_double.Location = new System.Drawing.Point(357, 93);
            this.lblResult_double.Name = "lblResult_double";
            this.lblResult_double.Size = new System.Drawing.Size(39, 25);
            this.lblResult_double.TabIndex = 21;
            this.lblResult_double.Text = "0,0";
            // 
            // btnSum_double
            // 
            this.btnSum_double.BackColor = System.Drawing.SystemColors.Control;
            this.btnSum_double.Location = new System.Drawing.Point(104, 64);
            this.btnSum_double.Name = "btnSum_double";
            this.btnSum_double.Size = new System.Drawing.Size(75, 23);
            this.btnSum_double.TabIndex = 14;
            this.btnSum_double.Text = "+";
            this.btnSum_double.UseVisualStyleBackColor = false;
            this.btnSum_double.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnSum_double_MouseClick);
            // 
            // btnResult_double
            // 
            this.btnResult_double.BackColor = System.Drawing.SystemColors.Control;
            this.btnResult_double.Location = new System.Drawing.Point(276, 95);
            this.btnResult_double.Name = "btnResult_double";
            this.btnResult_double.Size = new System.Drawing.Size(75, 23);
            this.btnResult_double.TabIndex = 20;
            this.btnResult_double.Text = "=";
            this.btnResult_double.UseVisualStyleBackColor = false;
            this.btnResult_double.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnResult_double_MouseClick);
            // 
            // lblA_double
            // 
            this.lblA_double.AutoSize = true;
            this.lblA_double.Location = new System.Drawing.Point(3, 78);
            this.lblA_double.Name = "lblA_double";
            this.lblA_double.Size = new System.Drawing.Size(25, 13);
            this.lblA_double.TabIndex = 11;
            this.lblA_double.Text = "a = ";
            this.lblA_double.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtB_double
            // 
            this.txtB_double.Location = new System.Drawing.Point(194, 96);
            this.txtB_double.Name = "txtB_double";
            this.txtB_double.Size = new System.Drawing.Size(76, 20);
            this.txtB_double.TabIndex = 19;
            this.txtB_double.Text = "0,0";
            this.txtB_double.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtB_double_KeyPress);
            // 
            // txtA_double
            // 
            this.txtA_double.Location = new System.Drawing.Point(6, 95);
            this.txtA_double.Name = "txtA_double";
            this.txtA_double.Size = new System.Drawing.Size(77, 20);
            this.txtA_double.TabIndex = 12;
            this.txtA_double.Text = "0,0";
            this.txtA_double.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA_double_KeyPress);
            // 
            // lblB_double
            // 
            this.lblB_double.AutoSize = true;
            this.lblB_double.Location = new System.Drawing.Point(194, 79);
            this.lblB_double.Name = "lblB_double";
            this.lblB_double.Size = new System.Drawing.Size(25, 13);
            this.lblB_double.TabIndex = 18;
            this.lblB_double.Text = "b = ";
            this.lblB_double.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnMult_double
            // 
            this.btnMult_double.BackColor = System.Drawing.SystemColors.Control;
            this.btnMult_double.Location = new System.Drawing.Point(104, 35);
            this.btnMult_double.Name = "btnMult_double";
            this.btnMult_double.Size = new System.Drawing.Size(75, 23);
            this.btnMult_double.TabIndex = 13;
            this.btnMult_double.Text = "*";
            this.btnMult_double.UseVisualStyleBackColor = false;
            this.btnMult_double.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnMult_double_MouseClick);
            // 
            // btnMod_double
            // 
            this.btnMod_double.BackColor = System.Drawing.SystemColors.Control;
            this.btnMod_double.Location = new System.Drawing.Point(104, 153);
            this.btnMod_double.Name = "btnMod_double";
            this.btnMod_double.Size = new System.Drawing.Size(75, 23);
            this.btnMod_double.TabIndex = 17;
            this.btnMod_double.Text = "mod";
            this.btnMod_double.UseVisualStyleBackColor = false;
            this.btnMod_double.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnMod_double_MouseClick);
            // 
            // btnDif_double
            // 
            this.btnDif_double.BackColor = System.Drawing.SystemColors.Control;
            this.btnDif_double.Location = new System.Drawing.Point(104, 93);
            this.btnDif_double.Name = "btnDif_double";
            this.btnDif_double.Size = new System.Drawing.Size(75, 23);
            this.btnDif_double.TabIndex = 15;
            this.btnDif_double.Text = "-";
            this.btnDif_double.UseVisualStyleBackColor = false;
            this.btnDif_double.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnDif_double_MouseClick);
            // 
            // btnDiv_double
            // 
            this.btnDiv_double.BackColor = System.Drawing.SystemColors.Control;
            this.btnDiv_double.Location = new System.Drawing.Point(104, 123);
            this.btnDiv_double.Name = "btnDiv_double";
            this.btnDiv_double.Size = new System.Drawing.Size(75, 23);
            this.btnDiv_double.TabIndex = 16;
            this.btnDiv_double.Text = "/";
            this.btnDiv_double.UseVisualStyleBackColor = false;
            this.btnDiv_double.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnDiv_double_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 398);
            this.Controls.Add(this.gbDouble);
            this.Controls.Add(this.gbDecimal);
            this.Name = "Form1";
            this.Text = "Действия над целыми и дробными числами";
            this.gbDecimal.ResumeLayout(false);
            this.gbDecimal.PerformLayout();
            this.gbDouble.ResumeLayout(false);
            this.gbDouble.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDecimal;
        private System.Windows.Forms.Label lblResult_decimal;
        private System.Windows.Forms.Button btnResult_decimal;
        private System.Windows.Forms.TextBox txtB_decimal;
        private System.Windows.Forms.Label lblB_decimal;
        private System.Windows.Forms.Button btnMod_decimal;
        private System.Windows.Forms.Button btnDiv_decimal;
        private System.Windows.Forms.Button btnDif_decimal;
        private System.Windows.Forms.Button btnSum_decimal;
        private System.Windows.Forms.Button btnMult_decimal;
        private System.Windows.Forms.TextBox txtA_decimal;
        private System.Windows.Forms.Label lblA_decimal;
        private System.Windows.Forms.GroupBox gbDouble;
        private System.Windows.Forms.Label lblResult_double;
        private System.Windows.Forms.Button btnSum_double;
        private System.Windows.Forms.Button btnResult_double;
        private System.Windows.Forms.Label lblA_double;
        private System.Windows.Forms.TextBox txtB_double;
        private System.Windows.Forms.TextBox txtA_double;
        private System.Windows.Forms.Label lblB_double;
        private System.Windows.Forms.Button btnMult_double;
        private System.Windows.Forms.Button btnMod_double;
        private System.Windows.Forms.Button btnDif_double;
        private System.Windows.Forms.Button btnDiv_double;
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExtensionsMethodsTest
{
    public static class MyExtensions
    {
        public static string[] MySplit(this string str) //метод расширения, вызывается иначе
        {
            
            return str.Split(' ');
        }
        public static int WordsCount(this string str)
        {
            return str.Split(new char[] {' ', '.', ',', '?', '!', ':', ';' }, 
                StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static int MultiplyBy(this int value, int multiplier)
        {
            return value * multiplier;
        }

        public static string ListToString (this IList list)
        {
            StringBuilder result = new StringBuilder("");
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                    result.AppendFormat(", {0}", list[i].ToString());
            }
            return result.ToString();
        }
    }
}

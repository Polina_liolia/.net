﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExtensionsMethodsTest
{
    class Program
    {
        //перечисления
        public enum MyColor 
        {
            White, //0 type byte
            Red, //1
            Green, //2
            Blue, //3
            Orange //4
        }

        public enum WeakDays : byte //enum - значимый тип //с указанием типа данных констант (int по умолчанию)
        {
            Monday,
            Tuthday,
            Wednethday,
            Thurthday,
            Friday,
            Saturday,
            Sunday
        }

        //усовершенствование метода GetEnumValues для того, чобы в дальнейшем избежать 
        //явного преобразования результата его работы
        public static TEnum[] GetEnumValues<TEnum>() where TEnum : struct //где TEnum - любой значимый тип
        {
            return (TEnum[])Enum.GetValues(typeof(TEnum)); //преобразование выполняться не будет, 
            //т.к. еще на этапе компиляции будет известно, какого типа то перечисление, для которого метод вызывется
        }

        static void Main(string[] args)
        {
            #region Статические методы и расширения:
            string str = "А роза упала на лапу Азора";
            Console.WriteLine("Using static class method:");
            string[]words = MyStaticClass.MySplit(str); //just a method of a static class
            foreach (string w in words)
                Console.WriteLine(w);
            Console.WriteLine();

            Console.WriteLine("Using extension method:");
            string[] words1 = str.MySplit();//extension method
            foreach (string w in words1)
                Console.WriteLine(w);
            Console.WriteLine();

            Console.WriteLine("Total words amount: {0}", str.WordsCount());//extension method
            Console.WriteLine();

            int num = 10;
            int multResult = num.MultiplyBy(5); //extension method
            Console.WriteLine("{0} * {1} = {2}", num, 5, multResult);

            List<int> intlist = new List<int>();
            Random r = new Random();
            for (int i = 0; i < 10; i++)
                intlist.Add(r.Next(1, 50));
            string listunion =  intlist.ListToString(); //extension method working with List
            Console.WriteLine(listunion);

            string arrUnion = words1.ListToString();//extension method working with array
            Console.WriteLine(arrUnion);
            #endregion

            #region Pабота с перечислениями:
            MyColor color = MyColor.Orange;
            Console.WriteLine(color.ToString()); //Orange
            string colorString = color.ToString("G");//G - general
            Console.WriteLine(colorString); //Orange
            Console.WriteLine(color.ToString("D")); //D - decimal //4
            Console.WriteLine("Базовый тип перечислений enum - это {0}", Enum.GetUnderlyingType(typeof(MyColor))); //Int32
            Console.WriteLine(Enum.Format(typeof(MyColor), 3, "G"));//Blue
            //узнать, сколько объектов у перечисления:
            MyColor[] colors = (MyColor[])Enum.GetValues(typeof(MyColor)); //всегда возвращает нетипизированнный System.Array
                                                                           //поэтому требуеьтся преобразование (MyColor[])
            foreach (MyColor c in colors)
                Console.WriteLine(c);
            Console.WriteLine("Total colors amount {0}",colors.Length);

            //вызов усовершенствованного метода GetValues:
            MyColor[] color2 = GetEnumValues<MyColor>(); 
            #endregion
        }
    }
}

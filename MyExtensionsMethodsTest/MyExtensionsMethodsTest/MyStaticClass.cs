﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyExtensionsMethodsTest
{
    public static class MyStaticClass
    {
        public static string [] MySplit(string str)
        {
            string[] words = str.Split(' ');
            return words;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DiskDirectoryFileInfoTest
{
    public class DelegateVsInterface
    {
        public delegate bool GetFilesFilter(string filename);
        public static bool FilterFiles(string file)
        {
            return file.Contains(".txt");
        }
        //if FilterFiles is used, returns array of all *.exe files in path
       

        public interface IGetFilesFilter
        {
            bool Filter(string path);
        }

        public class IGetFilesFilterDefRelease : IGetFilesFilter
        {
            public bool Filter(string file)
            {
                return file.Contains(".txt");
            }
        }

        public static string[] GetFiles(string path, GetFilesFilter filter) //второй аргумнгт - делегат
        {
            ArrayList res = new ArrayList();
            foreach (string file in Directory.GetFiles(path))
            {
                if (filter == null || filter(file))
                    res.Add(file);
            }
            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir, filter));
            return (string[])res.ToArray(typeof(string));
        }

        //работает быстрее
        public static string[] GetFiles(string path, IGetFilesFilter filter) //второй агрумент - не делегат, а интерфейс
        {
            ArrayList res = new ArrayList();
            foreach (string file in Directory.GetFiles(path))
            {
                if (filter == null || filter.Filter(file)) //вызываем метод интерфейса
                    res.Add(file);
            }
            foreach (string dir in Directory.GetDirectories(path))
                res.AddRange(GetFiles(dir, filter));
            return (string[])res.ToArray(typeof(string));
        }
    }
}

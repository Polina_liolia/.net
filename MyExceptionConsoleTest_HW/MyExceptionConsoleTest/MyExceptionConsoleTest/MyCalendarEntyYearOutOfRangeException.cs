﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MyExceptions
{
    [Serializable]
    public class MyCalendarEntyYearOutOfRangeException : ArgumentOutOfRangeException
    {
        public MyCalendarEntyYearOutOfRangeException() { }
        public MyCalendarEntyYearOutOfRangeException(string message) : base(message) { }
        public MyCalendarEntyYearOutOfRangeException(string message, Exception inner) : base(message, inner) { }
        protected MyCalendarEntyYearOutOfRangeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
        //специфические для класса ArgumentOutOfRangeException
        public MyCalendarEntyYearOutOfRangeException(String paramName, String message) : base (paramName, message){}
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using MyExceptions;

namespace MyExceptionConsoleTest
{
    class Program
    {
        /// <summary>
        /// Заполняет массив значениями из файла
        /// </summary>
        /// <param name="fileName">полное имя файл (включая путь к нему), который содержит данные для массива</param>
        /// <returns>Возвращает созданный и заполненный массив</returns>
        /// <exception cref="System.IO.FileNotFoundException">В случае если файл с заданным именем не найден</exception>
        /// <exception cref="System.ArrayTypeMismatchException">В случае, если файл содержит данные, которые не могут быть перобразованы к Double</exception>
        /// <exception cref="System.ArgumentNullException">В случае, если в качестве fileName пришел null (ДЗ)</exception>
        /// <exception cref="System.Exception">В остальных случаях (ДЗ)</exception>
        public static double[] getArrayFromFile(string fileName)
        {

            if (String.IsNullOrEmpty(fileName))
            {
                //  throw new ArgumentNullException();    //using standart ArgumentNullException
                throw new GetArrayFromFileArgumentNullException();
                //return new double[0]; //no need to return anything, because an ecxeption already has been thrown
            }
            else
            {
                                                       
                ArrayList array = new ArrayList();

                try
                {
                    //using (StreamReader reader = new StreamReader(fileName))
                    //{
                    StreamReader reader = new StreamReader(fileName);
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        array.Add(line); // Add to list.
                    }
                    //}
                }
                catch (System.IO.FileNotFoundException ex)
                {
                    Console.WriteLine(String.Format("Файл {0} не найден. Подробности об иcключении : {1}", ex.FileName, ex.Message));
                    return new double[0];
                }
                double[] d = new double[array.Count];
                int i =0;
                foreach(object item in array)
                {
                    try
                    {
                        //d[i++] = (double)item;//InvalidCastException
                        d[i++] = Convert.ToDouble((string)item);
                    }
                    catch(System.FormatException ex)
                    {
                        Console.WriteLine(String.Format("Преобразование значения {0} к типу Double не выполнено. Подробности об иcключении : {1}", item.ToString(), ex.Message));
                        return new double[0];
                    }
                    catch(System.ArrayTypeMismatchException ex)
                    {
                        Console.WriteLine(String.Format("Преобразование типа {0} к типу Double не выполнено. Подробности об иcключении : {1}", item.GetType(), ex.Message));
                        return new double[0];
                        /*
                         string[] names = {"Dog", "Cat", "Fish"};
            Object[] objs  = (Object[]) names;

            try 
            {
                objs[2] = "Mouse";

                foreach (object animalName in objs) 
                {
                    System.Console.WriteLine(animalName);
                }
            }
            catch (System.ArrayTypeMismatchException) 
            {
                // Not reached; "Mouse" is of the correct type.
                System.Console.WriteLine("Exception Thrown.");
            }

            try 
            {
                Object obj = (Object) 13;
                objs[2] = obj;
            }
            catch (System.ArrayTypeMismatchException) 
            {
                // Always reached, 13 is not a string.
                System.Console.WriteLine(
                    "New element is not of the correct type.");
            }

                         */
                    }
                }

                return d;

            }

        }
        /*
        #region Пример повторной генерации исключительной ситуации
        public static class Library
        {
            public static int[] FindOccurrences(this String s, String f)
            {
                var indexes = new List<int>();
                int currentIndex = 0;
                try
                {
                    while (currentIndex >= 0 && currentIndex < s.Length)
                    {
                        currentIndex = s.IndexOf(f, currentIndex);
                        if (currentIndex >= 0)
                        {
                            indexes.Add(currentIndex);
                            currentIndex++;
                        }
                    }
                }
                catch (ArgumentNullException e)
                {
                    // Perform some action here, such as logging this exception.
                    //реализуем например логгирование - сообщаем самим себе, где и что имено случилось
                    //и так как ситуация неустранима,  то запускаем повтрную генерацию этого же исключения
                    throw;
                }
                return indexes.ToArray();
            }
        }
       #endregion
        */
        public class CalendarEntry
        {
            // private field
            private DateTime date;

            // public field (Generally not recommended.)
            public string day;

            // Public property exposes date field safely.
            public DateTime Date
            {
                get
                {
                    return date;
                }
                set
                {
                    // Set some reasonable boundaries for likely birth dates.
                    if (value.Year > 1900 && value.Year <= DateTime.Today.Year)
                    {
                        date = value;
                    }
                    else
                        throw new ArgumentOutOfRangeException();
                }

            }

            // Public method also exposes date field safely.
            // Example call: birthday.SetDate("1975, 6, 30");
            public void SetDate(string dateString)
            {
                DateTime dt = Convert.ToDateTime(dateString);

                // Set some reasonable boundaries for likely birth dates.
                if (dt.Year > 1900 && dt.Year <= DateTime.Today.Year)
                {
                    date = dt;
                }
                else //использование собственного класса исключения - наследника НЕнаследника Exception
                    throw new MyCalendarEntyYearOutOfRangeException("SetDate", String.Format("Устанавливаемое значение {0} для года (Year) не принадлежит допустимому интервалу от 1900 до {1}", dt.Year, DateTime.Today.Year));
            }

            public TimeSpan GetTimeSpan(string dateString)
            {
                DateTime dt = Convert.ToDateTime(dateString);

                if (dt != null && dt.Ticks < date.Ticks)
                {
                    return date - dt;
                }
                else
                    throw new ArgumentOutOfRangeException();

            }
        }
        #region Создание собственных классов исключений - простое наследование от Exception
        [Serializable]
        public class MyException : Exception
        {
            public MyException() { }
            public MyException(string message) : base(message) { }
            public MyException(string message, Exception inner) : base(message, inner) { }
            protected MyException(
              System.Runtime.Serialization.SerializationInfo info,
              System.Runtime.Serialization.StreamingContext context)
                : base(info, context) { }
        }
        #endregion
        static void Main(string[] args)
        {
            //string filename = @"c:\mydata.txt";
            string filename = "mydata.txt";
            double[] arrDouble = getArrayFromFile(filename);

            //testing GetArrayFromFileArgumentNullException exception:
            filename = "";   //empty file name string
            try
            {
                arrDouble = getArrayFromFile(filename);
            }
            catch (GetArrayFromFileArgumentNullException ex)
            {
                Console.WriteLine(string.Format("GetArrayFromFileArgumentNullException occures. More information: {0}", ex.Message));
            }

            Console.ReadKey(true);

        }
    }


    abstract class MyAbstractClass
    {
        protected int x;
        public virtual double Area(){return 0;}
        public MyAbstractClass() { }
    }
}

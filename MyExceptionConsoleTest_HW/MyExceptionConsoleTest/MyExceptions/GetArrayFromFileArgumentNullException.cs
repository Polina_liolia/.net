﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;

namespace MyExceptions
{
    [Serializable]
    class GetArrayFromFileArgumentNullException : ArgumentNullException
    {
        public GetArrayFromFileArgumentNullException() {}
        public GetArrayFromFileArgumentNullException(string message) : base(message) { }
        public GetArrayFromFileArgumentNullException(string message, Exception innerException) : base(message, innerException) {}
        protected GetArrayFromFileArgumentNullException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) {}
        public GetArrayFromFileArgumentNullException(string paramname, string message) : base(paramname, message){}
    }
}

���� ������:
1. ��������� (refference type) - ��� ���������� System.Object, ��������� ��� ����������� �������� ������
	-��� ������ (class)
	-��� ������� (array)
	-��� ������ (string)
2. �������� (value type) - ��� ����������  System.ValueType, ��������� � ����� ������ ��� ���������� �������� ������
	-��� ��������� (struct: DateTime, Point)
	-��� "�������" ���� (int, double, float, char)

System.Object - �� ���� �����������:
	- ��� ��������� ���� (������)
	- System.ValueType - �� ���� ������.:
		- ��� �������� ���� (������)

������� ��� ������ ���� � ���� �����:
.GetType()
.ToString()
.GetHashCode()
.Equals()

/*
artem.konstantinovich@ukr.net
FYI � �� � ������� 27.07.2017
*/

�������� ���������� � ������:
in	�������� �����
ref	�����
out	�����������

in - �������� �� ���������
��������� ������ - ����������� ����� ������:
 public static void testString(in string str) //in ������������ �� ���������, ����� �� ������
        {
            str = "����� " + "������ " + "��������� ������";
        }


ref - ��������� ������ � ������� �����
public static void changeStr(ref string str)//������ ����� ��������
        {
            str = "new value string";
        }

public static void ChangeArray(ref int[] array, int pos, int v)//����� ����� �� ������������ ref, �.�. ������ � ����� ������ ���������� �� ������ 
        {
            array[pos] = v;
        }

out - ��������� ��������� �-���, ������� ������ ���� ����������
���� ����� �������, ������������ ��������� ��������:
1) ������������ �������� ��������� (out) 	(no return)
2) ������� ������ 				(return array)
3) ������� ���� ������ - ��������� 		(return new MyStruct())

//������������� ������� ���������������� �� ��������� - �������, ������������ ��������� ���������� / ������������ ���������� (����., bool)
- ��� �-��� bool
- ��� �-��� ���������� ��������� � ����������
- ���������� ������� ��������� ��������� 
- ������ �-��� - �������� 5-11 �����
- ��� �-��� static
public static bool calcSolutions2(int a, int b, int c, out double x1, out double x2)
        {
            bool result = false; //������������ ���������
            double d = Math.Sqrt(Math.Pow(b, 2.0) - 4 * a * c);
            if (d >= 0)
            {
                result = true;
                x1 = (-b - Math.Sqrt(d)) / (2 * a);
                x2 = (-b + Math.Sqrt(d)) / (2 * a);
            }
            else
            {
                result = false;
                x1 = 0;
                x2 = 0;
            }
            return result;
        }

//�������� ���������������� - ������������� try catch



Wondows Forms
events:
-Click - ����� ��� �����������
-MouseClick - ������ ���� �����


























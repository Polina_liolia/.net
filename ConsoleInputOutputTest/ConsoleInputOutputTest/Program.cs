﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleInputOutputTest
{
    class Program
    {
       public static void Main(string[] args)
        {
            //всегда вводят только символы, преобразование - задача программиста
            Console.Write("Введите целое число");//без перехода на новую строку
            String inputval1 = Console.ReadLine();
            int n1 = Convert.ToInt32(inputval1);

            Console.Write("Введите целое число");//без перехода на новую строку
            String inputval2 = Console.ReadLine();
            int n2 = Int32.Parse(inputval2);

            int n3 = -1;
            bool result = Int32.TryParse(inputval2, out n3);//проверка возможности преобразования, если возможно - преобразует и запишет в out

            //int n4 = Console.ReadLine();//цепь неявных преобразований

            //сравнение дробных типов - с указанием точности Е(0.001) и по модулю abs (т.к. есть NaN +0.000... и -0.000...)
            //compare with 0:
            double eps = 0.001;
            double d = 0.001;
            
            //is d positive:     
            result = d > eps;

            double d1 = 2.524;
            //compare two doubles on equivalecne(==):
            result = Math.Abs(d - d1) <= eps;
        }
    }
}

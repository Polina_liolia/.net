﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    public class Client : IEquatable<Client>, IComparable, IComparer<Client>, ICloneable
    {
        private int clients_id;
        private string clients_name;
        private string clients_phone;
        private string clients_mail;

        #region Properties
        public int ID { get { return this.clients_id; } }
        public string Name
        {
            set { clients_name = value; }
            get { return clients_name; }
        }
        public string Phone
        {
            get { return this.clients_phone; }
            set { this.clients_phone = value; }
        }
        public string Mail
        {
            set { this.clients_mail = value; }
            get { return this.clients_mail; }
        }
        #endregion

        #region Constructors
        protected Client() { }
        public Client(int clients_id,
            string clients_name,
            string clients_phone,
            string clients_mail
            )
        {
            this.clients_id = clients_id;
            this.clients_name = clients_name;
            this.clients_phone = clients_phone;
            this.clients_mail = clients_mail;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("id: {0}, name: {1}, phone: {2}, e-mail: {3}",
                this.clients_id, this.clients_name, this.clients_phone, this.clients_mail);
        }

        public override int GetHashCode()//для того, чтобы объект мог быть использован в контейнерах типа Dictionary
        {
            return this.clients_id.GetHashCode(); //основывается на том поле, которое задействовано в Equals
        }



        #endregion

        #region Operators overloaded
        //операторы сравнения "==" и "!=" основываются на Equals из System.Object
        public static bool operator ==(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return Object.Equals(client1, client2);
            return client1.Equals(client2);
        }
        public static bool operator !=(Client client1, Client client2)
        {
            if (((object)client1) == null || ((object)client2) == null)
                return !Object.Equals(client1, client2);

            return !(client1.Equals(client2));
        }

        //операторы сравнения могут использовать свою логику для сравнения, в зависимости о  задачи
        public static bool operator <(Client person1, Client person2)
        {
            return person1.clients_id < person2.clients_id;
        }

        public static bool operator >(Client person1, Client person2)
        {
            return person1.clients_id > person2.clients_id;
        }
        #endregion

        #region Equals
        //операторы равенства/неравенства должны быть реализованы с помощью принятых ранее реализаций Equals, чтобы сохранить единую логику
        public bool Equals(Client other)//реализация интерфейса IEquatable
        {
            if ((object)other == null)
                return false;
            if (this.clients_id == other.clients_id)
                return true;
            else
                return false;
        }
        public override bool Equals(Object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Client clientObj = obj as Client;
            if (clientObj == null)
                return false;
            else
                return Equals(clientObj);//вызов реализации для интерфейса-строгий тип
        }
        #endregion

        #region Clone
        public Client ShallowCopy()//удвоение ссылки
        {
            return (Client)this.MemberwiseClone(); //не затрагивает ссылочные типы внутри объекта (в данном случае - поля IdInfo (тип IdInfo) и Name (string)
        }

        public object Clone()
        {
            return ShallowCopy();
        }
        #endregion

        #region IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Client other_client = obj as MyDataBaseClasses.Client;
            if (other_client != null) 
                return this.clients_id.CompareTo(other_client.clients_id);
            else
                throw new ArgumentException("Object is not a Client");
        }

        #endregion

        #region IComparer
        public int Compare(Client x, Client y)
        {
            if (x == null && y != null)
                return -1;
            if (y == null && x != null)
                return 1;
            return x.CompareTo(y);
        }
        #endregion

        #region SQL queries 
        public static string getAllClientSSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master select ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_ID], ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_NAME], ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_PHONE], ");
            sb.Append(" [dbo].[CLIENTS].[CLIENTS_MAIL] ");
            sb.Append(" from  ");
            sb.Append(" [dbo].[CLIENTS] ");
            return sb.ToString();
        }

        public static string getClientSelect(int ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getAllClientSSelect());
            sb.Append(string.Format(" WHERE [DBO].[CLIENTS].[CLIENTS_ID] = {0}; ", ID));
            return sb.ToString();
        }

        public static string updateUnrequiredFields()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" UPDATE [DBO].[CLIENTS] ");
            sb.Append(" SET [DBO].[CLIENTS].[CLIENTS_NAME] = 'UNKNOWN' ");
            sb.Append(" WHERE [DBO].[CLIENTS].[CLIENTS_NAME] IS NULL; ");
            return sb.ToString();
        }

        public static string updateClientsInDB(int ID, Client New)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" UPDATE [DBO].[CLIENTS] ");
            sb.Append(string.Format(" SET [DBO].[CLIENTS].[CLIENTS_MAIL] = N'{0}', ", New.Mail));
            sb.Append(string.Format(" [DBO].[CLIENTS].[CLIENTS_NAME] = N'{0}', ", New.Name));
            sb.Append(string.Format(" [DBO].[CLIENTS].[CLIENTS_PHONE] = N'{0}', ", New.Phone));
            sb.Append(string.Format(" WHERE [DBO].[CLIENTS].[CLIENTS_ID] = {0}; ", ID));
            return sb.ToString();
        }
       
        public static string deleteClientFromDB(int ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" DELETE FROM [DBO].[CLIENTS] ");
            sb.Append(string.Format(" WHERE [DBO].[CLIENTS].[CLIENTS_ID] = {0}; ", ID));
            return sb.ToString();
        }

        #endregion
    }
}


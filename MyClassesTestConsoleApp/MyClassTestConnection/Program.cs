﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDataBaseClasses;

namespace MyClassesTestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client1 = new Client(1, "vasia", "0687452145", "mail@gmail.ru");
            Client client2 = new Client(2, "kolia", "0655214785", "email@mail.ru");
            Client client3 = client1;
            Client client4 = client2.Clone() as Client;
            Console.WriteLine(client1);
            Console.WriteLine(client2);
            Console.WriteLine(client3);
            Console.WriteLine(client4);
            Console.WriteLine(client1 == client3);//true
            Console.WriteLine(client1 != client2);//true
            Console.WriteLine(client1 < client2);//true
            Console.WriteLine(client1 > client2);//false
            Console.WriteLine(client2.Equals(client4)); //true
            Console.WriteLine(client2.CompareTo(client1)); //1

            List<Client> clientsList = new List<Client>();
            clientsList.Add(client1);
            clientsList.Add(client2);

            ArrayList clientsArrList = new ArrayList();
            clientsArrList.Add(client3);
            clientsArrList.Add(client4);

            Console.WriteLine("Clients in List:");
            foreach (Client c in clientsList)
                Console.WriteLine(c);

            Console.WriteLine("Clients in ArrayList:");
            foreach (Client c in clientsArrList)
                Console.WriteLine(c);


        }
    }
}

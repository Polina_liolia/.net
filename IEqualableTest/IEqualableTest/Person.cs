﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEquatableTest
{

    
    public class IdInfo
    {
        public int IdNumber;
        public IdInfo(int IdNumber)
        {
            this.IdNumber = IdNumber;
        }
    }
  
    class Person : IEquatable<Person>, ICloneable
    {
        private string uniqueSsn;

        public string UniqueSsn
        {
            get  { return uniqueSsn; }
            set  { uniqueSsn = value; }
        }

        private string lName;
        public string LName
        {
            get { return lName;  }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new ArgumentException("The last name can not be null or empty");
                else
                    lName = value;
            }
        }

        #region Additional fields
        public int Age;
        public string Name;
        public IdInfo IdInfo;
        #endregion

        #region Constructors
        protected Person() { }

        public Person (string lName, string SSN)
        {
            this.lName = lName;
            this.uniqueSsn = SSN;
        }
        #endregion

        #region Equals Implementation

        public bool Equals(Person other) //реализация интерфейса
        {
            if (other == null)
                return false;
            if (this.uniqueSsn == other.uniqueSsn)
                return true;
            else
                return false;
        }

        public override bool Equals(object obj)//реализация System.Object
        {
            if (obj == null)
                return false;
            Person personObj = obj as Person;
            if (personObj == null)
                return false;
            else
                return Equals(personObj);//вызов реализации для интерфейса (в соответствии с типом аргумента) 
        }

        public override int GetHashCode()//для того, чтобы объект мог быть использован в контейнерах типа Dictionary
        {
            return this.uniqueSsn.GetHashCode(); //основывается на том поле, которое задействовано в Equals
        }

        //операторы равенства/неравенства должны быть реализованы с помощью принятых ранее реализаций Equals, чтобы сохранить единую логику
        public static bool operator == (Person person1, Person person2)
        {
            if ((object)person1 == null || (object)person2 == null)
                return Object.Equals(person1, person2);
            return person1.Equals(person2);
        }

        public static bool operator!=(Person person1, Person person2)
        {
            if ((object)person1 == null || (object)person2 == null)
                return Object.Equals(person1, person2);
            return !person1.Equals(person2);
        }

        //операторы сравнения могут использовать свою логику для сравнения, в зависимости о  задачи
        public static bool operator < (Person person1, Person person2)
        {
            int result = string.Compare(person1.lName, person2.lName);
            switch (result)
            {
                case 0: return false;
                case 1: return false;
                case -1: return true;
                default: return false;
            }
        }

        public static bool operator >(Person person1, Person person2)
        {
            int result = string.Compare(person1.lName, person2.lName);
            switch (result)
            {
                case 0: return false;
                case -1: return false;
                case 1: return true;
                default: return false;
            }
        }

        #endregion

        #region IClonable
        public Person ShallowCopy()//удвоение ссылки
        {
            return (Person)this.MemberwiseClone(); //не затрагивает ссылочные типы внутри объекта (в данном случае - поля IdInfo (тип IdInfo) и Name (string)
        }

        public Person DeepCopy()//правильная реализация интерфейса IClonable
        {
            Person other = (Person)this.MemberwiseClone();
            //все "свои" ссылочные типы заполняем сами
            other.IdInfo = new IdInfo(IdInfo.IdNumber);
            other.Name = string.Copy(Name);
            return other;
        }

        //логика метода DeepCopy применяется для классов со ссылочными полями (именно собственными объектами)
        public object Clone()
        {
            return DeepCopy();
        }
    }

    public class Address : ICloneable
    {
        public int HouseNumber { get; set; }
        public string StreetName { get; set; }
        //логика ShallowCopy используется, если у класса все поля - "стандартного" типа
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }



    #endregion

}

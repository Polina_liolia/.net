﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEquatableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Person applicant1 = new Person("Jones", "55-88-41");
            Person applicant2 = new Person("Mary", "55-99-45");
            Person applicant3 = new Person("Andrew", "54-65-89");

            List<Person> applicants = new List<Person>();   //типизированный контейнер
            applicants.Add(applicant1);
            applicants.Add(applicant2);
            applicants.Add(applicant3);
            Console.WriteLine(applicants.Contains(applicant2));//будет использовать Equals из реализации интерфейса

            ArrayList list = new ArrayList();  //не типизированный контейнер, все эл-ты - наследники System.Object
            list.Add(applicant1);
            Console.WriteLine(list.Contains(applicant1));//будет использовать Equals из System.Object


            #region Clonable Test
            // Create an instance of Person and assign values to its fields.
            Person p1 = new Person("Jones", "199-29-3999");
            p1.Age = 42;
            p1.Name = "Sam";
            p1.IdInfo = new IdInfo(6565);
            // Perform a shallow copy of p1 and assign it to p2.
            Person p2 = p1.ShallowCopy();
            // Display values of p1, p2
            Console.WriteLine("Original values of p1 and p2:");
            Console.WriteLine("   p1 instance values: ");
            DisplayValues(p1);
            Console.WriteLine("   p2 instance values:");
            DisplayValues(p2);

            // Change the value of p1 properties and display the values of p1 and p2.
            p1.Age = 32;
            p1.Name = "Frank";
            p1.IdInfo.IdNumber = 7878;
            Console.WriteLine("\nValues of p1 and p2 after changes to p1:");
            Console.WriteLine("   p1 instance values: ");
            DisplayValues(p1);
            Console.WriteLine("   p2 instance values:");
            DisplayValues(p2);

            // Make a deep copy of p1 and assign it to p3.
            Person p3 = p1.DeepCopy();
            // Change the members of the p1 class to new values to show the deep copy.
            p1.Name = "George";
            p1.Age = 39;
            p1.IdInfo.IdNumber = 8641;
            Console.WriteLine("\nValues of p1 and p3 after changes to p1:");
            Console.WriteLine("   p1 instance values: ");
            DisplayValues(p1);
            Console.WriteLine("   p3 instance values:");
            DisplayValues(p3);

            #endregion
        }
    }
}

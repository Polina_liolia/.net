﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDelegateForSort
{
    public class MyEmployee : Person
    {
        public double Salary { set; get; }
        public override string ToString()
        {
            return string.Format("Name: {0}, Age: {1}, Salary: {2}", this.Name, this.Age, this.Salary);
        }

        Predicate<MyEmployee> isPositive = delegate (MyEmployee x) { return x.Salary > 1500; };

        Func<MyEmployee, bool> NameContains = x => x.Name.Contains("o"); //1-й тип даных - принимаемый, 2-й - возвращаемый

        //Эти две ф-ции аналогичны, существовать одновременно в одном классе не могут
        //т.к. компилятор н сможет определить, какую из них вызывать
        public static double calcSalary(MyEmployee[] list, Predicate<MyEmployee> pr)
        {
            double sum = 0;
            foreach (MyEmployee em in list)
                if (pr(em))
                    sum += em.Salary;
            return sum;
        }

        public static double calcSalary(MyEmployee[] list, Func<MyEmployee, bool> op)
        {
            double sum = 0;
            foreach (MyEmployee em in list)
                if (op(em))
                    sum += em.Salary;
            return sum;
        }
    }
}

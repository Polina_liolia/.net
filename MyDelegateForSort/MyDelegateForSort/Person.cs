﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDelegateForSort
{
    public class Person
    {
        //класс без конструкторов и полей, только публичные св-ва:
        public string Name { set; get; }
        public int Age { set; get; }
        public void Print() => Console.WriteLine("Name={0}, Age={1}", Name, Age);
        public override string ToString()
        {
            return string.Format("Name: {0}, Age: {1}", this.Name, this.Age);
        }

        
    }
}

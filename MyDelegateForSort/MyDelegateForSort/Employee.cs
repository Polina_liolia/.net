﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDelegateForSort
{
    public class Employee : Person
    {
        public double Salary { set; get; }
        public override string ToString()
        {
            return string.Format("Name: {0}, Age: {1}, Salary: {2}", this.Name, this.Age, this.Salary);
        }

        public delegate bool MyComparerEmployee(Employee em); //аналог Predicate<Employee> MyComparerEmployee
        public static double calcSalary(Employee[] list, Func<Employee, bool> op)
        {
            double sum = 0;
            foreach (Employee em in list)
                if(op(em))
                    sum += em.Salary;
            return sum;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDelegateForSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[10];
            Random r = new Random();
            //populating array with random numbers:
            for (int i = 0; i < array.Length; i++)
                array[i] = r.Next(1, 20);
            //outputting array to console:
            Console.WriteLine("Initial:");
            for (int i = 0; i < array.Length; i++)
                Console.WriteLine(array[i]);

            #region Простые делегаты
            //array sort:
            MyComparerOperation isLessDeligate = new MyComparerOperation(isLess);
            MyBubbleSort(array, isLessDeligate);
            Console.WriteLine("Sort with isLess:");
            for (int i = 0; i < array.Length; i++)
                Console.WriteLine(array[i]);

            MyComparerOperation isGreaterDeligate = new MyComparerOperation(isGreater);
            MyBubbleSort(array, isGreaterDeligate);
            Console.WriteLine("Sort with isGreater:");
            for (int i = 0; i < array.Length; i++)
                Console.WriteLine(array[i]);
            #endregion

            #region Стандартные делегаты
            Action<int, int> op; //стандартный делегат, принимает от 1 до 16 параметров стандартного типа
            op = Add;
            Operation(10, 6, op);
            op = Substract;
            Operation(10, 6, op);
            Predicate<int> isPositive = delegate (int x)  { return x > 0; }; //работает только с одним аргуметром, замена if
            Console.WriteLine(isPositive(20));
            Console.WriteLine(isPositive(-20));
            Func<int, int> myFunction = factorial; //1-й тип даных - принимаемый, 2-й - возвращаемый
            
            Func<int, int, int, int> myFunction2 = mySum;
            testFunctionFactorial(3, myFunction);
            testFunctionMySum(1, 2, 3, myFunction2);
            #endregion

            #region Простые лямбда-выражения
            myFunction =                             x          =>                    x - x;
            //присваеваестя функтору/делегату    параметры    неперегруж.оператор          выражение

            //лямбда-выраж. из ф-ции isZero:
            Func<int, bool> myFunction3 = x => x == 0;

            //лямбда-выраж. из ф-ции isGreater:
            Func<int, int, bool> myFunction4 = (x,y) => (x < y);

            testFunctionMySum(1, 2, 3, (x, y, z) => x + y + z);
            testFunctionMySum(1, 2, 3, (x, y, z) => x + y + z);

            Action message; //void ф-ция, ничего не принимает
            message = () => { Console.WriteLine("Привет от лямбды"); };
            message();

            Action<string, int> message2 = (name, age) => { Console.WriteLine("Привет, {0}! Тебе {1} лет?", name, age); };
            message2("Vasia", 5);

            Action<string, int> message3 = (name, age) =>
           {
               Console.WriteLine("Привет, {0}!", name);
               Console.WriteLine("Тебе, {0} лет?", age);
           };
            message2("Kolia", 10);

            //пример того, что лямбда-выражения могут быть аргументами ф-ции. Так лучше не делать:)
            testActions(
                () => { Console.WriteLine("Привет от лямбды"); },
                (name, age) => { Console.WriteLine("Привет, {0}! Тебе {1} лет?", name, age); },
                (name, age) =>
                {
                    Console.WriteLine("Привет, {0}!", name);
                    Console.WriteLine("Тебе, {0} лет?", age);
                });
            #endregion

            //в момент создания выполняем инициализацию всех public св-в:
            Person person = new Person { Name = "Tom", Age = 25 };
            Person person2 = new Person () { Name = "Tom", Age = 25 };//то же самое
            //вызывается неявно созданный конструктор по умолчанию, который ничего не делает
            Person person3 = new Person();

            /*
             По шагам:
             1) Инициализация всех полей
             2) Конструктор по умолчанию
             3) Вызов метода set
             */
            Man person4 = new Man() { Name = "Petia" };

            #region Пример использования лямбда-выражений:
            Console.WriteLine("Равны нулю:");
            PrintArray(array, x => x == 0); //выведет эл-ты, равные нулю
            Console.WriteLine("Четные:");
            PrintArray(array, x => x%2 == 0); //выведет четные эл-ты 
            Console.WriteLine("Не четные:");
            PrintArray(array, x => x % 2 != 0); //выведет не четные эл-ты 
            Console.WriteLine("Больше 5:");
            PrintArray(array, x => x > 5); //выведет эл-ты больше 5
            #endregion

            //Генерализировать PrintArray
            //Перейти на использование своего класса Person
            //Вывогдить на экран персон по условию (напр., определенный возраст, первая буква имени...)
            Person[] persons = new Person[10];
            persons[0] = new Person() { Name = "Tom", Age = 25 };
            persons[1] = new Person() { Name = "Ken", Age = 37 };
            persons[2] = new Person() { Name = "Ann", Age = 18 };
            persons[3] = new Person() { Name = "Kate", Age = 41 };
            persons[4] = new Person() { Name = "Jack", Age = 22 };
            persons[5] = new Person() { Name = "Liz", Age = 31 };
            persons[6] = new Person() { Name = "Jane", Age = 52 };
            persons[7] = new Person() { Name = "Rob", Age = 12 };
            persons[8] = new Person() { Name = "Mary", Age = 33 };
            persons[9] = new Person() { Name = "Ron", Age = 45 };
            Console.WriteLine("Persons older then 35:");
            PrintArray<Person>(persons, x => x.Age > 35);
            Console.WriteLine("Persons, whose name starts from charackter 'K':");
            PrintArray<Person>(persons, x => x.Name.StartsWith("K"));
            Console.WriteLine("Persons, whose name contains charackter 'o':");
            PrintArray<Person>(persons, x => x.Name.Contains("o"));

            Employee[] employees = new Employee[3];
            employees[0] = new Employee { Name = "Tom", Age = 25, Salary = 1200.20 };
            employees[1] = new Employee { Name = "Ken", Age = 37, Salary = 2020.36 };
            employees[2] = new Employee { Name = "Ann", Age = 18, Salary = 1500.00 };
            Console.WriteLine(Employee.calcSalary(employees, e => e.Salary > 900));
            Console.WriteLine(Employee.calcSalary(employees, e => e.Name.Contains("n")));

            MyEmployee[] empls = new MyEmployee[1];
            empls[0] = new MyEmployee { Name = "Tom", Age = 25, Salary = 1200.20 };
           // MyEmployee.calcSalary(empls, x => x.Name.Contains("o"));//множественный вызов - ошибка
        }
        public delegate bool MyComparerOperation(int a, int b);//объектно-ориентированный указатель на функцию
        

        #region Compare functions
        public static bool isLess(int a, int b)
        {
            return a < b;
        }
        public static bool isLessOrEqual(int a, int b)
        {
            return a <= b;
        }

        public static bool isGreater(int a, int b)
        {
            return a > b;
        }

        public static bool isGreaterOrEqual(int a, int b)
        {
            return a >= b;
        }
        #endregion

        //генерализация - создание обобщенной функции (в с++ - шаблонная ф-ция)
        //без ограничения на тип параметра
        public static void Swap<T>(ref T lv, ref T rv)
        {
            T temp = lv;
            lv = rv;
            rv = temp;
        }
        //ограничиваем тип параметра объектами, унаследованными от IComparable:
        public static void SwapForComparable<T>(ref T lv, ref T rv)
            where T : System.IComparable<T>
        {
            T temp = lv;
            lv = rv;
            rv = temp;
        }

        public static void MyBubbleSort(int [] arr, MyComparerOperation comparerOp)
        {
            for (int write = 0; write < arr.Length; write++)
            {
                for (int sort = 0; sort < arr.Length - 1; sort++)
                {
                    if (comparerOp(arr[sort], arr[sort + 1]))
                    {
                        //Swap<int>(ref arr[sort], ref arr[sort + 1]);//пример использования обобщенной ф-ции
                        SwapForComparable<int>(ref arr[sort], ref arr[sort + 1]);//использование обобщенной ф-ции, 
                        //конкретизированной под объекты, для которых реализован интерфейс IComparable
                    }
                }
            }
        }

        static void Operation(int x1, int x2, Action<int, int> op)
        {
            if (x1 > x2) op(x1, x2);
        }
        static void Add(int x1, int x2)
        {
            Console.WriteLine("Сумма чисел: {0}", (x1 + x2));
        }
        static void Substract (int x1, int x2)
        {
            Console.WriteLine("Разность чисел: {0}", (x1 - x2));
        }
        static int factorial(int n)
        {
            int result = 1;
            for (int i = 1; i <= n; i++)
                result *= i;
            return result;
        }
        static int mySum(int a, int b, int c)
        {
            return a + b + c;
        }
        static void testFunctionFactorial(int n, Func<int, int> op)
        {
            Console.WriteLine("Факториал {0} = {1}", n, op(n));
        }

        static void testFunctionMySum(int a, int b, int c, Func<int, int, int, int> op)
        {
            Console.WriteLine("Сумма {0}, {1}, {2} = {3}", a, b, c, op(a, b, c));
        }

        bool isZero (int a)
        {
            return a  == 0;
        }

        public static void testActions(Action message, Action<string, int> message2, Action<string, int> message3)
        {
            message();
            message2("Test1", 1);
            message3("Test2", 2);
        }

        //вывод эл-тов массива по условию:
        static void PrintArray<T> (T[]array, Func<T, bool>op)
        {
            foreach (T i in array)
                if (op(i))
                    Console.WriteLine(i);
        }

       

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses 
{
    public class Order : IEquatable<Order>, IComparable, IComparer<Order>, ICloneable
    {
        int order_ID;
        string description;
        DateTime order_date;
        double total_costs;
        int client_ID;

        #region Properties
        public int Orders_ID
        {
            get
            {
                return order_ID;
            }

            set
            {
                order_ID = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public DateTime Order_date
        {
            get
            {
                return order_date;
            }

            set
            {
                order_date = value;
            }
        }

        public double Total_costs
        {
            get
            {
                return total_costs;
            }

            set
            {
                total_costs = value;
            }
        }

        public int Client_ID
        {
            get
            {
                return client_ID;
            }

            set
            {
                client_ID = value;
            }
        }
        #endregion

        #region Constructors
        private Order() { }
        public Order(int order_ID, string description, DateTime order_date, double total_costs, int client_ID)
        {
            this.order_ID = order_ID;
            this.description = description;
            this.order_date = order_date;
            this.total_costs = total_costs;
            this.client_ID = client_ID;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("order ID: {0}, description: {1}, date: {2}, total costs: {3}, client ID: {4}",
                order_ID, description, order_date, total_costs, client_ID);
        }
        public override int GetHashCode()
        {
            return order_ID.GetHashCode();
        }

        #endregion

        #region Operators overloaded
        public static bool operator == (Order order1, Order order2)
        {
            if ((object)order1 == null || (object)order2 == null)
                return object.Equals(order1, order2);
            return order1.Equals(order2);
        }

        public static bool operator !=(Order order1, Order order2)
        {
            if ((object)order1 == null || (object)order2 == null)
                return !object.Equals(order1, order2);
            return !order1.Equals(order2);
        }

        public static bool operator > (Order order1, Order order2)
        {
            if (order1 == null && order2 != null)
                return false;
            if (order1 != null && order2 == null)
                return true;
            return (order1.order_ID > order2.order_ID);
        }

        public static bool operator < (Order order1, Order order2)
        {
            if (order1 == null && order2 != null)
                return true;
            if (order1 != null && order2 == null)
                return false;
            return (order1.order_ID < order2.order_ID);
        }
        #endregion

        #region IEquatable<Order>
        public bool Equals(Order other)
        {
            if ((object)other == null)
                return false;
            return (order_ID == other.order_ID);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Order order = obj as Order;
            if (order != null)
                return Equals(order);
            else
                throw new ArgumentException("Argument is not an Order");
        }
        #endregion

        #region IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Order order = obj as Order;
            if (order != null)
                return order_ID.CompareTo(order.order_ID);
            else
                throw new ArgumentException("Argument is not an Order");
        }
        #endregion 

        #region IComparer<Order>
        public int Compare(Order x, Order y)
        {
            if (x == null && y != null)
                return -1;
            if (x != null && y == null)
                return 1;
            return x.CompareTo(y);
        }
        #endregion

        #region ICloneable
        public object Clone()
        {
            Order New = (Order)this.MemberwiseClone();
            New.description = string.Copy(description);
            New.order_date = new DateTime(order_date.Year, order_date.Month, order_date.Day);
            return New;
        }
        #endregion

        #region DB queries
        public static string getAllOrdersSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master select ");
            sb.Append(" [DBO].[ORDERS].ORDERS_ID, ");
            sb.Append(" [DBO].[ORDERS].DESCRIPTION AS ORDER_DESCRIPTION, ");
            sb.Append(" [DBO].[ORDERS].ORDERS_DATE, ");
            sb.Append(" [DBO].[ORDERS].TOTAL_COSTS, ");
            sb.Append(" [DBO].[CLIENTS].CLIENTS_ID, ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_NAME], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_PHONE], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_MAIL] ");
            sb.Append(" FROM [DBO].[ORDERS] ");
            sb.Append(" LEFT JOIN [DBO].[CLIENTS] ");
            sb.Append(" ON [DBO].[ORDERS].[CLIENTS_ID] = [DBO].[CLIENTS].[CLIENTS_ID]; ");
            return sb.ToString();
        }

        public static string getOrdersSelect(int ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getAllOrdersSelect());
            sb.Append(string.Format(" WHERE [DBO].[ORDERS].ORDERS_ID = {0}; ", ID));
            return sb.ToString();
        }

        public static string updateUnrequiredFields()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" UPDATE [DBO].[ORDERS] ");
            sb.Append(" SET [DBO].[ORDERS].[DESCRIPTION] = 'NO DESCRIPTION' ");
            sb.Append(" WHERE [DBO].[ORDERS].[DESCRIPTION] IS NULL; ");
            return sb.ToString();
        }

        public static string updateOrderInDB(int ID, Order New)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" UPDATE [DBO].[ORDERS] ");
            sb.Append(string.Format(" SET [DBO].[ORDERS].DESCRIPTION = N'{0}', ", New.description));
            sb.Append(string.Format(" [DBO].[ORDERS].ORDERS_DATE = '{0}-{1}-{2}', ", 
                New.order_date.Year, New.order_date.Month, New.order_date.Day));
            sb.Append(string.Format(" [DBO].[ORDERS].TOTAL_COSTS = {0}, ", New.total_costs));
            sb.Append(string.Format(" [DBO].[ORDERS].CLIENTS_ID = {0} ", New.client_ID));
            sb.Append(string.Format(" WHERE [DBO].[ORDERS].ORDERS_ID = {0}; ", ID)); 
            return sb.ToString();
        }

        public static string deleteOrderFromDB(int ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" DELETE FROM [DBO].[ORDERS] ");
            sb.Append(string.Format(" WHERE [DBO].[ORDERS].ORDERS_ID = {0}; ", ID));
            return sb.ToString();
        }

        public static string insertIntoDB(Order o)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" INSERT INTO [dbo].[ORDERS] ");
            sb.Append(string.Format(" ([ORDERS_DATE], [DESCRIPTION], [TOTAL_COSTS], [CLIENTS_ID]) "));
            sb.Append(string.Format(" VALUES ({0}, {1}, {2}, {3}); ", o.order_date, o.description,
                o.total_costs, o.client_ID));
            return sb.ToString();
        }
        #endregion
    }
}

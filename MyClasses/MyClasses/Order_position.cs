﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    public class Order_position : IEquatable<Order_position>, IComparable, IComparer<Order_position>, ICloneable
    {
        int order_positions_ID;
        int order_ID;
        int product_ID;
        double price;//ambigious?..
        int item_count;

        #region Properties
        public int Order_positions_ID
        {
            get
            {
                return order_positions_ID;
            }

            set
            {
                order_positions_ID = value;
            }
        }

        public int Order_ID
        {
            get
            {
                return order_ID;
            }

            set
            {
                order_ID = value;
            }
        }

        public int Product_ID
        {
            get
            {
                return product_ID;
            }

            set
            {
                product_ID = value;
            }
        }

        public double Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }

        public int Item_count
        {
            get
            {
                return item_count;
            }

            set
            {
                item_count = value;
            }
        }
        #endregion

        #region Constructors
        private Order_position() { }
        public Order_position(int order_positions_ID, int order_ID, int product_ID, double price, int item_count)
        {
            this.order_positions_ID = order_positions_ID;
            this.order_ID = order_ID;
            this.product_ID = product_ID;
            this.price = price;
            this.item_count = item_count;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("Order positions ID: {0}, order ID: {1}, product ID: {2}, price: {3}, item count: {4}",
                order_positions_ID, order_ID, product_ID, price, item_count);
        }
        public override int GetHashCode()
        {
            return order_positions_ID.GetHashCode();
        }
        #endregion

        #region Operators overloaded
        public static bool operator == (Order_position x, Order_position y)
        {
            if (x == null || y == null)
                return object.Equals(x, y);
            return x.Equals(y);
        }
        public static bool operator !=(Order_position x, Order_position y)
        {
            if (x == null || y == null)
                return !object.Equals(x, y);
            return !x.Equals(y);
        }
        public static bool operator <(Order_position x, Order_position y)
        {
            if (x == null && y != null)
                return true;
            if (x != null && y == null)
                return false;
            return x.order_positions_ID < y.order_positions_ID;
        }
        public static bool operator >(Order_position x, Order_position y)
        {
            if (x == null && y != null)
                return false;
            if (x != null && y == null)
                return true;
            return x.order_positions_ID > y.order_positions_ID;
        }
        #endregion

        #region IEquatable
        public bool Equals(Order_position other)
        {
            if ((object)other == null)
                return false;
            return (order_positions_ID == other.order_positions_ID);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            Order_position other_order_pos = other as Order_position;
            if (other_order_pos != null)
                return (Equals(other_order_pos));
            else
                throw new ArgumentException("Argument is not an Order Position");
        }
        #endregion

        #region IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Order_position other = obj as Order_position;
            if (other == null)
                throw new ArgumentException("Argument is not an Order Position");
            else
                return order_positions_ID.CompareTo(other.order_positions_ID);

        }
        #endregion

        #region IComparer
        public int Compare(Order_position x, Order_position y)
        {
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            return x.CompareTo(y);
        }

        #endregion

        #region ICloneable
        public object Clone()
        {
            return this.MemberwiseClone(); 
        }
        #endregion

        #region SQL queries
         public static string selectByOrderID(int order_ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master select ");
            sb.Append(" [DBO].[ORDERS].[ORDERS_ID], ");
            sb.Append(" [DBO].[ORDERS].[ORDERS_DATE], ");
            sb.Append(" [DBO].[ORDERS].[DESCRIPTION], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_ID], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_NAME], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_PHONE], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_MAIL], ");
            sb.Append(" [DBO].[ORDERS_POSITIONS].[ITEM_COUNT], ");
            sb.Append(" [DBO].[ORDERS_POSITIONS].[PRICE] ");
            sb.Append(" FROM [DBO].[ORDERS_POSITIONS] ");
            sb.Append(" LEFT JOIN [DBO].[ORDERS] ");
            sb.Append(" ON [DBO].[ORDERS].ORDERS_ID = [DBO].[ORDERS_POSITIONS].[ORDERS_ID] ");
            sb.Append(" LEFT JOIN [DBO].[CLIENTS] ");
            sb.Append(" ON [DBO].[CLIENTS].[CLIENTS_ID] = [DBO].[ORDERS].[CLIENTS_ID] ");
            sb.Append(string.Format(" WHERE [DBO].[ORDERS].[ORDERS_ID] = {0}; ", order_ID));
            return sb.ToString();
        }

        public static string selectByClientID(int client_ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master select ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_ID], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_NAME], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_PHONE], ");
            sb.Append(" [DBO].[CLIENTS].[CLIENTS_MAIL], ");
            sb.Append(" [DBO].[ORDERS].[ORDERS_ID], ");
            sb.Append(" [DBO].[ORDERS].[ORDERS_DATE], ");
            sb.Append(" [DBO].[ORDERS].[DESCRIPTION], ");
            sb.Append(" [DBO].[ORDERS_POSITIONS].[ITEM_COUNT], ");
            sb.Append(" [DBO].[ORDERS_POSITIONS].[PRICE] ");
            sb.Append(" FROM [DBO].[ORDERS_POSITIONS] ");
            sb.Append(" LEFT JOIN [DBO].[ORDERS] ");
            sb.Append(" ON [DBO].[ORDERS].ORDERS_ID = [DBO].[ORDERS_POSITIONS].[ORDERS_ID] ");
            sb.Append(" LEFT JOIN [DBO].[CLIENTS] ");
            sb.Append(" ON [DBO].[CLIENTS].[CLIENTS_ID] = [DBO].[ORDERS].[CLIENTS_ID] ");
            sb.Append(string.Format(" WHERE [DBO].[CLIENTS].[CLIENTS_ID] = {0}; ", client_ID));
            return sb.ToString();
        }

        public static string deleteFromDB(int ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" DELETE FROM [DBO].[ORDERS_POSITIONS] ");
            sb.Append(string.Format(" WHERE [DBO].[ORDERS_POSITIONS].[ORDERS_POSITIONS_ID] = {0}; ", ID));
            return sb.ToString();
        }

        public static string updateInDB(int ID, Order_position op)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" UPDATE [DBO].[ORDERS_POSITIONS] ");
            sb.Append(string.Format(" SET [DBO].[ORDERS_POSITIONS].[ITEM_COUNT] = {0}; ", op.item_count));
            sb.Append(string.Format(" [DBO].[ORDERS_POSITIONS].[ORDERS_ID] = {0}; ", op.order_ID));
            sb.Append(string.Format(" [DBO].[ORDERS_POSITIONS].[PRODUCTS_ID] = {0}; ", op.product_ID));
            sb.Append(string.Format(" [DBO].[ORDERS_POSITIONS].[PRICE] = {0}; ", op.price));
            sb.Append(string.Format(" WHERE [DBO].[ORDERS_POSITIONS].[ORDERS_POSITIONS_ID] = {0}; ", ID));
            return sb.ToString();
        }

        public static string insertIntoDB(Order_position op)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" INSERT INTO [dbo].[ORDERS_POSITIONS] ");
            sb.Append(string.Format(" ([ORDERS_ID], [PRODUCTS_ID], [PRICE], [ITEM_COUNT]) "));
            sb.Append(string.Format(" VALUES ({0}, {1}, {2}, {3}); ", op.order_ID, op.product_ID,
                op.price, op.item_count));
            return sb.ToString();
        }
        #endregion
    }
}

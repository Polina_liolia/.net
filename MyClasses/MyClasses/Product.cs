﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDataBaseClasses
{
    public class Product : IEquatable<Product>, IComparable, IComparer<Product>, ICloneable
    {
        private int productID;
        private string product_name;
        private double price;

        #region Properties
        public int ProductID
        {
            get
            {
                return productID;
            }

            set
            {
                productID = value;
            }
        }

        public string Poduct_name
        {
            get
            {
                return product_name;
            }

            set
            {
                product_name = value;
            }
        }

        public double Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }
        #endregion

        #region Constructors
        private Product() { }
        public Product(int productID, string product_name, double price)
        {
            this.productID = productID;
            this.product_name = product_name;
            this.price = price;
        }
        #endregion

        #region Methods overrided
        public override string ToString()
        {
            return string.Format("Product ID: {0}, product name: {1}, price: {2}", productID, product_name, price);
        }
        public override int GetHashCode()
        {
            return productID.GetHashCode();
        }
        #endregion

        #region Operators overloaded
        public static bool operator ==(Product product1, Product product2)
        {
            if (product1 == null || product2 == null)
                return object.Equals(product1, product2);
            return product1.Equals(product2);
        }

        public static bool operator !=(Product product1, Product product2)
        {
            if (product1 == null || product2 == null)
                return !object.Equals(product1, product2);
            return !product1.Equals(product2);
        }

        public static bool operator <(Product product1, Product product2)
        {
            if (product1 == null && product2 != null)
                return true;
            if (product1 != null && product2 == null)
                return false;
            return product1.productID < product2.productID;
        }

        public static bool operator >(Product product1, Product product2)
        {
            if (product1 == null && product2 != null)
                return false;
            if (product1 != null && product2 == null)
                return true;
            return product1.productID > product2.productID;
        }
        #endregion

        #region IEquatable
        public bool Equals(Product other)
        {
            if ((object)other == null)
                return false;
            return (productID == other.productID);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            Product other_product = other as Product;
            if (other_product != null)
                return (Equals(other_product));
            else
                throw new ArgumentException("Argument is not a Product");
        }
        #endregion

        #region IComparable
        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            Product other = obj as Product;
            if (other == null)
                throw new ArgumentException("Argument is not a Product");
            else
                return productID.CompareTo(other.productID);
               
        }
        #endregion

        #region IComparer
        public int Compare(Product x, Product y)
        {
            if (x == null)
                return -1;
            if (y == null)
                return 1;
            return x.CompareTo(y);
        }

        #endregion

        #region ICloneable
        public object Clone()
        {
            Product other = (Product)this.MemberwiseClone(); //clones only ID and price
            other.Poduct_name = string.Copy(product_name); //deep copy
            return other;
        }
        #endregion

        #region DB queries
        public static string getAllProductsSelect()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master select ");
            sb.Append(" [DBO].[PRODUCTS].[PRODUCTS_ID], ");
            sb.Append(" [DBO].[PRODUCTS].[PRODUCTS_NAME], ");
            sb.Append(" [DBO].[PRODUCTS].[PRICE] ");
            sb.Append(" FROM [DBO].[PRODUCTS] ");
            return sb.ToString();
           
        }

        public static string getProductSelect(int ID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getAllProductsSelect());
            sb.Append(string.Format(" WHERE [dbo].[PRODUCTS].PRODUCTS_ID = {0}; ", ID));
            return sb.ToString();
        }

        public static string updateProductsInDB(int ID, Product New)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" UPDATE [dbo].[PRODUCTS] ");
            sb.Append(string.Format(" SET [dbo].[PRODUCTS].[PRODUCTS_NAME] = N'{0}', ", New.product_name));
            sb.Append(string.Format(" [dbo].[PRODUCTS].[PRICE] = {0} ", New.price));
            sb.Append(string.Format(" WHERE [dbo].[PRODUCTS].PRODUCTS_ID = {0}; ", ID));
            return sb.ToString();
        }

        public static string deletePoductFromDB(int ID)
        { 
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" DELETE FROM [dbo].[PRODUCTS] ");
            sb.Append(string.Format(" WHERE [dbo].[PRODUCTS].PRODUCTS_ID = {0}; ", ID));
            return sb.ToString();
        }
      
        public static string insertIntoDB(Product p)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" use master ");
            sb.Append(" INSERT INTO [dbo].[PRODUCTS] ");
            sb.Append(string.Format(" ([PRODUCTS_NAME], [PRICE]) "));
            sb.Append(string.Format(" VALUES ({0}, {1}); ", p.product_name, p.price));
            return sb.ToString();
        }
        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICompareTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList temperatures = new ArrayList();
            //random numbers generator:
            Random rnd = new Random();
            //generaiting 10 temperatures between 0 and 100:
            int N = 10;
            int a = 0;
            int b = 100;
            for (int ctr = 1; ctr <= N; ctr ++)
            {
                int degrees = rnd.Next(a, b);
                Temperature temp = new Temperature();
                temp.Farenheit = degrees;
                temperatures.Add(temp);
            }
            //для in должен быть реализован интерфейс IEnumerable
            Console.WriteLine("Before sort:");
            foreach (Temperature temp in temperatures)
                Console.WriteLine(temp.Farenheit);
            //Sort ArrayList, используется интерфейс IComparable
            temperatures.Sort();
            Console.WriteLine("After sort:");
            foreach (Temperature temp in temperatures)
                Console.WriteLine(temp.Farenheit);


        }
    }
}

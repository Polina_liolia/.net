﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICompareTest
{
    class Temperature : IComparable
    {
        #region Class fields
        //temperature value
        protected double temperatureF;
        public double Farenheit
        {
            get { return this.temperatureF; }
            set { this.temperatureF = value; }
        }
        public double Celsius
        {
            get
            {
                return (this.temperatureF - 32) * (5.0 / 9);
            }
            set
            {
                this.temperatureF = (value * 9.0 / 5) + 32;
            }
        }
        #endregion

        public Temperature() { }

        #region IComparable realisation
        public int CompareTo(object obj) //аналог string.Compare
        {
            if (obj == null)
                return 1;
            Temperature other = obj as Temperature; //преобразование без exception
            //Temperature other = (Temperature)obj; //то же самое преобразование, но с exception - только внутри try {} catch()
            if (other != null) //преобразование с помощью as произошло
                return this.temperatureF.CompareTo(other.temperatureF); //вызов реализации CompareTo для сравнения типа double
            else //преобразование с помощью as не произошло
                throw new ArgumentException("object is not a Temperature");
        }
        #endregion
    }
}
